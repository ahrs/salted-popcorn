#!/bin/sh

set -e

if [ "$(id -u)" -ne 0 ]
then
	if command -v sudo > /dev/null 2>&1
	then
		exec sudo "$0" -- "$@"
		exit 1 # exec shouldn't fail but just in case...
	else
		echo "Please run me as root..."
		exit 1
	fi
fi

CONTAINER="salted_popcorn_gentoo"
ROOT="/var/lib/lxc/$CONTAINER/rootfs"
EGIT_OVERRIDE_REPO_SALTSTACK_SALT="${EGIT_OVERRIDE_REPO_SALTSTACK_SALT:-https://github.com/saltstack/salt.git}"
EGIT_OVERRIDE_REPO_SALTSTACK_SALT=http://192.168.0.11:10080/ahrs/salt.git

make lxc_stop CONTAINER="$CONTAINER"

lxc-create \
	-n "$CONTAINER" \
	-t "download" \
	-- --dist gentoo  --release current --arch amd64

#make lxc_bindmount CONTAINER="$CONTAINER"

if [ -d "$PWD/packages" ]
then
	printf "lxc.mount.entry=%s %s none rw,bind 0 \n" "$PWD/packages" "usr/portage/packages" | tee -a "/var/lib/lxc/$CONTAINER/config"
    mkdir -p "/var/lib/lxc/$CONTAINER/rootfs/usr/portage/packages"
fi

mkdir -p "/var/lib/lxc/$CONTAINER/rootfs/$PWD"
mkdir -p "/var/lib/lxc/$CONTAINER/rootfs/var/tmp/ccache"

printf "lxc.mount.entry=%s %s none ro,bind 0 \n" "$PWD" "$(printf "%s" "$PWD" | cut -c 2-)" | tee -a "/var/lib/lxc/$CONTAINER/config"
printf "lxc.mount.entry=%s %s none rw,bind 0 \n" "/var/tmp/ccache" "var/tmp/ccache" | tee -a "/var/lib/lxc/$CONTAINER/config"

printf 'FEATURES="${FEATURES} %s"\n' "buildpkg parallel-fetch parallel-install ccache" | tee -a "/var/lib/lxc/$CONTAINER/rootfs/etc/portage/make.conf"

printf 'EMERGE_DEFAULT_OPTS="--jobs %d --usepkg=y --buildpkg-exclude \"virtual/*"\"\n' "$(nproc)" | tee -a "/var/lib/lxc/$CONTAINER/rootfs/etc/portage/make.conf"

lxc-start -n "$CONTAINER" --logfile=/dev/stderr

run_command() {
	/usr/bin/env -i -- \
      TERM="$TERM" \
      PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin" \
      HOME="/root" \
      USER="root" \
        lxc-attach -n "$CONTAINER" --logfile=/dev/stderr -- $@
}

set -x

run_command ln -vfs /etc/init.d/net.lo /etc/init.d/net.eth0
run_command /sbin/rc-update add net.eth0 sysinit

# reboot...
lxc-stop -n "$CONTAINER"

mkdir -p "/var/lib/lxc/$CONTAINER/rootfs/$PWD"

lxc-start -n "$CONTAINER"

run_command busybox udhcpc
run_command busybox ping -c3 1.1.1.1

run_command eselect profile show | tail -1 | grep -q musl && IS_MUSL=1 || IS_MUSL=0

run_command eselect python set python2.7

if [ "$IS_MUSL" -eq 1 ]
then
	mkdir -p "$ROOT/var/lib/layman"
	git clone https://github.com/gentoo/musl.git "$_/musl"
	mkdir -p "$ROOT/etc/portage/repos.conf"
	cat > "$_/layman.conf" <<EOF
[musl]
priority = 50
location = /var/lib/layman/musl
layman-type = git
auto-sync = No

EOF
fi

salt="$(find "$ROOT/usr/portage/app-admin/salt" -name "*.ebuild"  | sort | tail -1)"

cp "$salt" "$(dirname "$salt")/salt-9999.ebuild"

# fix patch that no longer applies on top of git-master by commenting it out
# shellcheck disable=SC2016
sed -i 's|"${FILESDIR}/${PN}-2017.7.8-tests.patch"|`#"${FILESDIR}/${PN}-2017.7.8-tests.patch"`|g' "$ROOT/usr/portage/app-admin/salt/salt-9999.ebuild"

run_command ebuild /usr/portage/app-admin/salt/salt-9999.ebuild manifest

unset salt

mkdir -p "$ROOT/etc/portage/patches/app-admin/salt"

cp ./fix-ebuild-pkg-module.diff "$ROOT/etc/portage/patches/app-admin/salt"

run_command emerge -q dev-util/ccache

run_command emerge -quDN --tree --keep-going @world

# Perl is really annoying 
run_command perl-cleaner --all -- --usepkg=n --getbinpkg=n

run_command emerge -quDN --tree --keep-going @world

run_command emerge -v --depclean || {
    # assume perl failed *again*
    run_command perl-cleaner --all -- --usepkg=n --getbinpkg=n --with-bdeps=y || true
    run_command emerge -quDN --tree --keep-going --usepkg=n --getbinpkg=n --with-bdeps=y @world
    run_command emerge -v --depclean
}

mkdir -p "$ROOT/etc/portage/package.use"
for f in 'dev-libs/openssl' 'net-misc/openssh'
do
	echo "${f} -bindist" > "$ROOT/etc/portage/package.use/$(basename "$f")"
done

#run_command emerge -q1 app-admin/sudo dev-libs/openssl net-misc/openssh

mkdir -p "$ROOT/etc/portage/package.keywords"
for f in 'app-admin/salt' 'dev-python/pycryptodome'
do
	echo "${f} ~*" > "$ROOT/etc/portage/package.keywords/$(basename "$f")"
done

cat > "$ROOT/etc/portage/package.keywords/cffi" << EOF
# required by dev-python/pyzmq-16.0.2::gentoo
# required by app-admin/salt-9999::gentoo
# required by app-admin/salt (argument)
dev-python/cffi ~amd64
EOF

#echo "app-admin/salt -* zeromq openssl portage python_targets_python3_6 python_targets_python2_7" > "$ROOT/etc/portage/package.use/salt"
echo "app-admin/salt -* zeromq portage python_targets_python3_6 python_targets_python2_7" > "$ROOT/etc/portage/package.use/salt"

echo "=app-admin/salt-9999 **" >> "$ROOT/etc/portage/package.accept_keywords"

run_command env EGIT_OVERRIDE_REPO_SALTSTACK_SALT="${EGIT_OVERRIDE_REPO_SALTSTACK_SALT}" emerge -v app-admin/salt

run_command salt-call --local test.ping

#run_command /bin/sh -c "cd $PWD;make groups users SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call\;make SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call"

env -i -- lxc-attach -n "$CONTAINER" -- /bin/sh -c ". /etc/profile;export PATH;set -e -x;cd $PWD;make groups users SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call MINION_LOG_FILE=/var/log/salt/minion;chown -R ahrs:ahrs ~ahrs 2>/dev/null || true;make SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call state STATE=common.Gentoo.local_overlay MINION_LOG_FILE=/var/log/salt/minion;make SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call MINION_LOG_FILE=/var/log/salt/minion"
#env -i -- lxc-attach -n "$CONTAINER" -- /bin/sh -c ". /etc/profile;export PATH;set -e -x;cd $PWD;make groups users SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call;chown -R ahrs:ahrs ~ahrs 2>/dev/null || true;make state STATE=common.Gentoo.Gentoo SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call"

set +x

exit "$?"
