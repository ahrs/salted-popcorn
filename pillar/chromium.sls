{% from 'applications.sls' import desktop_type with context %}

chromium:
  extensions:
    {% if desktop_type == 'kde' %}
    Plasma Integration: cimiefiiaegbelhefglklhhakcgmhkai
    {% elif desktop_type == 'gnome' %}
    GNOME Shell integration: gphhapmejobijbbhgpjhcjognlahblep
    {% endif %}
    Dark Reader: eimadpbcbfnmbkopoojfekhnkhdbieeh
    Decentraleyes: ldpochfccmkkmhdbclfhpagapcfdljkj
    HTTPS Everywhere: gcbommkclmclpchllfjekcdonpmejbdp
    Reddit Enhancement Suite: kbmfpngjjgdllneeigpgjifpgocmfgmb
    Ublock Origin: cjpalhdlnbpafiamejdnhcphjbkeiagm
    Vimium: dbepggeogbaibhgnhhndojpepiihcmeb
    Wallabagger: gbmgphmejlcoihgedabhgjdkcahacjlj
