{% set desktop_type = 'gtk' %}
{% set desktop_type = 'gnome' %}
{% set desktop_type = 'qt' %}
{% set desktop_type = 'kde' %}

{% set desktop_type = 'gtk' %}
desktop_type: {{desktop_type}}
applications:
  # installed on all systems regardless of the desktop environment
  common:
    {% if grains['os_family'] != 'Alpine' %}
    - audio.cantata.cantata
    {% endif %}
    - audio.mpc.mpc
    - audio.ncmpcpp.ncmpcpp
    - audio.pulsemixer.pulsemixer
    - internet.chromium.chromium
    - networking.network-manager.network-manager
    {% if grains['os_family'] != 'Suse' %}
    - terminals.kitty.kitty
    {% endif %}
    {% if grains['os_family'] != 'Suse' %}
    - utilities.pass.pass
    {% endif %}
    {% if grains['os_family'] != 'Alpine' %}
    - utilities.ranger.ranger
    {% endif %}
    #- utilities.sc-im.sc-im
    {% if grains['os_family'] != 'Suse' %}
    - video.ffmpeg.ffmpeg
    - video.mpv.mpv
    - video.youtube-dl.youtube-dl
    {% endif %}
  # installed in GNOME environments only
  gnome:
    - file-managers.nautilus.nautilus
    - graphics.eog.eog
    - graphics.evince.evince
    - graphics.gnome-screenshot.gnome-screenshot
    - utilities.baobab.baobab
    - utilities.alacarte.alacarte
    - utilities.chrome-gnome-shell.chrome-gnome-shell
    - utilities.dconf-editor.dconf-editor
    - utilities.file-roller.file-roller
    - utilities.gedit.gedit
    - utilities.gnome-calculator.gnome-calculator
    - utilities.gnome-disks.gnome-disks
    - utilities.gnome-logs.gnome-logs
    - utilities.gnome-system-monitor.gnome-system-monitor
    - video.gnome-mpv.gnome-mpv
  # installed in the KDE Plasma environment only
  kde:
    - file-managers.dolphin.dolphin
    - graphics.gwenview.gwenview
    - graphics.okular.okular
    - graphics.spectacle.spectacle
    - internet.akregator.akregator
    - utilities.ark.ark
    - utilities.kate.kate
    - utilities.kcalc.kcalc
    - utilities.kdeconnect.kdeconnect
    - utilities.ksysguard.ksysguard
  {% if grains['os_family'] != 'Alpine' %}
  # only installed in GTK environments
  gtk:
    - audio.pavucontrol.pavucontrol
  # only installed in Qt environments
  qt:
    - audio.pavucontrol.qt
  {% endif %}
