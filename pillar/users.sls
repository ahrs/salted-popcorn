users:
  ## Full list of pillar values
  nobody:
    home: /var/empty
    homedir_owner: root
    homedir_group: root
    groups:
      - nobody
      - nogroup
  ahrs:
    dotfiles:
      - 'http://192.168.0.11:10080/ahrs/dotfiles.git'
      - 'https://gitlab.com/ahrs/dotfiles.git'
    gpg_keys:
        - E0A0 7C10 71AC C886 3606  7760 E18B E278 07DA 2E96
    fullname: ahrs
    # WARNING: If 'empty_password' is set to True, the 'password' statement
    # will be ignored by enabling password-less login for the user.
    empty_password: True
    system: False
    home: /home/ahrs
    homedir_owner: ahrs
    homedir_group: ahrs
    user_dir_mode: 750
    createhome: True
    sudouser: True
    # sudo_rules doesn't need the username as a prefix for the rule
    # this is added automatically by the formula.
    # ----------------------------------------------------------------------
    # In case your sudo_rules have a colon please have in mind to not leave
    # spaces around it. For example:
    # ALL=(ALL) NOPASSWD: ALL    <--- THIS WILL NOT WORK (Besides syntax is ok)
    # ALL=(ALL) NOPASSWD:ALL     <--- THIS WILL WORK
    sudo_rules:
      - ALL=(ALL) ALL
    #sudo_defaults:
    #  - '!requiretty'
    shell: /bin/zsh
    groups:
      - ahrs
      - users
      - tty
      - sudo
      - audio
      - video
      - plugdev
      - input
      - kvm
    uid: 1000
