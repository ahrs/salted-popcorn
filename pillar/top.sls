base:
  '*':
    - default
    {% if grains['os_family'] == 'Gentoo' %}
    - gentoo
    {% endif %}
    - users
    - applications
    - mimeapps
    - chromium
