consolefont:
  # list of fonts to use on non-Debian systems
  # the first font found will be used
  fonts:
    - Uni2-VGA32x16 
    - latarcyrheb-sun32
  # values used on a debian system to seed `dpkg-reconfigure console-setup`
  debconf:
    fontface:
      - VGA
    fontsize:
      - 16x32

