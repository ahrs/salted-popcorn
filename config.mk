ALPINE_MIRROR?=http://dl-cdn.alpinelinux.org/alpine
# No default mirror specified for Arch. With a default mirror
# specified we'll keep the default mirror list but re-write it
# so that our mirror is at the top
ARCH_MIRROR?=
UBUNTU_ARCHIVE_MIRROR?=archive.ubuntu.com
DEBIAN_ARCHIVE_MIRROR?=deb.debian.org
# You can add extra hosts to the /etc/hosts file
EXTRA_HOSTS?=$(shell printf "%s\t%s\n" "$$(ip address show lxdbr0  | grep inet | head -1 | grep -oE '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')" "lxdbr0")
SALT_REPO_URI?=https://github.com/saltstack/salt.git
VOID_MIRROR?=http://alpha.de.repo.voidlinux.org/current

# DNS servers to use in /etc/resolv.conf since systemd-magic
# doesn't work properly on non-systemd hosts leaving you without
# dns in the container so resolving hosts fails.
PRIMARY_FALLBACK_DNS1?=1.1.1.1
PRIMARY_FALLBACK_DNS2?=8.8.8.8

DOCKER?=docker
ENV?=/usr/bin/env
LOG_FILE?=/proc/self/fd/2
LOG_LEVEL?=profile
MINION_LOG_FILE?=/proc/self/fd/2
# Override the configured state_output value for minion
# output. One of 'full', 'terse', 'mixed', 'changes' or 'filter'
MINION_STATE_OUTPUT?=mixed
LXC_HOME?=/var/lib/lxc
PILLAR?={}
SED?=sed
SALT:=$(PWD)
SALT_CALL?=salt-call
STATE?=
STDERR?=/proc/self/fd/2
STDOUT?=/proc/self/fd/1
SUDO?=sudo
SALT_TAG?=2019.2
