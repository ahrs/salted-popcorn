include config.mk

ifeq ($(STDOUT),/proc/self/fd/1)
  ifeq ($(STDERR),/proc/self/fd/2)
	COLOUR?=--force-color
  endif
endif

STOP?=false
COLOUR?=--no-color

all: state

state:
	$(SUDO) $(SED) -i 's|#file_client: remote|file_client: local|g' /etc/salt/minion
	$(SUDO) $(SED) -i 's|#failhard: False|failhard: True|g' /etc/salt/minion
	$(SUDO) $(SALT_CALL) --local --pillar-root=$(SALT)/pillar --file-root=$(SALT)/roots --log-level=$(LOG_LEVEL) --log-file="$(MINION_LOG_FILE)" --state-output=$(MINION_STATE_OUTPUT) state.apply $(STATE) pillar='$(PILLAR)' > $(STDOUT) 2> $(STDERR)

common groups users:
	$(MAKE) state STATE=$@

lxc_stop:
	if $(SUDO) lxc-ls | grep -F "$(CONTAINER)"; then \
	  $(SUDO) lxc-stop -n "$(CONTAINER)" --logfile=$(LOG_FILE); \
	  $(SUDO) lxc-destroy -n "$(CONTAINER)" --logfile=$(LOG_FILE); \
	fi
	# Hack to exit the Makefile early (in this case 3 indicates success)
	if [ "$(STOP)" = "true" ]; then \
	  exit 3; \
	fi

lxc_create: TEMPLATE?=download
lxc_create: ENVIRONMENT?=
lxc_create:
	$(SUDO) env $(ENVIRONMENT) lxc-create -n "$(CONTAINER)" -t "$(TEMPLATE)" --logfile=$(LOG_FILE) -- $(ARGS)

lxc_bindmount:
	$(SUDO) mkdir -p "$(LXC_HOME)/$(CONTAINER)/rootfs/$(PWD)"
	$(SUDO) $(SED) -i "s|lxc.mount.entry=$(PWD).*||g" "$(LXC_HOME)/$(CONTAINER)/config"
	printf "lxc.mount.entry = %s $(LXC_HOME)/$(CONTAINER)/rootfs/%s none ro,bind 0 0" "$(PWD)" "$$(printf "%s" "$(PWD)" | cut -c 2-)" | $(SUDO) tee -a "$(LXC_HOME)/$(CONTAINER)/config"

# systemd-resolved behaves "weirdly" in containers when the host is not usig systemd as init
# If the resolv.conf symlink is bad we remove it and revert to a fallback DNS
# fix_dns also adds custom hosts file entries to containers entries
fix_dns:
	@if ! $(SUDO) test -e "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/resolv.conf"; then \
		$(SUDO) rm -f "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/resolv.conf"; \
		printf "nameserver %s\nnameserver %s\n" "$(PRIMARY_FALLBACK_DNS1)" "$(PRIMARY_FALLBACK_DNS2)" | $(SUDO) tee "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/resolv.conf"; \
	fi
	@printf '$(EXTRA_HOSTS)\n' | $(SUDO) tee -a "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/hosts" > /dev/null; \

lxc_arch: CONTAINER=salted_popcorn_arch
lxc_arch:
	$(MAKE) lxc_stop CONTAINER=salted_popcorn_arch
	$(MAKE) lxc_create CONTAINER=salted_popcorn_arch ARGS="--dist archlinux --release current --arch amd64"
	$(MAKE) lxc_bindmount CONTAINER=salted_popcorn_arch
	$(SUDO) mkdir -p "$(LXC_HOME)/$(CONTAINER)/rootfs/$(PWD)"
	$(SUDO) lxc-start -n "$(CONTAINER)"
	@echo 'Server = $(ARCH_MIRROR)' | $(SUDO) tee "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/pacman.d/mirrorlist.tmp" > /dev/null
	$(SUDO) cat "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/pacman.d/mirrorlist.tmp" "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/pacman.d/mirrorlist" | $(SUDO) tee "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/pacman.d/mirrorlist.tmp"
	$(SUDO) mv -f "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/pacman.d/mirrorlist.tmp" "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/pacman.d/mirrorlist"
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" -- sed -i 's|\[options\]|[options]\nDisableDownloadTimeout|g' /etc/pacman.conf
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" -- ip link set eth0 up
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" -- dhcpcd eth0
	$(MAKE) CONTAINER="$(CONTAINER)" fix_dns
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" -- pacman -Syuw --noconfirm || true
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" -- pacman -Su --noconfirm
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" -- pacman -Sw --needed --noconfirm base base-devel git sed python2 make pkg-config --ignore linux || true
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" -- pacman -S --needed --noconfirm base base-devel git sed python2 make pkg-config --ignore linux
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" -- /bin/sh -c 'set -e -x;. /etc/profile;cd $(PWD);/bin/sh bootstrap-salt.sh -X -g $(SALT_REPO_URI) -d git $(SALT_TAG);make groups users SALT_CALL=/usr/bin/salt-call;chown -R ahrs:ahrs ~ahrs 2>/dev/null || true;make SALT_CALL=/usr/bin/salt-call'


lxc_ubuntu: DIST?=disco
lxc_ubuntu: CONTAINER?=salted_popcorn_ubuntu_$(DIST)
lxc_ubuntu:
	$(MAKE) lxc_stop CONTAINER=$(CONTAINER)
	$(MAKE) lxc_create CONTAINER=$(CONTAINER) ARGS?="--dist ubuntu --release $(DIST) --arch amd64"
	$(MAKE) lxc_bindmount CONTAINER=$(CONTAINER)
	$(SUDO) mkdir -p "$(LXC_HOME)/$(CONTAINER)/rootfs/$(PWD)"
	$(SUDO) lxc-start -n "$(CONTAINER)" --logfile=$(LOG_FILE)
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/ip link set eth0 up
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/dhclient eth0
	$(SUDO) sed -i "s|archive.ubuntu.com|$(UBUNTU_ARCHIVE_MIRROR)|g" "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/apt/sources.list"
	$(SUDO) sed -i "s|deb.debian.org|$(DEBIAN_ARCHIVE_MIRROR)|g" "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/apt/sources.list"
	$(MAKE) CONTAINER="$(CONTAINER)" fix_dns
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /bin/sh -c '. /etc/profile;export PATH;id ubuntu > /dev/null 2>&1 && userdel -f ubuntu;export DEBIAN_FRONTEND=noninteractive;apt-get update -qq;set -e;apt-get install --yes --no-install-recommends locales make git apt systemd ca-certificates;/bin/sh ./bootstrap-salt.sh -d -g $(SALT_REPO_URI) -X git $(SALT_TAG);touch /etc/default/locale;SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call make groups users all'

lxc_ubuntu_disco: DIST=disco
lxc_ubuntu_disco: lxc_ubuntu

lxc_ubuntu_cosmic: DIST=cosmic
lxc_ubuntu_cosmic:
	$(MAKE) lxc_ubuntu DIST="$(DIST)"
lxc_ubuntu_18.10: lxc_ubuntu_cosmic

lxc_ubuntu_bionic: DIST=bionic
lxc_ubuntu_bionic:
	$(MAKE) lxc_ubuntu DIST="$(DIST)"
lxc_ubuntu_18.04:  lxc_ubuntu_bionic

lxc_ubuntu_xenial: DIST=xenial
lxc_ubuntu_xenial:
	$(MAKE) lxc_ubuntu DIST="$(DIST)"
lxc_ubuntu_16.04: lxc_ubuntu_xenial

lxc_ubuntu_trusty: DIST=trusty
lxc_ubuntu_trusty:
	$(MAKE) lxc_ubuntu DIST="$(DIST)"
lxc_ubuntu_14.04: lxc_ubuntu_trusty

lxc_debian: DIST?=sid
lxc_debian: CONTAINER?=salted_popcorn_debian_$(DIST)
lxc_debian:
	$(MAKE) lxc_ubuntu CONTAINER=$(CONTAINER) ARGS="--dist debian --release $(DIST) --arch amd64"

lxc_debian_sid: DIST=sid
lxc_debian_sid: lxc_debian
lxc_debian_unstable: lxc_debian_sid

lxc_debian_buster: DIST=buster
lxc_debian_buster:
	$(MAKE) lxc_debian DIST="$(DIST)"
lxc_debian_testing: lxc_debian_buster
lxc_debian_10: lxc_debian_buster

lxc_debian_stretch: DIST=stretch
lxc_debian_stretch:
	$(MAKE) lxc_debian DIST="$(DIST)"
lxc_debian_stable: lxc_debian_stretch
lxc_debian_9: lxc_debian_stretch

lxc_debian_jessie: DIST=jessie
lxc_debian_jessie:
	$(MAKE) lxc_debian DIST="$(DIST)"
lxc_debian_oldstable: lxc_debian_jessie
lxc_debian_8: lxc_debian_jessie

lxc_opensuse: DIST?=15.0
lxc_opensuse: CONTAINER?=salted_popcorn_opensuse_$(DIST)
lxc_opensuse:
	$(MAKE) lxc_stop CONTAINER=$(CONTAINER)
	$(MAKE) lxc_create CONTAINER=$(CONTAINER) ARGS?="--dist opensuse --release $(DIST) --arch amd64"
	$(MAKE) lxc_bindmount CONTAINER=$(CONTAINER)
	$(SUDO) mkdir -p "$(LXC_HOME)/$(CONTAINER)/rootfs/$(PWD)"
	# The common rules break opensuse - This is probably not a good sign but whatever.
	$(SUDO) $(SED) -i 's|^lxc.include = /usr/share/lxc/config/common.conf|#lxc.include = /usr/share/lxc/config/common.conf|g' "$(LXC_HOME)/$(CONTAINER)/config"
	$(SUDO) lxc-start -n "$(CONTAINER)" --logfile=$(LOG_FILE)
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/ip link set eth0 up
	sleep 10
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /usr/bin/systemctl start wicked.service
	$(MAKE) CONTAINER="$(CONTAINER)" fix_dns
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /usr/bin/zypper --non-interactive --gpg-auto-import-keys dup
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /bin/sh -c '. /etc/profile;export PATH;zypper --non-interactive --gpg-auto-import-keys install git make;/bin/sh ./bootstrap-salt.sh -d -g $(SALT_REPO_URI) -X git $(SALT_TAG);SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call make groups users all'

lxc_opensuse_leap: lxc_opensuse

lxc_fedora: DIST?=29
lxc_fedora: CONTAINER?=salted_popcorn_fedora_$(DIST)
lxc_fedora: PKGMGR?=dnf
lxc_fedora:
	$(MAKE) lxc_stop CONTAINER=$(CONTAINER)
	$(MAKE) lxc_create CONTAINER=$(CONTAINER) ARGS?="--dist fedora --release $(DIST) --arch amd64"
	$(MAKE) lxc_bindmount CONTAINER=$(CONTAINER)
	$(SUDO) mkdir -p "$(LXC_HOME)/$(CONTAINER)/rootfs/$(PWD)"
	$(SUDO) lxc-start -n "$(CONTAINER)" --logfile=$(LOG_FILE)
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/ip link set eth0 up
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/dhclient eth0
	$(MAKE) CONTAINER="$(CONTAINER)" fix_dns
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /bin/sh -c '. /etc/profile;export PATH;$(PKGMGR) install -y git make;/bin/sh ./bootstrap-salt.sh -d -g $(SALT_REPO_URI) -X git $(SALT_TAG);SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call make groups users all'

lxc_fedora_29: lxc_fedora

lxc_fedora_28: DIST=28
lxc_fedora_28:
	$(MAKE) lxc_fedora DIST="$(DIST)"

lxc_fedora_27: DIST=27
lxc_fedora_27:
	$(MAKE) lxc_fedora DIST="$(DIST)"

lxc_centos: DIST?=7
lxc_centos:
	$(MAKE) lxc_fedora CONTAINER=salted_popcorn_centos_$(DIST) PKGMGR=yum ARGS="--dist centos --release $(DIST) --arch amd64"

lxc_centos_7: DIST=7
lxc_centos_7: lxc_centos

lxc_centos_6: DIST=6
lxc_centos_6:
	$(MAKE) lxc_centos DIST="$(DIST)"

lxc_gentoo:
	$(SUDO) /bin/sh -x gentoo-lxc.sh

# Currently there's no py2-m2crypto in the stable releases
# so salt only bootstraps properly on edge. I may be able
# to backport packages to the older releases if I need to.
# Caveat: Edge is a fast-paced distro sometimes packages
# break due to so-name upgrades so things break until the
# repos settle down. This can cause a bootstrap to fail
# in the case of ssl being broken.
lxc_alpine: DIST=edge
lxc_alpine: CONTAINER=salted_popcorn_alpine_$(DIST)
lxc_alpine:
	$(MAKE) lxc_stop CONTAINER=$(CONTAINER)
	$(MAKE) lxc_create CONTAINER=$(CONTAINER) ARGS="--dist alpine --release $(DIST) --arch amd64"
	$(MAKE) lxc_bindmount CONTAINER=$(CONTAINER)
	$(SUDO) mkdir -p "$(LXC_HOME)/$(CONTAINER)/rootfs/$(PWD)"
	$(SUDO) lxc-start -n "$(CONTAINER)" --logfile=$(LOG_FILE)
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/ip link set eth0 up
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /bin/busybox udhcpc
	$(MAKE) CONTAINER="$(CONTAINER)" fix_dns
	@$(SUDO) cat "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/apk/repositories"
	@$(SUDO) cat "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/apk/repositories" | rev | awk -F '/' '{print $$1 "/" $$2}' | rev | sed 's|^|$(ALPINE_MIRROR)/|g' | $(SUDO) tee "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/apk/repositories" > /dev/null
	@if [ "$(DIST)" = "edge" ] ; then \
	  echo "$(ALPINE_MIRROR)/edge/testing" | $(SUDO) tee -a "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/apk/repositories" > /dev/null; \
	fi
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/apk update -v
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/apk update -v
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/apk add -v ca-certificates
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/apk add -v bash coreutils make pciutils sudo shadow py-futures docker-py git dmidecode
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /bin/sh -c '. /etc/profile;export PATH;/bin/sh ./bootstrap-salt.sh -d -X git $(SALT_TAG);printf "providers:\n  service: %s\n  pkg: %s" "busybox" "apk" >> /etc/salt/minion;SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call LOG_LEVEL=debug make groups users all'

lxc_alpine_edge: lxc_alpine

lxc_alpine_3.8: DIST=3.8
lxc_alpine_3.8:
	$(MAKE) lxc_alpine DIST="$(DIST)"

lxc_void: CONTAINER=salted_popcorn_void
lxc_void:
	$(MAKE) lxc_stop CONTAINER=$(CONTAINER)
	$(MAKE) lxc_create CONTAINER=$(CONTAINER) TEMPLATE=voidlinux ENVIRONMENT="XBPS_ARCH=x86_64"
	$(MAKE) lxc_bindmount CONTAINER=$(CONTAINER)
	$(SUDO) mkdir -p "$(LXC_HOME)/$(CONTAINER)/rootfs/$(PWD)"
	$(SUDO) lxc-start -n "$(CONTAINER)" --logfile=$(LOG_FILE)
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /sbin/ip link set eth0 up
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- dhcpcd eth0
	$(MAKE) CONTAINER="$(CONTAINER)" fix_dns
	@printf 'repository=%s\n' '$(VOID_MIRROR)' | $(SUDO) tee "$(LXC_HOME)/$(CONTAINER)/rootfs/etc/xbps.d/00-repository-main.conf"
	# Calling xbps-install multiple times is deliberate since sometimes it hangs the first time we call it...
	# This seems like a bug but for now we'll work around it
	$(SUDO) $(ENV) -i -- lxc-attach -n "$(CONTAINER)" --logfile=$(LOG_FILE) -- /bin/sh -c '. /etc/profile;export PATH;xbps-install -Syu || xbps-install -Syu; xbps-install -Syu git salt make;SUDO=/usr/bin/env SALT_CALL=/usr/bin/salt-call make groups users all'
