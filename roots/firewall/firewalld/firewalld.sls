{% from "firewall/map.jinja" import firewall with context %}

install_and_enable_firewalld:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}net-firewall/{% endif %}firewalld
  {% if not 'virtual_subtype' in grains %}
  {% set state = 'service.running' %}
  {% else %}
  {% set state = 'service.enabled' %}
  {% endif %}
  {{state}}:
    - name: firewalld
    - enable: True
    - reload: True
{% if firewall['zone'] != None %}
set_default_zone:
  cmd.run:
    - name: |
        firewall-cmd --set-default-zone={{firewall['zone']}}
    - onlyif: |
        if command -v systemd-detect-virt > /dev/null 2>&1
        then
          systemd-detect-virt --chroot && exit 1
        fi
        current_zone="$(firewall-cmd --get-default-zone)"
        test "$current_zone" != "{{firewall['zone']}}"
{% else %}
set_default_zone:
  cmd.run:
    - name: |
        firewall-cmd --set-default-zone=public
{% endif %}
