{% set desktop_type = salt['pillar.get']('desktop_type', default=None) %}
include:
  - firewall.firewalld.firewalld
  {% if desktop_type != None and grains['os_family'] == 'RedHat' %}
  - firewall.firewalld.firewall-config
  {% endif %}
