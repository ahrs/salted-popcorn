install_and_enable_ufw:
  pkg.installed:
    - pkgs:
      - ufw
  {% if not 'virtual_subtype' in grains %}
  {% set state = 'service.running' %}
  {% else %}
  {% set state = 'service.enabled' %}
  {% endif %}
  {% if grains['os_family'] == 'Alpine' %}
  cmd.run:
    - name: |
        ufw enable
        rc-service ufw -v start
        rc-update add ufw default
  {% elif grains['os_family'] == 'Void' %}
  cmd.run:
    - name: |
        ln -fs /etc/sv/ufw /var/service
  {% else %}
  {{state}}:
    - name: ufw
    - enable: True
  {% endif %}
