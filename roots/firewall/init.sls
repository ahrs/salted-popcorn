{% from "firewall/map.jinja" import firewall with context %}

{% if firewall.firewall != None %}
{% if firewall.disabled == False %}
include:
  - firewall.{{ firewall.firewall }}
{% else %}
{% if 'service' in firewall %}
disable_firewall:
  module.run:
    - name: service.disabled
    - m_name: {{ firewall.service }}
    - kwargs: {{ firewall.firewall }}
{% endif %}
{% endif %}
{% endif %}
