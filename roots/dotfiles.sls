include:
  - applications.programming.tools.git.git
  - applications.utilities.busybox.busybox
  - applications.utilities.stow.stow
  - applications.utilities.sudo.sudo
dotfiles:
  cmd.run:
    - check_cmd:
      - /bin/true
    - name: |
        set -e
        if ! command -v sudo > /dev/null 2>&1
        then
          echo "sudo not found" >&2
          exit 1
        fi
        if ! command -v git > /dev/null 2>&1
        then
          echo "git not found" >&2
          exit 1
        fi
        {% for user in pillar['users'] %}
        {% if user == 'root' or user == 'nobody' %}
        {% continue %}
        {% endif %}
        DOTS="$(printf "%s" ~{{user}})"
        _HOME="$DOTS"
        if [ -z "$DOTS" ]
        then
          echo "Home folder for {{user}} not found" >&2
          exit 1
        fi
        DOTS="${DOTS}/dotfiles"
        if [ ! -d "$DOTS" ]
        then
          MIRRORS=""
          {% for repo in pillar['users'][user]['dotfiles'] %}
          MIRRORS="${MIRRORS} {{repo}}"
          {% endfor %}
          for repo in $MIRRORS
          do
            sudo -u {{user}} -- git clone --depth 1 "$repo" "${DOTS}" || continue
            break # should only be reached if we cloned the repo successfully
          done
          if [ ! -d "$DOTS" ]
          then
            exit 1
          fi
        fi
        if [ -d "$DOTS" ]
        then
          cd "$DOTS"
          /usr/bin/env -i CHECK_DEPS_ONLY=true PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin" busybox sh -x ./bootstrap.sh
          chown {{user}}:{{user}} ~{{user}}
          mkdir -p ~{{user}}/.config ~{{user}}/.local ~{{user}}/.cache
          chown -R {{user}}:{{user}} ~{{user}}/.config ~{{user}}/.local ~{{user}}/.cache
          /usr/bin/env -i sudo -u {{user}} -- env -i DEBUG=1 PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin" busybox sh -x ./bootstrap.sh dotfiles
          exit "$?"
        fi
        {% endfor %}
