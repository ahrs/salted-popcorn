bash:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}app-shells/{% endif %}bash
      - {% if grains['os_family'] == 'Gentoo' %}app-shells/{% endif %}bash-completion
    - install_recommends: False
