include:
  - shells.bash.bash
  {% if 'users' in pillar %}
  {% for user in pillar['users'] %}
  {% if 'shell' in pillar['users'][user] %}
  # /bin/zsh -> shells.zsh.zsh
  {% set shell = pillar['users'][user]['shell'].split('/')|last %}
  # This requires a relatively new version of salt:
  # https://github.com/saltstack/salt/pull/45730
  {% if True or salt['state.sls_exists']('shells.{0}.{0}'.format(shell)) %}
  - shells.{{shell}}.{{shell}}
  {% endif %}
  {% endif %}
  {% endfor %}
  {% endif %}
