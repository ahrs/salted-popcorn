{% set zsh_packages = salt['grains.filter_by']({
  'default': {
    'zsh': 'zsh',
    'zsh-completions': 'zsh-completions',
  },
  'Alpine': {
    'zsh-completions': None,
    'zsh-doc':  'zsh-doc'
  },
  'Arch': {
    'zsh-lovers': 'zsh-lovers'
  },
  'Debian': {
    'zsh-completions': None
  },
  'Gentoo': {
    'zsh':  'app-shells/zsh',
    'zsh-completions': 'app-shells/zsh-completions',
    'zsh-lovers':   'app-doc/zsh-lovers'
  },
  'RedHat': {
    'zsh-lovers': 'zsh-lovers',
    'zsh-completions': None
  },
  'Suse': {
    'zsh-lovers': None,
    'zsh-completions': None,
    'salt-zsh-completion': 'salt-zsh-completion'
  }
}, grain='os_family', merge = salt['pillar.get']('zsh_packages:lookup'), base='default') %}
zsh:
  pkg.installed:
    - pkgs:
      {% for pkg in zsh_packages %}
      {% if zsh_packages[pkg] != None %}
      - {{zsh_packages[pkg]}}
      {% endif %}
      {% endfor %}
