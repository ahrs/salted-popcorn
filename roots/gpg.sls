{% for user in salt['pillar.get']('users', default=[]) %}
{% for key in salt['pillar.get']('users:' + user + ':gpg_keys', default=[])|unique %}
{% set key = key|replace(' ', '') %}
{{user}}_{{key}}:
  gpg.present:
    - name: {{key}}
    - user: {{user}}
{% endfor %}
{% endfor %}
