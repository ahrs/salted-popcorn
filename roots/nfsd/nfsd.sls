{% from "firewall/map.jinja" import firewall with context %}

{% if grains['os_family'] == 'RedHat' %}
nfsd:
  pkg.installed:
    - pkgs:
      - nfs-utils
      - libnfsidmap
{% endif %}

{% if grains['os_family'] == 'Gentoo' %}
nfsd:
  pkg.installed:
    - pkgs:
      - net-fs/nfs-utils
{% endif %}

{% if grains['os_family'] == 'Debian' %}
nfsd:
  pkg.installed:
    - pkgs:
      - rpcbind
      - nfs-kernel-server
{% endif %}

{% if firewall.firewall == 'firewalld' %}
{% if firewall['zone'] != None %}
nfsd_firewalld_{{firewall['zone']}}:
  firewalld.present:
    - name: {{firewall['zone']}}
    - services:
      - mountd
      - nfs
      - nfs3
      - rpc-bind
    - prune_services: False
{% endif %}
{% elif firewall.firewall == 'ufw' %}
nfsd_ufw:
  cmd.run:
    - name: ufw allow from any to any port nfs
{% endif %}
remove_empty_/etc/exports:
  file.absent:
    - name: /etc/exports
    - onlyif:
      - test -z "$(cat /etc/exports)"
/etc/exports:
  file.managed:
    - replace: False
    - mode: 0644
    - user: root
    - group: root
    - source:
      - salt://nfsd/exports
/export:
  file.directory
