/etc/makepkg.conf:
  file.append:
    - text: |
        MAKEFLAGS="-j{{ salt['grains.get']('num_cpus', default=2) + 1 }}"
        BUILDENV=(!distcc color ccache check !sign)
aur:
  user.present:
    - home: /var/tmp/aur
    - createhome: True
    - shell: /bin/nologin
    - system: True
aur_sudoers:
  file.append:
    - name: /etc/sudoers
    - text:
      - "aur ALL=(ALL) NOPASSWD: /usr/bin/pacman"
include:
  - common.base.base
package_managers:
  pkg.installed:
    - pkgs:
      - asp
      - arch-audit
      - pkgfile
      - aria2
      - flatpak
    - cmd.run:
      - name: |
          pkgfile --update
filesystems:
  pkg.installed:
    - pkgs:
      - btrfs-progs
      - dosfstools
      - f2fs-tools
 #     - linux-headers
 #     - linux-macbook
 #     - linux-macbook-headers
      - mtools
      - ntfs-3g
      - squashfs-tools
      - xfsprogs
system:
  pkg.installed:
    - pkgs:
      - reflector
network:
  pkg.installed:
    - pkgs:
#      - b43-fwcutter
#      - broadcom-wl
#      - broadcom-wl-dkms
      - bind-tools
      - intel-ucode
#      - ipw2100-fw
#      - ipw2200-fw
      - linux-firmware
      - modemmanager
      - netctl
      - net-tools
      - nss-mdns
      - usb_modeswitch
      - whois
      - wireless_tools
      - wpa_actiond
      - avahi
      - dnscrypt-proxy
      - dnsmasq
      - httping
      - httrack
      - mitmproxy
      - speedtest-cli
      - tor
      - torsocks
      - fail2ban
xorg:
  pkg.installed:
    - pkgs:
      {% if grains['virtual'] == "VirtualBox" %}
      - virtualbox-guest-modules-arch
      - virtualbox-guest-utils
      {% endif %}
      - xf86-input-libinput
      - xf86-video-amdgpu
      - xf86-video-ati
      - xf86-video-intel
      - xf86-video-dummy
      - xf86-video-nouveau
      - xf86-video-vesa
      - xorg-server
#     - xorg-server-utils
      - xorg-xinit
      - xterm
#nvidia:
#  pkg.installed:
#    - pkgs:
#      # - nvidia
#      # - nvidia-utils
#      # - nvidia-libgl
#      # - nvidia-settings
