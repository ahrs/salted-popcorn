{% from "common/Arch/aur_install.jinja" import yay with context %}

include:
  - applications.utilities.yay.yay

{% for pkg in [
  'dtrx',
  'systemd-cron',
  'xkcdpass'
] %}
{{pkg}}:
{{yay([pkg])}}
{% endfor %}
