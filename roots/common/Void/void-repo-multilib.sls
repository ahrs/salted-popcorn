{% set cpuarch = salt['grains.get']('cpuarch', default='x86_64') %}
{% if cpuarch == 'x86_64' %}
void-repo-multilib:
  pkg.installed
{% endif %}
