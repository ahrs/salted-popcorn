{% from "common/base/map.jinja" import common_packages with context %}

{% if grains['os_family'] == "Void" %}
glibc-locales:
  pkg.installed:
    - pkgs:
      - glibc-locales
    - onlyif:
      - test "$(/lib/libc.so 2>&1 | grep -oE -m 1 'musl libc' | head -1)" != "musl libc"
/etc/default/libc-locales:
  file.uncomment:
    - regex: en_GB.UTF-8 UTF-8
    - onlyif:
      - test -f /etc/default/libc-locales
  cmd.run:
    - name: xbps-reconfigure -f glibc-locales
    - onlyif:
      - test "$(/lib/libc.so 2>&1 | grep -oE -m 1 'musl libc' | head -1)" != "musl libc"
uncomment_TIMEZONE:
  file.uncomment:
    - name: /etc/rc.conf
    - regex: TIMEZONE=
Europe/London:
  file.replace:
    - name: /etc/rc.conf
    - pattern: TIMEZONE=.*
    - repl: TIMEZONE="Europe/London"
    - append_if_not_found: True
syslog:
  pkg.installed:
    - pkgs:
      - socklog
      - socklog-void
  cmd.run:
    - name: |
        ln -fs /etc/sv/socklog-unix /var/service
{% elif grains['os_family'] == 'Alpine' %}
tzdata:
  pkg.installed
Europe/London:
  cmd.run:
    - name: |
        set -e -x
        ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime
    - onlyif: apk info -v | grep -q -E '^tzdata-'
{% elif grains['os_family'] == 'Arch' %}
/etc/locale.gen:
  file.uncomment:
    - regex: en_GB.UTF-8 UTF-8
  cmd.run:
    - name: locale-gen
/etc/locale.conf:
  file.managed:
    - contents: |
        LANG="en_GB.UTF-8"
{% else %}

{% if grains['os_family'] == 'RedHat' %}
langpacks-en:
  pkg.installed
{% endif %}

# Use Britsh English...
gb_locale:
  locale.present:
    - name: en_GB.UTF-8

default_locale:
  locale.system:
    - name: en_GB.UTF-8
    - require:
      - locale: gb_locale

# Sets the system timezone to Europe/London
# as an offset of UTC
# https://help.ubuntu.com/community/UbuntuTime#Multiple_Boot_Systems_Time_Conflicts
Europe/London:
  timezone.system:
    - utc: True
    - unless:
      - systemd-detect-virt --chroot
{% endif %}

include:
  {% if grains['os_family'] == 'Gentoo' %}
  - common.Gentoo.local_overlay
  - common.Gentoo.kernel
  {% endif %}
  - applications.programming.tools.ccache.ccache
  - applications.programming.tools.git.git
  - applications.utilities.fzf.fzf
  - applications.utilities.ripgrep.ripgrep

common_packages:
  pkg.installed:
      - pkgs:
        {% for package in common_packages %}
        {% if common_packages[package] != None and common_packages[package]|length > 0 %}
        - {{common_packages[package]}}
        {% endif %}
        {% endfor %}
      - install_recommends: False
      {% if grains['os_family'] == 'Gentoo' %}
      - require:
        - sls: common.Gentoo.local_overlay
        - sls: common.Gentoo.kernel
      {% endif %}
