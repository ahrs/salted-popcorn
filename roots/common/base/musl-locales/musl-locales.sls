include:
  - applications.utilities.stow.stow

musl-locales_deps:
  pkg.installed:
    - pkgs:
      - cmake
      - make
      - musl-dev
      - gcc
      - gettext-dev
      - libintl
      - sudo
    - check_cmd:
      # assume dependencies are installed
      - /bin/true
musl-locales_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/musl-locales
musl-locales_clone_mirror:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/musl-locales
    - user: nobody
    - url:  http://192.168.0.11:10080/ahrs/musl-locales.git
    - opts: --depth 1
    failhard: False
musl-locales_clone_upsteam:
  module.run:
    - url:  https://github.com/rilian-la-te/musl-locales.git
    - use:
      - module: musl-locales_clone_mirror
    - onfail:
      - module: musl-locales_clone_mirror

musl-locales:
  cmd.run:
    - name: |
        set -e
        set -x
        sudo -u nobody mkdir -p /tmp/musl-locales/build
        cd /tmp/musl-locales/build
        sudo -u nobody cmake ..
        sudo -u nobody make
        rm -rf /stow/musl-locales.tmp
        install -dm0775 -o nobody -g nobody /stow/musl-locales.tmp
        if [ -d /stow/musl-locales ]
        then
          cd /stow/musl-locales
          stow --verbose=3 -t /usr -D usr
          cd -
          mv /stow/musl-locales /stow/musl-locales.bak
        fi
        /usr/bin/env -i sudo -u nobody -- make DESTDIR=/stow/musl-locales.tmp install || {
          cd /stow
          if [ -d musl-locales.bak ]
          then
            rm -rf musl-locales
            rm -rf musl-locales.tmp
            mv musl-locales.bak musl-locales
            cd musl-locales
            stow --verbose=3 -t /usr usr
          fi
          exit 1
        }
        cd /stow
        mv musl-locales.tmp musl-locales
        chown -R root:root musl-locales
        cd musl-locales
        stow --verbose=3 -t /usr usr
        rm -rf /stow/musl-locales.bak
