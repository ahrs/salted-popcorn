{% set sensible_fonts = salt['grains.filter_by']({
  'default': {
    'opendesktop-fonts':  'opendesktop-fonts',
    'ttf-arphic-uming':   'ttf-arphic-uming',
    'ttf-baekmuk':        'ttf-baekmuk',
    'ttf-bitstream-vera': 'ttf-bitstream-vera',
    'ttf-dejavu':         'ttf-dejavu',
    'ttf-freefont':       'ttf-freefont'
  },
  'Alpine': {
    'opendesktop-fonts':  None,
    'ttf-arphic-uming':   None,
    'ttf-baekmuk':        None,
    'ttf-bitstream-vera': 'font-bitstream-100dpi',
    'font-bitstream-75dpi': 'font-bitstream-75dpi',
    'font-bitstream-type1': 'font-bitstream-type1',
    'font-bitstream-speedo': 'font-bitstream-speedo'
  },
  'Debian': {
    'opendesktop-fonts':  None,
    'ttf-arphic-uming':   'fonts-arphic-uming',
    'ttf-baekmuk':        'fonts-baekmuk',
    'ttf-freefont':       'fonts-freefont-ttf'
   },
   'Gentoo': {
    'opendesktop-fonts':  'media-fonts/opendesktop-fonts',
    'ttf-arphic-uming':   'media-fonts/arphicfonts',
    'ttf-baekmuk':        'media-fonts/baekmuk-fonts',
    'ttf-bitstream-vera': 'media-fonts/ttf-bitstream-vera',
    'ttf-dejavu':         'media-fonts/dejavu',
    'ttf-freefont':       'media-fonts/freefont'
   },
   'RedHat': {
    'opendesktop-fonts': None,
    'ttf-arphic-uming': 'cjkuni-uming-fonts',
    'ttf-baekmuk':  'baekmuk-ttf-dotum-fonts',
    'ttf-baekmuk-gulim': 'baekmuk-ttf-gulim-fonts',
    'ttf-baekmuk-batang': 'baekmuk-ttf-batang-fonts',
    'ttf-baekmuk-hline': 'baekmuk-ttf-hline-fonts',
    'ttf-bitstream-vera': 'bitstream-vera-sans-fonts',
    'ttf-bitstream-vera-serif': 'bitstream-vera-serif-fonts',
    'ttf-bitstream-vera-common': 'bitstream-vera-fonts-common',
    'ttf-bitstream-vera-sans-mono': 'bitstream-vera-sans-mono-fonts',
    'ttf-dejavu': 'dejavu-serif-fonts',
    'ttf-dejavu-sans':  'dejavu-sans-fonts',
    'ttf-freefont':       'gnu-free-serif-fonts',
    'ttf-freefont-sans':  'gnu-free-sans-fonts',
    'ttf-freefont-mono':  'gnu-free-mono-fonts'
   },
   'Suse': {
    'opendesktop-fonts': None,
    'ttf-arphic-uming': 'arphic-uming-fonts',
    'ttf-baekmuk':  'baekmuk-ttf-fonts',
    'ttf-bitstream-vera': 'bitstream-vera-fonts',
    'ttf-dejavu': 'dejavu-fonts',
    'ttf-freefont': 'texlive-gnu-freefont'
   },
   'Void': {
    'opendesktop-fonts':  None,
    'ttf-baekmuk':  None,
    'ttf-arphic-uming': None,
    'ttf-dejavu': 'dejavu-fonts-ttf',
    'ttf-freefont': 'freefont-ttf',
    'ttf-bitstream-vera': 'font-bitstream-100dpi',
    'ttf-bitstream-vera-75dpi': 'font-bitstream-75dpi',
    'ttf-bitstream-vera-speedo':  'font-bitstream-speedo',
    'ttf-bitstream-vera-type1': 'font-bitstream-type1'
   }
}, grain='os_family', merge = salt['pillar.get']('sensible_fonts:lookup'), base='default') %}
include:
  - fonts.liberation.liberation
  # To Do:
  #   * Do the split font package dance with Suse too
  {% if grains['os_family'] != 'Suse' %}
  - fonts.noto.noto
  {% endif %}
  {% if grains['os_family'] != 'Alpine' %}
  - fonts.roboto.roboto
  {% endif %}
fonts:
  pkg.installed:
    - pkgs:
      {% for pkg in sensible_fonts %}
      {% if sensible_fonts[pkg] != None %}
      - {{sensible_fonts[pkg]}}
      {% endif %}
      {% endfor %}
    - install_recommends: False
