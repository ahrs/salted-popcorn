{% if grains['os'] == 'Fedora' %}
rpm_fusion:
  cmd.run:
    - name: |
        set -e -x
        dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    - onlyif:
      - "! test -f /etc/yum.repos.d/rpmfusion-free.repo"
      - "! test -f /etc/yum.repos.d/rpmfusion-nonfree.repo"
    - check_cmd:
      - "test -f /etc/yum.repos.d/rpmfusion-free.repo"
      - "test -f /etc/yum.repos.d/rpmfusion-nonfree.repo"
{% endif %}
