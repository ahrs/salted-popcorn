# A chrome(ium) extension that advertises your user agent as "Fedora"
# full credit to Fedora for doing this via an extension. On Ubuntu it's
# hardcoded in the source as "Ubuntu Chromium".
fedora-user-agent-chrome:
  pkg.purged

# Purged for the same reason as Debian/Ubuntu
# (excessive phoning home at repeat intervals for no reason)
NetworkManager-config-connectivity-fedora:
  pkg.purged
