include:
  {% if grains['os_family'] == 'Gentoo' %}
  - common.Gentoo.kernel
  - common.Gentoo.local_overlay
  - common.Gentoo.Gentoo
  - common.Gentoo.make_dot_conf
  - common.Gentoo.common_packages
  - common.Gentoo.qt
  {% endif %}
  {% if grains['os_family'] == 'Arch' %}
  - common.Arch.Arch
  - common.Arch.common_packages
  {% endif %}
  {% if grains['os_family'] == 'Debian' %}
  - common.Debian.purged_packages
  - common.Debian.popularity-contest
  {% endif %}
  {% if grains['os_family'] == 'RedHat' %}
  {% if grains['os'] != 'Fedora' %}
  # On RedHat systems we want to make sure epel-release is installed
  - common.RedHat.epel
  {% else %}
  - common.RedHat.Fedora.purged_packages
  - common.RedHat.rpmfusion
  {% endif %}
  - common.RedHat.development_tools
  {% endif %}
  - common.base.base
  - common.sensible-fonts
{% if grains['os_family'] == 'RedHat' and grains['init'] == 'systemd' %}
systemd-cron:
  pkg.installed:
    - pkgs:
      - curl
      - git
      - sudo
  cmd.run:
    - name: |
        set -e -x
        sudo="/usr/bin/env -i -- sudo -u nobody --"
        command -v dnf > /dev/null 2>&1 && pkgmgr="dnf" || pkgmgr="yum"

        $pkgmgr groupinstall -y {% if grains['os'] == 'CentOS' %}"Fedora Packager"{% else %}"RPM Development Tools"{% endif %}
        cd /tmp
        rm -rf systemd-cron
        _HOME="$($sudo mktemp -d)"
        tag="$(curl -s -L https://github.com/systemd-cron/systemd-cron/releases.atom | grep -oE 'https://github.com/systemd-cron/systemd-cron/releases/tag/v[0-9.]+' | awk -F '/' '{print $NF}' | head -1 | sed -e 's|\s||g' -e 's| ||g' -e 's|^v||g')"
        $sudo git clone --depth 1 https://github.com/systemd-cron/systemd-cron.git systemd-cron
        cd systemd-cron/contrib
        sed -i "s|^Version:        .*|Version:        ${tag}|g" systemd-cron.spec
        $sudo mkdir -p "$_HOME/rpmbuild/SOURCES"
        $sudo curl -L -o "$_HOME/rpmbuild/SOURCES/v${tag}.tar.gz" "https://github.com/systemd-cron/systemd-cron/archive/v${tag}.tar.gz"
        $sudo env -i HOME="$_HOME" rpmbuild -bb -v systemd-cron.spec
        [ "$pkgmgr" = "dnf" ] && opts="--best --allowerasing" || opts=""
        rpm -e --nodeps cronie || true
        $pkgmgr -y $opts install "$_HOME"/rpmbuild/RPMS/noarch/*.rpm || true
        rm -rf "$_HOME"
        cd /tmp
        rm -rf /tmp/systemd-cron
    - check_command: rpm -q systemd-cron
{% elif grains['os_family'] != 'Arch' %}
cron:
  {% if grains['init'] == 'systemd' %}
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Suse' %}cronie{% else %}systemd-cron{% endif %}
  {% else %}
  pkg.installed:
    - pkgs:
      {% if grains['os_family'] == 'Debian' %}
      - cron
      {% elif grains['os_family'] == 'Alpine' %}
      - busybox
      {% else %}
      - {% if grains['os_family'] == 'Gentoo' %}sys-process/{% endif %}cronie
      {% endif %}
  {% if grains['os_family'] == 'Void' %}
  cmd.run:
    - name: |
        ln -fs /etc/sv/cronie /var/service
  {% elif grains['init'] != 'unknown' %}
  service.enabled:
    - name: cronie
  {% endif %}
  {% endif %}
{% endif %}
