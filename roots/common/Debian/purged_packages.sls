# Network Manager connectivity checks phone home excessively
# This excessive phonining home is pointless. Once or twice
# when activating a network connection is fine, at regular
# intervals is excessive and just "noise".
network-manager-connectivity-checks:
  pkg.purged:
    - pkgs:
      - network-manager-config-connectivity-ubuntu
      - network-manager-config-connectivity-debian

# Amazon "spam"
ubuntu-web-launchers:
  pkg.purged
