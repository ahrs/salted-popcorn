# The popularity-contest package sets up a cron job that will
# periodically anonymously submit statistics about the most used
# packages on this system.
popularity-contest:
  pkg.installed

# Opt-In
/etc/popularity-contest.conf:
  file.replace:
    - pattern: PARTICIPATE="no"
    - repl: PARTICIPATE="yes"
    - append_if_not_found: True
    - require:
      - pkg: popularity-contest
