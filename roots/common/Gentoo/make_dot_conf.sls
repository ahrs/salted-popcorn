{% from "common/Gentoo/map.jinja" import gentoo with context %}
{% from "gpus.jinja" import video_cards with context %}

{% if "nvidia" in video_cards %}
{% set nvidia = "nvidia" %}
{% else %}
{% set nvidia = "" %}
{% endif %}

python_single_target:
  makeconf.present:
    - value: python3_6

python_targets:
  makeconf.present:
    - value: python2_7 python3_6 python3_7

features:
  makeconf.present:
    - value: {% for feature in (salt['pillar.get']('make.conf:features', default=[]))|unique|sort %}{{feature}} {% endfor %}

makeopts:
  makeconf.present:
    - value: {{salt['pillar.get']('make.conf:makeopts', default="-j%s"|format(grains['num_cpus']))}}
cflags:
  makeconf.present:
    - value: {{salt['pillar.get']('make.conf:cflags', default='-O3 -pipe -march=native')}}
cxxflags:
  makeconf.present:
    - value: {{salt['pillar.get']('make.conf:cxxflags', default='$CFLAGS')}}
use:
  makeconf.present:
    - value: {% for useflag in (salt['pillar.get']('make.conf:use', default=[]) + [gentoo.ssl_lib] + [nvidia])|unique|sort %}{{useflag}} {% endfor %}

video_cards:
  makeconf.present:
    - value: {% for video_card in video_cards %}{% if video_card == 'intel' %}intel i915 i965{% elif video_card == 'nvidia' %} nvidia nouveau {% endif %}{% endfor %}
