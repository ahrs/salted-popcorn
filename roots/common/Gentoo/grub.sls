# needed by os-prober
use_sys-boot/grub:
  portage_config.flags:
    - name: sys-boot/grub
    - use:
      - mount
sys-boot/grub:
  pkg.installed
sys-boot/os-prober:
  pkg.installed
