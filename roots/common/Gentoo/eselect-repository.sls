# eselect-repository doesn't directly depend on git
# but most overlays won't be able to sync without it
include:
  - applications.programming.tools.git.git

app-eselect/eselect-repository:
  pkg.installed:
    - pkgs:
      - app-eselect/eselect-repository
    - require:
      - sls: applications.programming.tools.git.git
