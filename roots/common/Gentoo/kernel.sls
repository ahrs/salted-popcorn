# Mask all stable kernels so we use the latest upstream LTS kernel series
# (4.14.76 as I write this)
# To Do:
#   * This should be behind a boolean flag in the pillar (I want the latest stable kernel on my laptops, the LTS on my desktop)
mask_linux-headers-4.19:
  portage_config.flags:
    - name: ">=sys-kernel/linux-headers-4.20"
    - mask: True
{% for kernel in ['vanilla-sources','gentoo-sources','ck-sources', 'zen-sources'] %}
mask_{{kernel}}-4.20.0:
  portage_config.flags:
    - name: ">=sys-kernel/{{kernel}}-4.20.0"
    - mask: True
unmask_{{kernel}}-4.19.0:
  portage_config.flags:
    - name: "<sys-kernel/{{kernel}}-4.20.0"
    - unmask: True
    - accept_keywords:
      - "~*"
{% if salt['pillar.get']('bootstrap', default=True) == True %}
    - use:
      - symlink
{% endif %}
{% endfor %}
sys-kernel/gentoo-sources:
  pkg.installed
">=sys-apps/util-linux-2.33-r1":
  portage_config.flags:
    - use:
      - static-libs
defconfig:
  cmd.run:
    - name: |
        set -e -x
        cd /usr/src/linux
        defconfig() {
          make mrproper
          make defconfig
          make prepare
          make modules_prepare
          make -j$(($(nproc)+1))
        }
        {% if salt['pillar.get']('bootstrap', default=True) == True %}
        defconfig
        {% else %}
        if [ ! -f .config ]
        then
          defconfig
        else
          make prepare
        fi
        {% endif %}
