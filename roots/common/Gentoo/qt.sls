{% from "common/Gentoo/map.jinja" import gentoo with context %}

include:
  - common.Gentoo.local_overlay

# This doesn't belong here
# To Do:
#   * Find a better place for this 
{% if gentoo.ssl_lib|lower == 'libressl' %}
{% if salt['pillar.get']('bootstrap', default=True) == True %}
ensure_curl_is_built_with_libressl:
  cmd.run:
    - name: |
        set -e
        set -x
        env CURL_SSL="libressl" emerge -v1 --usepkg=n --usepkg-exclude '*' --getbinpkg=n net-misc/curl dev-vcs/git app-misc/ca-certificates
{% endif %}
{% endif %}

qt-overlay:
  module.run:
    - name: eselect.exec_action
    - module: repository
    - action: enable
    - action_parameter: qt
    - require:
      - sls: common.Gentoo.local_overlay
  cmd.run:
    - name: emerge --sync qt

# Use 5.11.9999 for now since I know this works
# and have libressl patches in my overlay.
# To Do:
#   * Investigate upgrading to 5.12.9999
# set qtver = '5.11.9999'
{% set qtver = '5.11.3' %}

{% for qtpkg in [
  'dev-qt/assistant',
  'dev-qt/designer',
  'dev-qt/linguist-tools',
  'dev-qt/linguist',
  'dev-qt/pixeltool',
  'dev-qt/qdbus',
  'dev-qt/qdbusviewer',
  'dev-qt/qdoc',
  'dev-qt/qt-creator',
  'dev-qt/qt-docs',
  'dev-qt/qt3d',
  'dev-qt/qtbluetooth',
  'dev-qt/qtcharts',
  'dev-qt/qtconcurrent',
  'dev-qt/qtcore',
  'dev-qt/qtdatavis3d',
  'dev-qt/qtdbus',
  'dev-qt/qtdeclarative',
  'dev-qt/qtdiag',
  'dev-qt/qtgraphicaleffects',
  'dev-qt/qtgui',
  'dev-qt/qthelp',
  'dev-qt/qtimageformats',
  'dev-qt/qtlocation',
  'dev-qt/qtlockedfile',
  'dev-qt/qtmultimedia',
  'dev-qt/qtnetwork',
  'dev-qt/qtnetworkauth',
  'dev-qt/qtopengl',
  'dev-qt/qtpaths',
  'dev-qt/qtplugininfo',
  'dev-qt/qtpositioning',
  'dev-qt/qtprintsupport',
  'dev-qt/qtquickcontrols',
  'dev-qt/qtquickcontrols2',
  'dev-qt/qtscript',
  'dev-qt/qtscxml',
  'dev-qt/qtsensors',
  'dev-qt/qtserialbus',
  'dev-qt/qtserialport',
  'dev-qt/qtsingleapplication',
  'dev-qt/qtspeech',
  'dev-qt/qtsql',
  'dev-qt/qtsvg',
  'dev-qt/qttest',
  'dev-qt/qttranslations',
  'dev-qt/qtvirtualkeyboard',
  'dev-qt/qtwayland',
  'dev-qt/qtwebchannel',
  'dev-qt/qtwebengine',
  'dev-qt/qtwebkit',
  'dev-qt/qtwebsockets',
  'dev-qt/qtwebview',
  'dev-qt/qtwidgets',
  'dev-qt/qtx11extras',
  'dev-qt/qtxml',
  'dev-qt/qtxmlpatterns'
] %}
accept_{{qtpkg}}-5.11.9999:
  portage_config.flags:
    - name: "<={{qtpkg}}-5.11.9999-r1"
    - accept_keywords:
      - "**"
{% for vers in ['5.9999','5.12.9999','5.12.0'] %}
mask_{{qtpkg}}-{{vers}}:
  portage_config.flags:
    - name: "={{qtpkg}}-{{vers}}"
    - mask: True
{% endfor %}
mask_{{qtpkg}}-5.11.1:
  portage_config.flags:
    - name: "<={{qtpkg}}-5.11.2"
    - mask: True
unmask_{{qtpkg}}-{{qtver}}:
  portage_config.flags:
    - name: "={{qtpkg}}-{{qtver}}"
    - unmask: True
{% endfor %}
{% for pkg in ['dev-qt/qt-docs', 'dev-qt/qtlockedfile', 'dev-qt/qtsingleapplication'] %}
unmask_{{pkg}}:
  portage_config.flags:
    - name: {{pkg}}
    - unmask: True
{% endfor %}
install_qt_packages:
  cmd.run:
    - name: |
        set -e -x
        pkgs=">=dev-qt/qtcore-{{qtver}} >=dev-qt/qtwidgets-{{qtver}} {% if gentoo.ssl_lib|lower == 'libressl' %}>=dev-qt/qtnetwork-{{qtver}}::libressl{% else %}>=dev-qt/qtnetwork-{{qtver}}{% endif %} >=dev-qt/qtdbus-{{qtver}} >=dev-qt/qtwidgets-{{qtver}} >=dev-qt/qtsvg-{{qtver}} >=dev-qt/qtxml-{{qtver}} >=dev-qt/qtgui-{{qtver}}"
        is_already_installed() {
          set +x
          for pkg in $pkgs
          do
            _pkg="$(printf "%s" "$pkg" | cut -c 2-)"
            if [ ! -d /var/db/pkg/$_pkg ]
            then
              set -x
              return 1
            fi
          done
          set -x

          return 0
        }
        if is_already_installed
        then
          exit 0
        fi
        emerge -v1 $pkgs
