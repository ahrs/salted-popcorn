include:
  - applications.programming.tools.git.git
  - common.Gentoo.eselect-repository

/etc/portage/repos.conf:
  file.directory

/etc/portage/repos.conf/localrepo.conf:
  file.managed:
    - contents: |
        [ahrs]
        location = /usr/local/portage
        priority = 1000

#- name: https://gitlab.com/ahrs/gentoo-stuff.git
#- name:  http://192.168.0.11:10080/ahrs/gentoo-stuff.git
localrepo:
  git.latest:
    - name:  http://192.168.0.11:10080/ahrs/localrepo.git
    - target: /usr/local/portage
    - require:
      - sls: applications.programming.tools.git.git
  cmd.run:
    - name: |
        set -e -x
        test -d /usr/local/portage
        chown -R portage:portage /usr/local/portage

go-overlay:
  module.run:
    - name: eselect.exec_action
    - module: repository
    - action: enable
    - action_parameter: go-overlay
    - require:
      - sls: applications.programming.tools.git.git
      - sls: common.Gentoo.eselect-repository
  cmd.run:
    - name: emerge --sync go-overlay
