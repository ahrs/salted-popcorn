{% from "common/Gentoo/map.jinja" import gentoo with context %}

include:
  - applications.programming.tools.pkg-config.pkg-config
  - common.Gentoo.local_overlay
  - common.Gentoo.kernel

{% set world_upgrade = 'emerge -quDN --tree --keep-going @world' %}

profile:
  eselect.set:
    - target: {{gentoo.profile}}

enforce_nice_config:
  module.run:
    - name: portage_config.enforce_nice_config
# Mask all non-git salt's until #47083 is fixed:
# pkg.installed broken in Gentoo in 2018.3
# (https://github.com/saltstack/salt/issues/47083)
"<app-admin/salt-9999":
  portage_config.flags:
    - mask: True

"<app-text/mupdf-1.15.0":
  portage_config.flags:
    - accept_keywords:
      - ~*

"<media-plugins/alsa-plugins-1.2.0":
  portage_config.flags:
    - accept_keywords:
      - ~*

# required by media-plugins/alsa-plugins-1.1.7::gentoo
"<media-libs/alsa-lib-1.2.0":
  portage_config.flags:
    - accept_keywords:
      - ~*

"/etc/portage/package.use/virtual/python-cffi":
  file.absent
"virtual/python-cffi":
  portage_config.flags:
    - use:
      - "PYTHON_TARGETS: python3_7"
    - require:
      - file: "/etc/portage/package.use/virtual/python-cffi"

"/etc/portage/package.use/dev-python":
  file.directory

"/etc/portage/package.use/dev-python/certifi":
  file.absent

">=dev-python/certifi-2018.11.29":
  portage_config.flags:
    - use:
      - "PYTHON_TARGETS: python2_7 python3_6 python3_7"
    - require:
      - file: "/etc/portage/package.use/dev-python/certifi"

"/etc/portage/package.use/dev-python/setuptools":
  file.managed:
    - contents: |
        dev-python/setuptools PYTHON_TARGETS: python2_7 python3_6 python3_7
    - require:
      - file: "/etc/portage/package.use/dev-python"

"/etc/portage/package.use/dev-python/pycryptodome":
  file.absent
"dev-python/pycryptodome":
  portage_config.flags:
    - use:
      - "PYTHON_TARGETS: -python3_7"
      - "PYTHON_TARGET: -python3_7"
    - require:
      - file: "/etc/portage/package.use/dev-python/pycryptodome"

{% for pkg in ['dev-lang/python-exec'] %}
accept_{{pkg}}:
  portage_config.flags:
    - name: {{pkg}}
    - accept_keywords:
      - "~*"
{% endfor %}

{% for pkg in ['dev-python/certifi'] %}
accept_{{pkg}}:
  portage_config.flags:
    - name: {{pkg}}
    - accept_keywords:
      - ~*
{% endfor %}

# Unkeyword python3.7
dev-lang/python:
  portage_config.flags:
    - name: "<dev-lang/python-3.8.0"
    - accept_keywords:
      - ~*
    {% if salt['pillar.get']('bootstrap', default=True) == True %}
    - use:
      - "-bluetooth"
    {% else %}
    - use:
      - "bluetooth"
      - "sqlite"
      - "ssl"
    {% endif %}
  cmd.run:
    - name: |
        set -e -x
        if [ -z "$(find /var/db/pkg/dev-lang -type d -name "python-3.7*")" ]
        then
          env ACCEPT_KEYWORDS="~*" PYTHON_TARGETS="python2_7 python3_6 python3_7" MAKEOPTS="-j1" emerge -v -j1 --usepkg=n --with-bdeps=y --usepkg-exclude '*' dev-lang/python:3.7 dev-python/setuptools dev-python/cffi
        fi
    - failhard: True
unkeyword_setuptools:
  portage_config.flags:
    - name: "dev-python/setuptools"
    - accept_keywords:
      - "~*"

app-text/asciidoc_use_directory:
  file.directory:
    - name: "/etc/portage/package.use/app-text"
app-text/asciidoc:
  file.managed:
    - name: "/etc/portage/package.use/app-text/asciidoc"
    - contents: |
        app-text/asciidoc PYTHON_TARGETS: python3_7
    - require:
      - file: app-text/asciidoc_use_directory

{% set sphinx_deps = [
  {
    'atom': "<dev-python/six-1.12.0",
    'pkg':  "dev-python/six",
    'comment': "Required by sphinxcontrib-websupport"
  },
  {
    'atom': "<dev-python/alabaster-0.8.0",
    'pkg':  "dev-python/alabaster"
  },
  {
    'atom': "<dev-python/sphinx_rtd_theme-0.3.0",
    'pkg':  "dev-python/sphinx_rtd_theme"
  },
  {
    'atom': "<dev-python/snowballstemmer-1.3.0",
    'pkg':  "dev-python/snowballstemmer"
  },
  {
    'atom': "<dev-python/imagesize-1.1.0",
    'pkg':  "dev-python/imagesize"
  },
  {
    'atom': '<dev-python/jinja-2.11',
    'pkg':  'dev-python/jinja'
  },
  {
    'atom': '<dev-python/markupsafe-1.1',
    'pkg':  'dev-python/markupsafe',
    'comment': 'needed by dev-python/jinja'
  },
  {
    'atom': '<dev-python/requests-2.22.0',
    'pkg':  'dev-python/requests'
  },
  {
    'atom': '<dev-python/chardet-3.1.0',
    'pkg':  'dev-python/chardet',
    'comment': 'required by dev-python/requests'
  },
  {
    'atom': '<dev-python/pyopenssl-18.0.0',
    'pkg':  'dev-python/pyopenssl',
    'comment': 'required by dev-python/requests'
  },
  {
    'atom': '<dev-python/pycparser-2.19',
    'pkg':  'dev-python/pycparser',
    'comment': [
      '# required by dev-python/cryptography-2.3::gentoo[python_targets_python2_7,-python_targets_python3_4,-python_targets_python3_5,python_targets_python3_6,python_targets_python3_7]',
      '# required by dev-python/requests-2.19.1-r1::gentoo[ssl]'
    ]
  },
  {
    'atom': '<dev-python/ply-3.12',
    'pkg':  'dev-python/ply',
    'comment': [
      '# required by dev-python/pycparser-2.18-r1::gentoo',
      '# required by dev-python/cffi-1.11.5::gentoo',
      '# required by dev-python/cryptography-2.3::gentoo[python_targets_python2_7,-python_targets_python3_4,-python_targets_python3_5,python_targets_python3_6,python_targets_python3_7]',
      '# required by dev-python/requests-2.19.1-r1::gentoo[ssl]'
    ]
  },
  {
    'atom': '<dev-python/PySocks-1.7.0',
    'pkg':  'dev-python/PySocks',
    'comment': [
      '# required by dev-python/urllib3-1.23::gentoo',
      '# required by dev-python/requests-2.19.1-r1::gentoo'
    ]
  },
  {
    'atom': '<dev-python/cryptography-2.5',
    'pkg':  'dev-python/cryptography',
    'comment': [
      '# required by dev-python/pyopenssl-17.5.0::gentoo',
      '# required by dev-python/requests-2.19.1-r1::gentoo[ssl]'
    ]
  },
  {
    'atom': '<dev-python/idna-2.8',
    'pkg':  'dev-python/idna',
    'comment':  'required by dev-python/requests-2.19.1-r1::gentoo'
  },
  {
    'atom': '<dev-python/asn1crypto-0.25.0',
    'pkg':  'dev-python/asn1crypto',
    'comment': [
      '# required by dev-python/cryptography-2.3::gentoo',
      '# required by dev-python/requests-2.19.1-r1::gentoo[ssl]'
    ]
  },
  {
    'atom': '<dev-python/urllib3-1.24',
    'pkg':  'dev-python/urllib3',
    'comment': '# required by dev-python/requests-2.19.1-r1::gentoo'
  },
  {
    'atom': '<dev-python/cffi-1.12.0',
    'pkg': 'dev-python/cffi',
    'comment': [
      '# required by dev-python/cryptography-2.3::gentoo[python_targets_python2_7,-python_targets_python3_4,-python_targets_python3_5,python_targets_python3_6,python_targets_python3_7]',
      '# required by dev-python/requests-2.19.1-r1::gentoo[ssl]'
    ]
  },
  {
    'atom': 'virtual/python-ipaddress',
    'pkg':  'virtual/python-ipaddress'
  },
  {
    'atom': "<dev-python/sqlalchemy-1.3.0",
    'pkg':  "dev-python/sqlalchemy"
  },
  {
    'atom': "<dev-python/whoosh-2.8.0",
    'pkg':  "dev-python/whoosh"
  },
  {
    'atom': "<dev-python/namespace-sphinxcontrib-2.0",
    'pkg':  "dev-python/namespace-sphinxcontrib"
  },
  {
    'atom': "<dev-python/pyparsing-2.3.0",
    'pkg':  "dev-python/pyparsing"
  },
  {
    'atom': "<dev-python/pytz-2019.0",
    'pkg':  "dev-python/pytz"
  },
  {
    'atom': "<dev-python/typing-3.7.0",
    'pkg':  "dev-python/typing"
  },
  {
    'atom': "<dev-python/Babel-2.7.0",
    'pkg': "dev-python/Babel"
  },
  {
    'atom': '<dev-python/docutils-1.0',
    'pkg':  'dev-python/docutils'
  },
  {
    'atom': '<dev-python/pygments-2.3.0',
    'pkg':  'dev-python/pygments',
    'comment': 'needed by docutils'
  },
  {
    'atom': '<dev-python/html5lib-1.1.0',
    'pkg': 'dev-python/html5lib'
  },
  {
    'atom': '<dev-python/webencodings-0.6.0',
    'pkg':  'dev-python/webencodings',
    'comment': 'required by html5lib'
  },
  {
    'atom': "<dev-python/packaging-17.2",
    'pkg':  "dev-python/packaging"
  },
  {
    'atom': '<dev-python/simplejson-3.17.0',
    'pkg': 'dev-python/simplejson'
  },
  {
    'atom': "virtual/python-typing",
    'pkg': "virtual/python-typing"
  },
  {
    'atom': "<dev-python/sphinxcontrib-websupport-1.2.0",
    'pkg':  "dev-python/sphinxcontrib-websupport"
  }
] %}

{% for dep in sphinx_deps %}
"/etc/portage/package.use/{{dep.pkg}}":
  file.absent
{{dep.atom}}:
  portage_config.flags:
    - accept_keywords:
      - ~*
    - use:
      - "PYTHON_TARGETS: python3_7"
    - require:
      - file: "/etc/portage/package.use/{{dep.pkg}}"
{% endfor %}

"/etc/portage/package.use/dev-python/sphinx":
  file.absent
dev-python/sphinx:
  portage_config.flags:
    - use:
      - "PYTHON_TARGETS: python3_7"
    - accept_keywords:
      - ~*
    - require:
      - file: "/etc/portage/package.use/dev-python/sphinx"
      {% for dep in sphinx_deps %}
      - file: "/etc/portage/package.use/{{dep.pkg}}"
      {% endfor %}

emerge_world:
  cmd.run:
    - name: |
        set -e
        {{world_upgrade}}
        perl-cleaner --all -- --usepkg=n --getbinpkg=n --with-bdeps=y
        if [ -f /usr/sbin/haskell-updater ]
        then
          /usr/sbin/haskell-updater || true
        fi
{% if gentoo.ssl_lib|lower == 'libressl' %}
libressl_overlay:
  module.run:
    - name: eselect.exec_action
    - module: repository
    - action: enable
    - action_parameter: libressl
    - require:
      - sls: common.Gentoo.local_overlay
  cmd.run:
    - name: emerge --sync libressl
unstable_iputils:
  portage_config.flags:
    - name: net-misc/iputils
    - accept_keywords:
      - ~*
  cmd.run:
    - name: |
        set -e -x

        if [ -n "$(find /var/db/pkg/dev-libs -type d -name "libressl-*" 2>/dev/null)" ]
        then
          exit 0
        fi

        emerge -u1 net-misc/iputils
libressl_cyrus-sasl:
  portage_config.flags:
    - name: dev-libs/cyrus-sasl
    - use:
      - -ssl
libressl:
  cmd.run:
    - name: |
        set -e -x

        if [ -n "$(find /var/db/pkg/dev-libs -type d -name "libressl-*" 2>/dev/null)" ]
        then
          exit 0
        fi

        PYTHON_SLOTS="$(cat /var/db/pkg/dev-lang/python-*/SLOT | grep -E '[0-9]+.[0-9]+' | awk -F'/' '{print $1}' | xargs -r -I'{}' printf "%s" 'dev-lang/python:{} ')"

        openssl_rollback() {
          set +e
          rm -f /etc/portage/package.mask/dev-libs/openssl
          sed -i '/USE="${USE} -openssl"/d' /etc/portage/make.conf
          sed -i '/USE="${USE} libressl"/d' /etc/portage/make.conf
          sed -i '/CURL_SSL="libressl"/d' /etc/portage/make.conf
          find /etc/portage -name libressl -delete
          emerge -q1 --usepkgonly openssl $PYTHON_SLOTS wget openssh iputils salt
          printf "%s\n" "$@"
          exit 1
        }

        if [ -n "$(find /var/db/pkg/dev-libs -type d -name "openssl-*" 2>/dev/null)" ]
        then
          mkdir -p /etc/portage/package.mask/dev-libs
          echo "dev-libs/openssl" > /etc/portage/package.mask/dev-libs/openssl
          echo 'USE="${USE} libressl"' >> /etc/portage/make.conf
          echo 'USE="${USE} -openssl"' >> /etc/portage/make.conf
          echo 'CURL_SSL="libressl"' >> /etc/portage/make.conf
          mkdir -p /etc/portage/package.accept_keywords/dev-libs
          echo '<dev-libs/libressl-2.9.0 ~*' > /etc/portage/package.accept_keywords/dev-libs/libressl
          mkdir -p /etc/portage/package.unmask/dev-libs
          echo '<dev-libs/libressl-2.9.0' > /etc/portage/package.unmask/dev-libs/libressl
          emerge -f libressl
          quickpkg --include-config y openssl python wget openssh iputils salt
          emerge --rage-clean openssl
          emerge -1q libressl || openssl_rollback "libressl failed"
          emerge -1v openssh wget $PYTHON_SLOTS iputils salt || openssl_rollback "rebuilding openssl dependents failed"
          emerge -q @preserved-rebuild || openssl_rollback "preserved rebuild failed"
          {{world_upgrade}} || openssl_rollback "world upgrade failed"
          emerge -1 net-misc/curl dev-vcs/git
          perl-cleaner --all || true
          if [ -f /usr/sbin/haskell-updater ]
          then
            /usr/sbin/haskell-updater || true
          fi
        fi
{% else %}
# disable bindist for openssl and openssh
# to prevent dependency hell
openssl_disable_bindist:
  portage_config.flags:
    - name: dev-libs/openssl
    - use:
      - -bindist
openssh_disable_bindist:
  portage_config.flags:
    - name: net-misc/openssh
    - use:
      - -bindist
{% endif %}

sys-process/parallel:
  pkg.installed
