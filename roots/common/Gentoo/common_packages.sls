# Any packages from my overlay need to be keyworded
{% set unstable_packages = [
  'app-arch/dtrx',
  'app-admin/xkcdpass'
] %}

{% for pkg in unstable_packages %}
{{pkg}}:
  portage_config.flags:
    - accept_keywords:
      - ~*
{% endfor %}

# imlib useflag is needed by neofetch
www-client/w3m:
  portage_config.flags:
    - use:
      - imlib

# prevents conflicts with libressl
# (I don't need ldap or sasl anyway)
use_app-admin/sudo:
  portage_config.flags:
    - name: app-admin/sudo
    - use:
      - -sasl
      - -ldap

# libressl is incompatible with >net-libs/libssh-0.7.5-r2
# Interestingly Void Linux doesn't have this problem so it
# could be worth looking at what version of libressl they
# use and possibly upgrading from the "stable" version
# in the Gentoo tree.
mask_net-libs/libssh:
  portage_config.flags:
    - name: "<net-libs/libssh-0.8.5"
    - mask: True
unmask_net-libs/libssh:
  portage_config.flags:
    - name: "=net-libs/libssh-0.7.5-r2"
    - accept_keywords:
      - ~*
    - unmask: True

">=net-dns/avahi-0.7-r1":
  portage_config.flags:
    - use:
      - mdnsresponder-compat

">=media-libs/mesa-18.2.8":
  portage_config.flags:
    - use:
      - wayland

"<net-libs/http-parser-2.10.0-r1":
  portage_config.flags:
    - name: "<net-libs/http-parser-2.10.0-r1"
    - accept_keywords:
      - "~*"

include:
  - applications.video.ffmpeg.ffmpeg
