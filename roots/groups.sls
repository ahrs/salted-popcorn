{% for group in [
  'audio',
  'input',
  'kvm',
  'nobody',
  'nogroup',
  'plugdev',
  'sudo',
  'tty',
  'users',
  'video',
] %}
{{group}}:
  group.present:
    - system: True
{% endfor %}
