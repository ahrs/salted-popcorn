{% from "fonts/ubuntu/map.jinja" import ubuntu_fonts with context %}
{% if ubuntu_fonts['ttf-ubuntu-font-family'] != None %}
ubuntu:
  pkg.installed:
    - pkgs:
      - {{ubuntu_fonts['ttf-ubuntu-font-family']}}
    - install_recommends: False
{% endif %}
