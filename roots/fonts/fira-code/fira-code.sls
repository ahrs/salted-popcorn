{% from "fonts/fira-code/map.jinja" import firacode_fonts with context %}
{% if firacode_fonts['ttf-fira-code'] != None %}
fira_code:
  pkg.installed:
    - pkgs:
      - {{firacode_fonts['ttf-fira-code']}}
    - install_recommends: False
{% endif %}
