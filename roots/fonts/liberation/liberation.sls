{% set liberation_fonts = salt['grains.filter_by']({
  'default': {
    'ttf-liberation':     'ttf-liberation'
  },
  'Debian': {
    'ttf-liberation':     'fonts-liberation2'
  },
  'Gentoo': {
    'ttf-liberation':     'media-fonts/liberation-fonts'
  },
  'RedHat': {
    'ttf-liberation':     'liberation-fonts'
  },
  'Suse': {
    'ttf-liberation':     'liberation-fonts'
  },
  'Void': {
    'ttf-liberation': 'liberation-fonts-ttf'
  }
}, grain='os_family', merge = salt['pillar.get']('liberation_fonts:lookup'), base='default') %}
liberation:
  pkg.installed:
    - pkgs:
      {% for pkg in liberation_fonts %}
      {% if liberation_fonts[pkg] != None %}
      - {{liberation_fonts[pkg]}}
      {% endif %}
      {% endfor %}
    - install_recommends: False
