{% from "fonts/cantarell/map.jinja" import cantarell_fonts with context %}
{% if cantarell_fonts['cantarell-fonts'] != None %}
cantarell:
  pkg.installed:
    - pkgs:
      - {{cantarell_fonts['cantarell-fonts']}}
    - install_recommends: False
{% endif %}
