{% from "fonts/hack/map.jinja" import hack_fonts with context %}
{% if hack_fonts['ttf-hack'] != None %}
hack:
  pkg.installed:
    - pkgs:
      - {{hack_fonts['ttf-hack']}}
    - install_recommends: False
{% endif %}
