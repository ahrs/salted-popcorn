{% from "fonts/noto/map.jinja" import noto_fonts with context %}
{% if 'noto-fonts-emoji' in noto_fonts and noto_fonts['noto-fonts-emoji'] != None %}
noto_emoji:
  pkg.installed:
    - pkgs:
      - {{noto_fonts['noto-fonts-emoji']}}
    - install_recommends: False
{% endif %}
