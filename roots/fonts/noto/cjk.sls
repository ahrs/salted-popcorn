{% from "fonts/noto/map.jinja" import noto_fonts with context %}
{% if noto_fonts['noto-fonts-cjk'] != None %}
noto_cjk:
  pkg.installed:
    - pkgs:
      - {{noto_fonts['noto-fonts-cjk']}}
    - install_recommends: False
{% endif %}
{% if 'noto-fonts-extra' in noto_fonts and noto_fonts['noto-fonts-extra'] != None%}
noto_extra:
  pkg.installed:
    - pkgs:
      - {{noto_fonts['noto-fonts-extra']}}
    - install_recommends: False
{% endif %}
