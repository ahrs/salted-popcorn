{% from "fonts/noto/map.jinja" import noto_fonts with context %}
{% if 'noto-fonts-mono' in noto_fonts %}
noto_mono:
  pkg.installed:
    - pkgs:
      - {{noto_fonts['noto-fonts-mono']}}
    - install_recommends: False
{% endif %}
