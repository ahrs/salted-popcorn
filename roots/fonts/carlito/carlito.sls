{% from "fonts/carlito/map.jinja" import carlito_fonts with context %}
{% if carlito_fonts['ttf-carlito'] != None %}
carlito:
  pkg.installed:
    - pkgs:
      - {{carlito_fonts['ttf-carlito']}}
    - install_recommends: False
{% endif %}
