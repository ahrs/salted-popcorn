{% from "fonts/roboto/map.jinja" import roboto_fonts with context %}
{% if roboto_fonts['ttf-roboto'] != None %}
roboto_fonts:
  pkg.installed:
    - pkgs:
      - {{roboto_fonts['ttf-roboto']}}
    - install_recommends: False
{% endif %}
{% if 'ttf-roboto-hinted' in roboto_fonts %}
roboto_fonts_unhinted:
  pkg.installed:
    - pkgs:
      - {{roboto_fonts['ttf-roboto-hinted']}}
    - install_recommends: False
{% endif %}
