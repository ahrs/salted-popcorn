{% from "fonts/caladea/map.jinja" import caladea_fonts with context %}
{% if caladea_fonts['ttf-caladea'] != None %}
caladea:
  pkg.installed:
    - pkgs:
      - {{caladea_fonts['ttf-caladea']}}
    - install_recommends: False
{% endif %}
