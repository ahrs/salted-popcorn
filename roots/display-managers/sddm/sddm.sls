{% from "display-managers/sddm/map.jinja" import display_managers_sddm with context %}
{% from "detect-laptop.jinja" import detect_laptop with context %}

{% set init = salt['grains.get']('init', default='unknown') %}

install_sddm:
  pkg.installed:
    - pkgs:
      - {{display_managers_sddm.pkg}}
enable_sddm:
  service.enabled:
    - name: {{display_managers_sddm.service}}
    - require:
      - pkg: install_sddm
    {% if init == 'systemd' %}
    - unless:
      - test -e /etc/systemd/system/display-manager.service
    {% endif %}

/etc/sddm.conf:
  file.managed:
    - makedirs: True
    - require:
      - pkg: install_sddm
  ini.options_present:
    - require:
      - pkg: install_sddm
    - sections:
        General:
          HaltCommand: {% if init == 'systemd' %}{{salt['cmd.which']('systemctl')}} {% endif %}poweroff
          RebootCommand: {% if init == 'systemd' %}{{salt['cmd.which']('systemctl')}} {% endif %}reboot
        X11:
          DisplayCommand: "/usr/share/sddm/scripts/Xsetup"
          EnableHiDPI: true
          ServerArguments: -nolisten tcp
        Wayland:
          EnableHiDPI: true
/etc/sddm.conf_x11-dpi:
  ini.options_present:
    - require:
      - pkg: install_sddm
    - name: /etc/sddm.conf
    - sections:
        X11:
          ServerArguments: -nolisten tcp -dpi 282
    - onlyif: |
        {{detect_laptop()}}

/usr/share/sddm/scripts/Xsetup:
  file.append:
    - require:
      - pkg: install_sddm
    - makedirs: True
    - text: |
        xinput list --id-only | while read -r id; do xinput --list-props $id | grep -q -F 'libinput Tapping' && echo $id; done | while read -r id; do xinput --set-prop "$id" "libinput Tapping Enabled" 1; xinput --set-prop "$id" "libinput Tapping Enabled" 1; done
