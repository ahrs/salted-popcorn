{% from "display-managers/lightdm/map.jinja" import display_managers_lightdm with context %}
install_lightdm:
  pkg.installed:
    - pkgs:
      - {{display_managers_lightdm.pkg}}
      - {{display_managers_lightdm.greeter}}
enable_lightdm:
  service.enabled:
    - name: {{display_managers_lightdm.service}}
    {% if salt['grains.get']('init', default='unknown') == 'systemd' %}
    - unless:
      - test -e /etc/systemd/system/display-manager.service
    {% endif %}
