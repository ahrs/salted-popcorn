{% from "display-managers/gdm/map.jinja" import display_managers_gdm with context %}
install_gdm:
  pkg.installed:
    - pkgs:
      - {{display_managers_gdm.pkg}}
enable_gdm:
  service.enabled:
    - name: {{display_managers_gdm.service}}
    {% if salt['grains.get']('init', default='unknown') == 'systemd' %}
    - unless:
      - test -e /etc/systemd/system/display-manager.service
    {% endif %}

gdm_tap_to_click:
  cmd.run:
    - name: sudo -u gdm dbus-launch gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
    - onlyif:
      - id gdm
      - command -v gsettings

# https://wiki.archlinux.org/index.php/Bluetooth_headset#Gnome_with_GDM
/var/lib/gdm/.config/pulse/client.conf:
  file.managed:
    - makedirs: True
    - user: gdm
    - group: gdm
    - onlyif:
      - id gdm
    - contents: |
        autospawn = no
        daemon-binary = /bin/true
