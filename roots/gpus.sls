{% set video_cards = salt['grains.get']('gpus', default={'vendor': 'intel'})|map(attribute='vendor') %}
{% for video_card in video_cards %}
{{video_card}}:
  cmd.run:
    - name: echo "{{video_card}}"
{% endfor %}
