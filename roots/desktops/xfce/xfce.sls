{% set xfce4 = salt['grains.filter_by']({
  'default': {
    'xfce4':  'xfce4',
    'xfce4-goodies':  'xfce4-goodies'
  },
  'Gentoo': {
    'xfce4':  'xfce-base/xfce4-meta',
    'xfce4-goodies':  None
  },
  'Void': {
    'xfce4-goodies': None
  }
}, grain='os_family', merge = salt['pillar.get']('xfce4:lookup'), base='default') %}


xfce:
  pkg.installed:
    - pkgs:
      {% for pkg in xfce4 %}
      {% if xfce4[pkg] != None %}
      - {{xfce4[pkg]}}
      {% endif %}
      {% endfor %}
