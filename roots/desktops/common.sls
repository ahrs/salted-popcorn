{% from "gpus.jinja" import video_cards with context %}

include:
  - dummy
  {% if grains['os_family'] == 'Arch' %}
  - applications.utilities.yay.yay
  {% elif grains['os_family'] == 'Void' %}
  {% if 'nvidia' in video_cards %}
  - common.Void.void-repo-nonfree
  {% endif %}
  {% else %}
  {% endif %}

# Pretend nvidia-drivers are installed on systems without nvidia
# hardware. This is done to stop binary packages like mpv that
# have the "nvidia" use-flag set (since they're build with cuda support)
# from pulling in the nvidia-drivers when they are not needed.
{% if grains['os_family'] == 'Gentoo' and not 'nvidia' in video_cards %}
"/etc/portage/profile/package.provided/x11-drivers":
  file.directory
"/etc/portage/profile/package.provided/x11-drivers/nvidia-drivers":
  file.managed:
    - contents: x11-drivers/nvidia-drivers-9999999999
    - require:
      - file: "/etc/portage/profile/package.provided/x11-drivers"
{% endif %}

{% set gpu_drivers = salt['grains.filter_by']({
  'default': {
    'amd': [
      'xf86-video-amdgpu'
    ],
    'intel': [
      "xf86-video-intel"
    ],
    'nvidia': [
      "xf86-video-nouveau",
      "nvidia"
     ]
  },
  'Alpine': {
    'nvidia': [
      'xf86-video-nouveau'
    ]
  },
  'Debian': {
    'intel': [
      'xserver-xorg-video-intel'
    ],
    'nvidia': [
      'xserver-xorg-video-nouveau'
    ]
  },
  'RedHat': {
    'amd': [
      'xorg-x11-drv-amdgpu'
    ],
    'intel': [
      'xorg-x11-drv-intel'
    ],
    'nvidia': [
      'xorg-x11-drv-nouveau'
    ]
  },
  'Suse': {
    'amd': [
      'xf86-video-amdgpu'
    ],
    'intel': [
      'xf86-video-intel'
    ],
    'nvidia': [
      'xf86-video-nouveau'
    ]
 }
}, merge=salt['pillar.get']('gpu_drivers:lookup'), base='default') %}

{% if grains['os'] == 'Ubuntu' %}
ubuntu-drivers-common:
  pkg.installed
ubuntu-drivers:
  cmd.run:
    - name: ubuntu-drivers autoinstall
    - require:
      - pkg: ubuntu-drivers-common
# Gentoo will pull in drivers automatically via VIDEO_CARDS
{% elif grains['os_family'] != "Gentoo" %}
{% for video_card in video_cards %}
{% if video_card in gpu_drivers and gpu_drivers[video_card] != None %}
# Nvidia might need special treatment for the _absolutely_proprietary_ driver
{% if video_card == 'nvidia' %}
{% if grains['os_family'] == 'RedHat' %}
negativo_repo:
  cmd.run:
    - name: |
        set -e -x
        command -v dnf > /dev/null 2>&1 && cmd="dnf config-manager" || cmd="yum-config-manager"
        $cmd --add-repo={% if grains['os'] == 'Fedora' %}https://negativo17.org/repos/fedora-nvidia.repo{% else %}https://negativo17.org/repos/epel-nvidia.repo{% endif %}
    - onlyif:
      - "! test -f /etc/yum.repos.d/{% if grains['os'] == 'Fedora' %}fedora-nvidia{% else %}epel-nvidia{% endif %}.repo"
    - check_cmd:
      - "test -f /etc/yum.repos.d/{% if grains['os'] == 'Fedora' %}fedora-nvidia{% else %}epel-nvidia{% endif %}.repo"
gpu_drivers_{{video_card}}:
  pkg.installed:
    - pkgs:
      {% for pkg in ['nvidia-driver', 'dkms-nvidia', 'nvidia-modprobe', 'nvidia-settings', 'nvidia-xconfig'] %}
      - {{pkg}}
      {% endfor %}
{% continue %}
{% endif %}
{% endif %}
gpu_drivers_{{video_card}}:
  pkg.installed:
    - pkgs:
      {% for pkg in gpu_drivers[video_card] %}
      {% if pkg != None %}
      - {{pkg}}
      {% endif %}
      {% endfor %}
{% endif %}
{% endfor %}
{% endif %}

{% set common_desktop_packages = salt['grains.filter_by']({
  'default': {
    'setxkbmap': 'setxkbmap',
    'xclip':  'xclip',
    'xdotool':  'xdotool',
    'xf86-input-libinput': 'xf86-input-libinput',
    'xinit': 'xinit',
    'xinput': 'xinput',
    'xkbset': 'xkbset',
    'xrandr': 'xrandr'
  },
  'Alpine': {
    'xkbset': None
  },
  'Arch': {
    'setxkbmap': 'xorg-setxkbmap',
    'xinit':  'xorg-xinit',
    'xinput': 'xorg-xinput',
    'xkbset': None,
    'xrandr': 'xorg-xrandr'
  },
  'Debian': {
    'setxkbmap':  'x11-xkb-utils',
    'x11-utils':  'x11-utils',
    'xf86-input-libinput': 'xserver-xorg-input-libinput',
    'xrandr': 'x11-xserver-utils'
  },
  'Gentoo': {
    'setxkbmap':  'x11-apps/setxkbmap',
    'xclip':  'x11-misc/xclip',
    'xdotool':  'x11-misc/xdotool',
    'xf86-input-libinput': None,
    'xinit':  'x11-apps/xinit',
    'xinput': 'x11-apps/xinput',
    'xkbset': 'x11-misc/xkbset',
    'xrandr': 'x11-apps/xrandr'
  },
  'RedHat': {
    'setxkbmap':  'xorg-x11-xkb-utils',
    'xf86-input-libinput': 'xorg-x11-drv-libinput',
    'xinit':  'xorg-x11-xinit',
    'xinput': 'xorg-x11-server-utils',
    'xrandr': 'xorg-x11-server-utils'
  },
  'Suse': {
    'xkbset': None
  },
  'Void': {
    'xkbset': None
  }
},
grain="os_family",
base='default',
merge=salt['grains.filter_by']({
  'CentOS': {
    'xkbset': None
  }
}, grain='os', merge=salt['pillar.get']('common_desktop_packages:lookup'))) %}

{% if grains['os_family'] == 'Gentoo' %}
unkeyword_x11-misc/xkbset:
  portage_config.flags:
    - name: x11-misc/xkbset
    - accept_keywords:
      - "~*"
{% endif %}

common_desktop_packages:
  pkg.installed:
    - pkgs:
      {% for pkg in common_desktop_packages %}
      {% if common_desktop_packages[pkg] != None %}
      - {{common_desktop_packages[pkg]}}
      {% endif %}
      {% endfor %}

{% if grains['os_family'] == 'Arch' %}
{% from "common/Arch/aur_install.jinja" import yay with context %}

{% for pkg in [
  'xkbset',
] %}
{{pkg}}:
{{yay([pkg])}}
{% endfor %}
{% endif %}

/etc/X11/xorg.conf.d:
  file.directory

/etc/X11/xorg.conf.d/10-keyboard.conf:
  file.managed:
    - contents: |
        Section "InputClass"
            Identifier "keyboard-all"
            Driver "libinput"
            Option "XkbLayout" "gb"
            #Option "XkbOptions" "caps:escape"
            Option "XkbOptions" "ctrl:nocaps"
            Option "XkbOptions" "keypad:pointerkeys"
            MatchIsKeyboard "on"
        EndSection
    - require:
      - file: /etc/X11/xorg.conf.d

org.gnome.desktop.input-sources_sources:
  cmd.run:
    - name: |
        # Try to affect the currently running environment (if any)
        env -i sudo -u ahrs -- env DISPLAY=:0 XDG_RUNTIME_DIR=/run/user/1000 gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'gb')]" || true
    - onlyif:
      - id ahrs
      - command -v gsettings
      - command -v dbus-launch
  module.run:
    - name: gnome.set
    - user: ahrs
    - schema: org.gnome.desktop.input-sources
    - key: sources
    - value: "[('xkb', 'gb')]"
    # Requires python2-gobject to be installed so just return true regardless
    - check_cmd:
      - exit 0

org.gnome.desktop.input-sources_xkb-options:
  cmd.run:
    - name: |
        # Try to affect the currently running environment (if any)
        env -i sudo -u ahrs -- env DISPLAY=:0 XDG_RUNTIME_DIR=/run/user/1000 gsettings set org.gnome.desktop.input-sources xkb-options "['ctrl:nocaps', 'keypad:pointerkeys']" || true
    - onlyif:
      - id ahrs
      - command -v gsettings
      - command -v dbus-launch
  module.run:
    - name: gnome.set
    - user: ahrs
    - schema: org.gnome.desktop.input-sources
    - key: xkb-options
    - value: "['ctrl:nocaps', 'keypad:pointerkeys']"
    # Requires python2-gobject to be installed so just return true regardless
    - check_cmd:
      - exit 0

org.gnome.desktop.peripherals.touchpad_tap-to-click:
  cmd.run:
    - name: |
        # Try to affect the currently running environment (if any)
        env -i sudo -u ahrs -- env DISPLAY=:0 XDG_RUNTIME_DIR=/run/user/1000 gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true || true
    - onlyif:
      - id ahrs
      - command -v gsettings
      - command -v dbus-launch
  module.run:
    - name: gnome.set
    - user: ahrs
    - schema: org.gnome.desktop.peripherals.touchpad
    - key: tap-to-click
    - value: true
    # Requires python2-gobject to be installed so just return true regardless
    - check_cmd:
      - exit 0

org.gnome.desktop.wm.preferences_button-layout:
  cmd.run:
    - name: |
        # Try to affect the currently running environment (if any)
        env -i sudo -u ahrs -- env DISPLAY=:0 XDG_RUNTIME_DIR=/run/user/1000 gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close' || true
    - onlyif:
      - id ahrs
      - command -v gsettings
      - command -v dbus-launch
  module.run:
    - name: gnome.set
    - user: ahrs
    - schema: org.gnome.desktop.wm.preferences
    - key: button-layout
    - value: 'appmenu:minimize,maximize,close'
    # Requires python2-gobject to be installed so just return true regardless
    - check_cmd:
      - exit 0
