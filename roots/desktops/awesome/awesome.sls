{% set awesome = salt['grains.filter_by']({
  'default': {
    'awesome':  'awesome',
  },
  'Gentoo': {
    'awesome':  'x11-wm/awesome'
  }
}, grain='os_family', merge = salt['pillar.get']('awesome:lookup'), base='default') %}

awesome:
  pkg.installed:
    - pkgs:
      - {{awesome['awesome']}}
