{% set lxqt = salt['grains.filter_by']({
  'default': {
    'lxqt': 'lxqt',
    'libstatgrab':  'libstatgrab',
    'libsysstat': 'libsysstat',
    'lm_sensors': 'lm_sensors'
  },
  'Debian': {
    'libstatgrab': 'statgrab',
    'libsysstat': 'libsysstat-qt5-0',
    'lm_sensors': 'lm-sensors'
  },
  'Gentoo': {
    'lxqt': 'lxqt-base/lxqt-meta',
    'libstatgrab':  'sys-libs/libstatgrab',
    'libsysstat': 'app-admin/sysstat',
    'lm_sensors': 'sys-apps/lm_sensors'
  }
}, grain='os_family', merge = salt['pillar.get']('lxqt:lookup'), base='default') %}

lxqt:
  pkg.installed:
    - pkgs:
      {% for pkg in lxqt %}
      - {{lxqt[pkg]}}
      {% endfor %}
    - install_recommends: False
