{% if grains['os_family'] != 'Gentoo' %}
gnome-flashback:
  pkg.installed:
    - pkgs:
      - gnome-flashback
      {% if grains['os_family'] == 'Arch' %}
      - gnome-applets
      - sensors-applet
      {% endif %}
{% endif %}
