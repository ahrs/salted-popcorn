{% if grains['os'] == 'Ubuntu' %}
{% set mate_desktop = 'ubuntu-mate-desktop' %}
{% else %}
{% set mate_desktop = 'mate-desktop' %}
{% endif %}

{% set mate = salt['grains.filter_by']({
  'default': {
    'mate': 'mate',
    'mate-extra': 'mate-extra',
    'brisk-menu': 'brisk-menu'
  },
  'Gentoo': {
    'mate': 'mate-base/mate',
    'mate-extra': None,
    'brisk-menu': None
  },
  'Debian': {
    'mate': mate_desktop,
    'mate-extra': None,
    'brisk-menu': 'mate-applet-brisk-menu'
  }
}, grain='os_family', merge = salt['pillar.get']('mate:lookup'), base='default') %}

mate:
  pkg.installed:
    - pkgs:
      {% for pkg in mate %}
      {% if mate[pkg] != None %}
      - {{mate[pkg]}}
      {% endif %}
      {% endfor %}
    - install_recommends: False
