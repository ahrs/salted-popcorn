{% set atom = salt['grains.filter_by']({
  'default': {
    'deepin': 'deepin',
  },
  'Arch': {
    'deepin-extra': 'deepin-extra'
  },
  'Gentoo': {
    'deepin': 'dde-base/dde-meta'
  }
}, grain='os_family', merge = salt['pillar.get']('atom:lookup'), base='default') %}


{% if grains['os_family'] == 'Gentoo' %}
deepin_add_sabayon_overlay:
  cmd.run:
    - name: |
        set -e -x
        layman -q -d deepin || true
        if [ ! -e /etc/layman/layman.cfg ]
        then
          mkdir -p /etc/layman
          echo "check_official : No" > /etc/layman/layman.cfg
        fi
        sed 's|check_official.*|check_official : No|g' /etc/layman/layman.cfg | layman --config=/dev/stdin -q -a deepin
{% endif %}

deepin:
  pkg.installed:
    - pkgs:
      {% for pkg in deepin %}
      - {{deepin[pkg]}}
      {% endfor %}
