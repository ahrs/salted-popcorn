include:
  - applications.programming.tools.git.git
  - applications.programming.tools.make.make
  - applications.programming.tools.patch.patch
  {% if grains['os_family'] != 'Arch' and grains['os_family'] != 'Gentoo' %}
  - applications.utilities.stow.stow
  {% endif %}
{% set dwm = salt['grains.filter_by']({
  'default': {
    'libXinerama': 'libXinerama',
    'libXft': 'libXft',
    'freetype': 'freetype'
  },
  'Alpine': {
    'libXinerama': 'libxinerama-dev',
    'libXft': 'libxft-dev',
    'libx11-dev': 'libx11-dev',
    'freetype': 'freetype-dev'
  },
  'Void': {
    'gcc':  'gcc',
    'libXinerama':  'libXinerama-devel',
    'libXft': 'libXft-devel',
    'freetype': 'freetype-devel'
  }
}, grain='os_family', merge = salt['pillar.get']('dwm:lookup'), base='default') %}
{% if grains['os_family'] == 'Debian' %}
dwm:
  cmd.run:
    - name: |
        set -e -x
        sed -i 's|^# deb-src|deb-src|g' /etc/apt/sources.list
        if ! grep -q 'deb-src' /etc/apt/sources.list
        then
          grep -E '^deb ' /etc/apt/sources.list | while read -r line
          do
          line="$(printf "%s" "$line" | sed 's|^deb |deb-src |g')"
          echo "$line" >> /etc/apt/sources.list
          done
        fi
        apt-get update -qq
        apt-get build-dep --yes dwm
{% elif grains['os_family'] != 'Gentoo' %}
dwm_builddeps:
  pkg.installed:
    - pkgs:
      {% for pkg in dwm %}
      - {{dwm[pkg]}}
      {% endfor %}
{% endif %}
dwm_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/dwm
dwm_clone:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/dwm
    - user: nobody
    - url: https://gitlab.com/ahrs/dwm.git
    - opts: --depth 1
{% if grains['os_family'] == 'Gentoo' %}
dwm_install:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp/dwm
        make gentoo
{% elif grains['os_family'] == 'Arch' %}
dwm_install:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp/dwm
        make pkgbuild
        pacman -U *.pkg.tar.xz
{% else %}
dwm_install:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp/dwm
        /usr/bin/env -i sudo -u nobody -- make SUDO=env
        rm -rf /stow/dwm.tmp
        install -dm0775 -o nobody -g nobody /stow/dwm.tmp
        if [ -d /stow/dwm ]
        then
          cd /stow
          stow --verbose=3 -t / -D dwm
          cd -
          mv /stow/dwm /stow/dwm.bak
        fi
        cd pkg
        install -dm755 local
        mv usr local
        cd -
        mv pkg/local/usr pkg/local/local
        mv pkg/local pkg/usr
        test -f pkg/usr/local/bin/dwm || {
          cd /stow
          if [ -d dwm.bak ]
          then
            rm -rf dwm
            rm -rf dwm.tmp
            mv dwm.bak dwm
            stow --verbose=3 -t / dwm
          fi
          exit 1
        }
        cp -r pkg/usr /stow/dwm.tmp
        cd /stow
        mv dwm.tmp dwm
        chown -R root:root dwm
        stow --verbose=3 -t / dwm
        rm -rf /stow/dwm.bak
{% endif %}
