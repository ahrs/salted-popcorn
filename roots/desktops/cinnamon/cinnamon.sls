{% set cinnamon = salt['grains.filter_by']({
  'default': {
    'cinnamon': 'cinnamon'
  },
  'Gentoo': {
    'cinnamon': 'gnome-extra/cinnamon'
  }
}, grain='os_family', merge = salt['pillar.get']('cinnamon:lookup'), base='default') %}


install_cinnamon:
  pkg.installed:
    - pkgs:
      - {{cinnamon['cinnamon']}}
