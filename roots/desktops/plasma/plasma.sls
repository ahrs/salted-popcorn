{% from "detect-laptop.jinja" import detect_laptop with context %}

include:
  - themes.breeze.breeze
  - themes.papirus.papirus
  - themes.sierrabreeze.kwin-decoration

{% if grains['os'] == 'Ubuntu' %}
{% set plasma_desktop = 'kubuntu-desktop' %}
{% else %}
{% set plasma_desktop = 'plasma-desktop' %}
{% endif %}

{% set plasma = salt['grains.filter_by']({
  'default': {
    'plasma': 'plasma',
    'kscreen': 'kscreen',
    'kde-gtk-config': 'kde-gtk-config',
    'https://wiki.archlinux.org/index.php/KDE_Wallet#Unlock_KDE_Wallet_automatically_on_login':  None,
    'kwallet-pam': 'kwallet-pam',
    'bluedevil': 'bluedevil',
    'latte-dock': 'latte-dock'
  },
  'Arch': {
    'plasma': 'plasma-desktop',
    'kscreen': None,
    'kde-gtk-config': None,
    'bluedevil': None,
  },
  'Debian': {
    'plasma': plasma_desktop,
    'kde-gtk-config': 'kde-config-gtk-style'
  },
  'Gentoo': {
    'plasma': 'kde-plasma/plasma-meta',
    'latte-dock': 'kde-misc/latte-dock',
    'bluedevil': 'kde-plasma/bluedevil',
    'kwallet-pam': 'kde-plasma/kwallet-pam',
    'kde-gtk-config': 'kde-plasma/kde-gtk-config',
    'kscreen': 'kde-plasma/kscreen'
  },
  'RedHat': {
    'plasma': 'plasma-desktop',
    'kwallet-pam': 'pam-kwallet'
  },
  'Void': {
    'plasma': 'plasma-desktop',
    'kde-gtk-config': 'kde-gtk-config5'
  }
}, grain='os_family', merge = salt['pillar.get']('plasma:lookup'), base='default') %}

plasma:
  pkg.installed:
    - pkgs:
      {% for pkg in plasma %}
      {% if plasma[pkg] != None %}
      - {{plasma[pkg]}}
      {% endif %}
      {% endfor %}
    - install_recommends: False
#    - onlyif: /bin/false
# Uncomment the above when you don't feel like compiling all of KDE on Gentoo just to change some settings
# To Do:
#   * Configuration/Settings should really be split out from the installation of KDE itself 

{% if grains['os_family'] == 'Arch' %}
extra/plasma:
  pkg.group_installed:
    - name: plasma
{% endif %}

konqueror:
  pkg.purged

pavucontrol:
  pkg.purged

/home/ahrs/.config/touchpadrc:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - sections:
        parameters:
          Tapping: True

/home/ahrs/.config/kdeglobals:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - sections:
        "ColorEffects:Disabled":
          Color: 56,56,56
          ColorAmount: 0
          ColorEffect: 0
          ContrastAmount: 0.65
          ContrastEffect: 1
          IntensityAmount: 0.1
          IntensityEffect: 2
        "ColorEffects:Inactive":
          ChangeSelectionColor: "true"
          Color: 112,111,110
          ColorAmount: 0.025
          ColorEffect: 2
          ContrastAmount: 0.1
          ContrastEffect: 2
          Enable: false
          IntensityAmount: 0
          IntensityEffect: 0
        "Colors:Button":
          BackgroundAlternate: 77,77,77
          BackgroundNormal: 49,54,59
          DecorationFocus: 61,174,233
          DecorationHover: 61,174,233
          ForegroundActive: 61,174,233
          ForegroundInactive: 189,195,199
          ForegroundLink: 41,128,185
          ForegroundNegative: 218,68,83
          ForegroundNeutral: 246,116,0
          ForegroundNormal: 239,240,241
          ForegroundPositive: 39,174,96
          ForegroundVisited: 127,140,141
        "Colors:Complementary":
          BackgroundAlternate: 59,64,69
          BackgroundNormal: 49,54,59
          DecorationFocus: 30,146,255
          DecorationHover: 61,174,230
          ForegroundActive: 246,116,0
          ForegroundInactive: 175,176,179
          ForegroundLink: 61,174,230
          ForegroundNegative: 237,21,21
          ForegroundNeutral: 201,206,59
          ForegroundNormal: 239,240,241
          ForegroundPositive: 17,209,22
          ForegroundVisited: 61,174,230
        "Colors:Selection":
          BackgroundAlternate: 29,153,243
          BackgroundNormal: 61,174,233
          DecorationFocus: 61,174,233
          DecorationHover: 61,174,233
          ForegroundActive: 252,252,252
          ForegroundInactive: 239,240,241
          ForegroundLink: 253,188,75
          ForegroundNegative: 218,68,83
          ForegroundNeutral: 246,116,0
          ForegroundNormal: 239,240,241
          ForegroundPositive: 39,174,96
          ForegroundVisited: 189,195,199
        "Colors:Tooltip":
          BackgroundAlternate: 77,77,77
          BackgroundNormal: 49,54,59
          DecorationFocus: 61,174,233
          DecorationHover: 61,174,233
          ForegroundActive: 61,174,233
          ForegroundInactive: 189,195,199
          ForegroundLink: 41,128,185
          ForegroundNegative: 218,68,83
          ForegroundNeutral: 246,116,0
          ForegroundNormal: 239,240,241
          ForegroundPositive: 39,174,96
          ForegroundVisited: 127,140,141
        "Colors:View":
          BackgroundAlternate: 49,54,59
          BackgroundNormal: 35,38,41
          DecorationFocus: 61,174,233
          DecorationHover: 61,174,233
          ForegroundActive: 61,174,233
          ForegroundInactive: 189,195,199
          ForegroundLink: 41,128,185
          ForegroundNegative: 218,68,83
          ForegroundNeutral: 246,116,0
          ForegroundNormal: 239,240,241
          ForegroundPositive: 39,174,96
          ForegroundVisited: 127,140,141
        "Colors:Window":
          BackgroundAlternate: 77,77,77
          BackgroundNormal: 49,54,59
          DecorationFocus: 61,174,233
          DecorationHover: 61,174,233
          ForegroundActive: 61,174,233
          ForegroundInactive: 189,195,199
          ForegroundLink: 41,128,185
          ForegroundNegative: 218,68,83
          ForegroundNeutral: 246,116,0
          ForegroundNormal: 239,240,241
          ForegroundPositive: 39,174,96
          ForegroundVisited: 127,140,141
        General:
          ColorScheme: Breeze Dark
          Name: Breeze Dark
          TerminalApplication[$e]: terminal
          XftAntialias: "true"
          XftHintStyle: hintslight
          XftSubPixel: rgb
          font: Roboto,8,-1,5,50,0,0,0,0,0,Regular
          menuFont: Roboto,8,-1,5,50,0,0,0,0,0,Regular
          smallestReadableFont: Roboto,8,-1,5,50,0,0,0,0,0,Regular
          toolBarFont: Roboto,8,-1,5,50,0,0,0,0,0,Regular
        Icons:
          Theme: Papirus-Dark
          KDE:
            LookAndFeelPackage: org.kde.breezedark.desktop
            contrast: 4
            widgetStyle: Breeze
        KScreen:
          ScaleFactor: 1
        WM:
          activeBackground: 49,54,59
          activeBlend: 255,255,255
          activeFont: Roboto,6,-1,5,87,0,0,0,0,0,Black
          activeForeground: 239,240,241
          inactiveBackground: 49,54,59
          inactiveBlend: 75,71,67
          inactiveForeground: 127,140,141

/home/ahrs/.config/kxkbrc:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - sections:
        Layout:
          LayoutList: gb
          Options: "caps:ctrl_modifier"
          ResetOldOptions: true

/home/ahrs/.config/kwinrc:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - sections:
        Desktops:
          Number: 10
          Rows: 2
        TabBox:
          DesktopLayout: org.kde.breeze.desktop
          DesktopListLayout: org.kde.breeze.desktop
          LayoutName: org.kde.breeze.desktop
        org.kde.kdecoration2:
          BorderSize: Normal
          ButtonsOnLeft: XIA
          ButtonsOnRight: SM
          CloseOnDoubleClickOnMenu: "false"
          library: org.kde.sierrabreeze
        MouseBindings:
          CommandAllKey: Meta

# This isn't very DRY but it does the job
# Shoutout to Elementary OS developer Cassidy James's Dippy app
# https://github.com/cassidyjames/dippi
/home/ahrs/.config/kcmfonts_laptop:
  file.managed:
    - name: /home/ahrs/.config/kcmfonts
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - name: /home/ahrs/.config/kcmfonts
    - sections:
        General:
          forceFontDPI: 282
          forceFontDPIWayland: 282
    - onlyif: |
        {{detect_laptop()}}

/home/ahrs/.config/kcmfonts_desktop:
  file.managed:
    - name: /home/ahrs/.config/kcmfonts
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - name: /home/ahrs/.config/kcmfonts
    - sections:
        General:
          forceFontDPI: 111
          forceFontDPIWayland: 111
    - unless: |
        {{detect_laptop()}}

/home/ahrs/.config/khotkeysrc:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
    - contents: |
        [Data]
        DataCount=6
        
        [Data_1]
        Comment=KMenuEdit Global Shortcuts
        DataCount=2
        Enabled=true
        Name=KMenuEdit
        SystemGroup=1
        Type=ACTION_DATA_GROUP
        
        [Data_11]
        Comment[$d]
        Enabled[$d]
        Name[$d]
        Type[$d]
        
        [Data_11Actions]
        ActionsCount[$d]
        
        [Data_11Actions0]
        CommandURL[$d]
        Type[$d]
        
        [Data_11Conditions]
        Comment[$d]
        ConditionsCount[$d]
        
        [Data_11Triggers]
        Comment[$d]
        TriggersCount[$d]
        
        [Data_11Triggers0]
        Key[$d]
        Type[$d]
        Uuid[$d]
        
        [Data_12]
        Comment[$d]
        Enabled[$d]
        Name[$d]
        Type[$d]
        
        [Data_12Actions]
        ActionsCount[$d]
        
        [Data_12Actions0]
        CommandURL[$d]
        Type[$d]
        
        [Data_12Conditions]
        Comment[$d]
        ConditionsCount[$d]
        
        [Data_12Triggers]
        Comment[$d]
        TriggersCount[$d]
        
        [Data_12Triggers0]
        Key[$d]
        Type[$d]
        Uuid[$d]
        
        [Data_1Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_1_1]
        Comment=Comment
        Enabled=true
        Name=Search
        Type=SIMPLE_ACTION_DATA
        
        [Data_1_1Actions]
        ActionsCount=1
        
        [Data_1_1Actions0]
        CommandURL=http://duckduckgo.com
        Type=COMMAND_URL
        
        [Data_1_1Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_1_1Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_1_1Triggers0]
        Key=
        Type=SHORTCUT
        Uuid={d03619b6-9b3c-48cc-9d9c-a2aadb485550}
        
        [Data_1_2]
        Comment=Global keyboard shortcut to launch Konsole
        Enabled=true
        Name=Launch Konsole
        Type=MENUENTRY_SHORTCUT_ACTION_DATA
        
        [Data_1_2Actions]
        ActionsCount=1
        
        [Data_1_2Actions0]
        CommandURL=org.kde.konsole.desktop
        Type=MENUENTRY
        
        [Data_1_2Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_1_2Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_1_2Triggers0]
        Key=Ctrl+Alt+T
        Type=SHORTCUT
        Uuid={3721f516-16f2-4bef-a39e-0e7c4ea499e4}
        
        [Data_2]
        Comment=Basic Konqueror gestures.
        DataCount=14
        Enabled=true
        ImportId=konqueror_gestures_kde321
        Name=Konqueror Gestures
        SystemGroup=0
        Type=ACTION_DATA_GROUP
        
        [Data_2Conditions]
        Comment=Konqueror window
        ConditionsCount=1
        
        [Data_2Conditions0]
        Type=ACTIVE_WINDOW
        
        [Data_2Conditions0Window]
        Comment=Konqueror
        WindowsCount=1
        
        [Data_2Conditions0Window0]
        Class=^konqueror\s
        ClassType=3
        Comment=Konqueror
        Role=konqueror-mainwindow#1
        RoleType=0
        Title=file:/ - Konqueror
        TitleType=0
        Type=SIMPLE
        WindowTypes=1
        
        [Data_2_1]
        Comment=Press, move left, release.
        Enabled=true
        Name=Back
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_10]
        Comment=Opera-style: Press, move up, release.\nNOTE: Conflicts with 'New Tab', and as such is disabled by default.
        Enabled=false
        Name=Stop Loading
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_10Actions]
        ActionsCount=1
        
        [Data_2_10Actions0]
        DestinationWindow=2
        Input=Escape\n
        Type=KEYBOARD_INPUT
        
        [Data_2_10Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_10Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_10Triggers0]
        GesturePointData=0,0.125,-0.5,0.5,1,0.125,0.125,-0.5,0.5,0.875,0.25,0.125,-0.5,0.5,0.75,0.375,0.125,-0.5,0.5,0.625,0.5,0.125,-0.5,0.5,0.5,0.625,0.125,-0.5,0.5,0.375,0.75,0.125,-0.5,0.5,0.25,0.875,0.125,-0.5,0.5,0.125,1,0,0,0.5,0
        Type=GESTURE
        
        [Data_2_11]
        Comment=Going up in URL/directory structure.\nMozilla-style: Press, move up, move left, move up, release.
        Enabled=true
        Name=Up
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_11Actions]
        ActionsCount=1
        
        [Data_2_11Actions0]
        DestinationWindow=2
        Input=Alt+Up
        Type=KEYBOARD_INPUT
        
        [Data_2_11Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_11Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_11Triggers0]
        GesturePointData=0,0.0625,-0.5,1,1,0.0625,0.0625,-0.5,1,0.875,0.125,0.0625,-0.5,1,0.75,0.1875,0.0625,-0.5,1,0.625,0.25,0.0625,1,1,0.5,0.3125,0.0625,1,0.875,0.5,0.375,0.0625,1,0.75,0.5,0.4375,0.0625,1,0.625,0.5,0.5,0.0625,1,0.5,0.5,0.5625,0.0625,1,0.375,0.5,0.625,0.0625,1,0.25,0.5,0.6875,0.0625,1,0.125,0.5,0.75,0.0625,-0.5,0,0.5,0.8125,0.0625,-0.5,0,0.375,0.875,0.0625,-0.5,0,0.25,0.9375,0.0625,-0.5,0,0.125,1,0,0,0,0
        Type=GESTURE
        
        [Data_2_12]
        Comment=Going up in URL/directory structure.\nOpera-style: Press, move up, move left, move up, release.\nNOTE: Conflicts with  "Activate Previous Tab", and as such is disabled by default.
        Enabled=false
        Name=Up #2
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_12Actions]
        ActionsCount=1
        
        [Data_2_12Actions0]
        DestinationWindow=2
        Input=Alt+Up\n
        Type=KEYBOARD_INPUT
        
        [Data_2_12Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_12Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_12Triggers0]
        GesturePointData=0,0.0625,-0.5,1,1,0.0625,0.0625,-0.5,1,0.875,0.125,0.0625,-0.5,1,0.75,0.1875,0.0625,-0.5,1,0.625,0.25,0.0625,-0.5,1,0.5,0.3125,0.0625,-0.5,1,0.375,0.375,0.0625,-0.5,1,0.25,0.4375,0.0625,-0.5,1,0.125,0.5,0.0625,1,1,0,0.5625,0.0625,1,0.875,0,0.625,0.0625,1,0.75,0,0.6875,0.0625,1,0.625,0,0.75,0.0625,1,0.5,0,0.8125,0.0625,1,0.375,0,0.875,0.0625,1,0.25,0,0.9375,0.0625,1,0.125,0,1,0,0,0,0
        Type=GESTURE
        
        [Data_2_13]
        Comment=Press, move up, move right, release.
        Enabled=true
        Name=Activate Next Tab
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_13Actions]
        ActionsCount=1
        
        [Data_2_13Actions0]
        DestinationWindow=2
        Input=Ctrl+.\n
        Type=KEYBOARD_INPUT
        
        [Data_2_13Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_13Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_13Triggers0]
        GesturePointData=0,0.0625,-0.5,0,1,0.0625,0.0625,-0.5,0,0.875,0.125,0.0625,-0.5,0,0.75,0.1875,0.0625,-0.5,0,0.625,0.25,0.0625,-0.5,0,0.5,0.3125,0.0625,-0.5,0,0.375,0.375,0.0625,-0.5,0,0.25,0.4375,0.0625,-0.5,0,0.125,0.5,0.0625,0,0,0,0.5625,0.0625,0,0.125,0,0.625,0.0625,0,0.25,0,0.6875,0.0625,0,0.375,0,0.75,0.0625,0,0.5,0,0.8125,0.0625,0,0.625,0,0.875,0.0625,0,0.75,0,0.9375,0.0625,0,0.875,0,1,0,0,1,0
        Type=GESTURE
        
        [Data_2_14]
        Comment=Press, move up, move left, release.
        Enabled=true
        Name=Activate Previous Tab
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_14Actions]
        ActionsCount=1
        
        [Data_2_14Actions0]
        DestinationWindow=2
        Input=Ctrl+,
        Type=KEYBOARD_INPUT
        
        [Data_2_14Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_14Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_14Triggers0]
        GesturePointData=0,0.0625,-0.5,1,1,0.0625,0.0625,-0.5,1,0.875,0.125,0.0625,-0.5,1,0.75,0.1875,0.0625,-0.5,1,0.625,0.25,0.0625,-0.5,1,0.5,0.3125,0.0625,-0.5,1,0.375,0.375,0.0625,-0.5,1,0.25,0.4375,0.0625,-0.5,1,0.125,0.5,0.0625,1,1,0,0.5625,0.0625,1,0.875,0,0.625,0.0625,1,0.75,0,0.6875,0.0625,1,0.625,0,0.75,0.0625,1,0.5,0,0.8125,0.0625,1,0.375,0,0.875,0.0625,1,0.25,0,0.9375,0.0625,1,0.125,0,1,0,0,0,0
        Type=GESTURE
        
        [Data_2_1Actions]
        ActionsCount=1
        
        [Data_2_1Actions0]
        DestinationWindow=2
        Input=Alt+Left
        Type=KEYBOARD_INPUT
        
        [Data_2_1Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_1Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_1Triggers0]
        GesturePointData=0,0.125,1,1,0.5,0.125,0.125,1,0.875,0.5,0.25,0.125,1,0.75,0.5,0.375,0.125,1,0.625,0.5,0.5,0.125,1,0.5,0.5,0.625,0.125,1,0.375,0.5,0.75,0.125,1,0.25,0.5,0.875,0.125,1,0.125,0.5,1,0,0,0,0.5
        Type=GESTURE
        
        [Data_2_2]
        Comment=Press, move down, move up, move down, release.
        Enabled=true
        Name=Duplicate Tab
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_2Actions]
        ActionsCount=1
        
        [Data_2_2Actions0]
        DestinationWindow=2
        Input=Ctrl+Shift+D\n
        Type=KEYBOARD_INPUT
        
        [Data_2_2Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_2Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_2Triggers0]
        GesturePointData=0,0.0416667,0.5,0.5,0,0.0416667,0.0416667,0.5,0.5,0.125,0.0833333,0.0416667,0.5,0.5,0.25,0.125,0.0416667,0.5,0.5,0.375,0.166667,0.0416667,0.5,0.5,0.5,0.208333,0.0416667,0.5,0.5,0.625,0.25,0.0416667,0.5,0.5,0.75,0.291667,0.0416667,0.5,0.5,0.875,0.333333,0.0416667,-0.5,0.5,1,0.375,0.0416667,-0.5,0.5,0.875,0.416667,0.0416667,-0.5,0.5,0.75,0.458333,0.0416667,-0.5,0.5,0.625,0.5,0.0416667,-0.5,0.5,0.5,0.541667,0.0416667,-0.5,0.5,0.375,0.583333,0.0416667,-0.5,0.5,0.25,0.625,0.0416667,-0.5,0.5,0.125,0.666667,0.0416667,0.5,0.5,0,0.708333,0.0416667,0.5,0.5,0.125,0.75,0.0416667,0.5,0.5,0.25,0.791667,0.0416667,0.5,0.5,0.375,0.833333,0.0416667,0.5,0.5,0.5,0.875,0.0416667,0.5,0.5,0.625,0.916667,0.0416667,0.5,0.5,0.75,0.958333,0.0416667,0.5,0.5,0.875,1,0,0,0.5,1
        Type=GESTURE
        
        [Data_2_3]
        Comment=Press, move down, move up, release.
        Enabled=true
        Name=Duplicate Window
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_3Actions]
        ActionsCount=1
        
        [Data_2_3Actions0]
        DestinationWindow=2
        Input=Ctrl+D\n
        Type=KEYBOARD_INPUT
        
        [Data_2_3Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_3Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_3Triggers0]
        GesturePointData=0,0.0625,0.5,0.5,0,0.0625,0.0625,0.5,0.5,0.125,0.125,0.0625,0.5,0.5,0.25,0.1875,0.0625,0.5,0.5,0.375,0.25,0.0625,0.5,0.5,0.5,0.3125,0.0625,0.5,0.5,0.625,0.375,0.0625,0.5,0.5,0.75,0.4375,0.0625,0.5,0.5,0.875,0.5,0.0625,-0.5,0.5,1,0.5625,0.0625,-0.5,0.5,0.875,0.625,0.0625,-0.5,0.5,0.75,0.6875,0.0625,-0.5,0.5,0.625,0.75,0.0625,-0.5,0.5,0.5,0.8125,0.0625,-0.5,0.5,0.375,0.875,0.0625,-0.5,0.5,0.25,0.9375,0.0625,-0.5,0.5,0.125,1,0,0,0.5,0
        Type=GESTURE
        
        [Data_2_4]
        Comment=Press, move right, release.
        Enabled=true
        Name=Forward
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_4Actions]
        ActionsCount=1
        
        [Data_2_4Actions0]
        DestinationWindow=2
        Input=Alt+Right
        Type=KEYBOARD_INPUT
        
        [Data_2_4Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_4Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_4Triggers0]
        GesturePointData=0,0.125,0,0,0.5,0.125,0.125,0,0.125,0.5,0.25,0.125,0,0.25,0.5,0.375,0.125,0,0.375,0.5,0.5,0.125,0,0.5,0.5,0.625,0.125,0,0.625,0.5,0.75,0.125,0,0.75,0.5,0.875,0.125,0,0.875,0.5,1,0,0,1,0.5
        Type=GESTURE
        
        [Data_2_5]
        Comment=Press, move down, move half up, move right, move down, release.\n(Drawing a lowercase 'h'.)
        Enabled=true
        Name=Home
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_5Actions]
        ActionsCount=1
        
        [Data_2_5Actions0]
        DestinationWindow=2
        Input=Alt+Home\n
        Type=KEYBOARD_INPUT
        
        [Data_2_5Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_5Triggers]
        Comment=Gesture_triggers
        TriggersCount=2
        
        [Data_2_5Triggers0]
        GesturePointData=0,0.0461748,0.5,0,0,0.0461748,0.0461748,0.5,0,0.125,0.0923495,0.0461748,0.5,0,0.25,0.138524,0.0461748,0.5,0,0.375,0.184699,0.0461748,0.5,0,0.5,0.230874,0.0461748,0.5,0,0.625,0.277049,0.0461748,0.5,0,0.75,0.323223,0.0461748,0.5,0,0.875,0.369398,0.065301,-0.25,0,1,0.434699,0.065301,-0.25,0.125,0.875,0.5,0.065301,-0.25,0.25,0.75,0.565301,0.065301,-0.25,0.375,0.625,0.630602,0.0461748,0,0.5,0.5,0.676777,0.0461748,0,0.625,0.5,0.722951,0.0461748,0,0.75,0.5,0.769126,0.0461748,0,0.875,0.5,0.815301,0.0461748,0.5,1,0.5,0.861476,0.0461748,0.5,1,0.625,0.90765,0.0461748,0.5,1,0.75,0.953825,0.0461748,0.5,1,0.875,1,0,0,1,1
        Type=GESTURE
        
        [Data_2_5Triggers1]
        GesturePointData=0,0.0416667,0.5,0,0,0.0416667,0.0416667,0.5,0,0.125,0.0833333,0.0416667,0.5,0,0.25,0.125,0.0416667,0.5,0,0.375,0.166667,0.0416667,0.5,0,0.5,0.208333,0.0416667,0.5,0,0.625,0.25,0.0416667,0.5,0,0.75,0.291667,0.0416667,0.5,0,0.875,0.333333,0.0416667,-0.5,0,1,0.375,0.0416667,-0.5,0,0.875,0.416667,0.0416667,-0.5,0,0.75,0.458333,0.0416667,-0.5,0,0.625,0.5,0.0416667,0,0,0.5,0.541667,0.0416667,0,0.125,0.5,0.583333,0.0416667,0,0.25,0.5,0.625,0.0416667,0,0.375,0.5,0.666667,0.0416667,0,0.5,0.5,0.708333,0.0416667,0,0.625,0.5,0.75,0.0416667,0,0.75,0.5,0.791667,0.0416667,0,0.875,0.5,0.833333,0.0416667,0.5,1,0.5,0.875,0.0416667,0.5,1,0.625,0.916667,0.0416667,0.5,1,0.75,0.958333,0.0416667,0.5,1,0.875,1,0,0,1,1
        Type=GESTURE
        
        [Data_2_6]
        Comment=Press, move right, move down, move right, release.\nMozilla-style: Press, move down, move right, release.
        Enabled=true
        Name=Close Tab
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_6Actions]
        ActionsCount=1
        
        [Data_2_6Actions0]
        DestinationWindow=2
        Input=Ctrl+W\n
        Type=KEYBOARD_INPUT
        
        [Data_2_6Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_6Triggers]
        Comment=Gesture_triggers
        TriggersCount=2
        
        [Data_2_6Triggers0]
        GesturePointData=0,0.0625,0,0,0,0.0625,0.0625,0,0.125,0,0.125,0.0625,0,0.25,0,0.1875,0.0625,0,0.375,0,0.25,0.0625,0.5,0.5,0,0.3125,0.0625,0.5,0.5,0.125,0.375,0.0625,0.5,0.5,0.25,0.4375,0.0625,0.5,0.5,0.375,0.5,0.0625,0.5,0.5,0.5,0.5625,0.0625,0.5,0.5,0.625,0.625,0.0625,0.5,0.5,0.75,0.6875,0.0625,0.5,0.5,0.875,0.75,0.0625,0,0.5,1,0.8125,0.0625,0,0.625,1,0.875,0.0625,0,0.75,1,0.9375,0.0625,0,0.875,1,1,0,0,1,1
        Type=GESTURE
        
        [Data_2_6Triggers1]
        GesturePointData=0,0.0625,0.5,0,0,0.0625,0.0625,0.5,0,0.125,0.125,0.0625,0.5,0,0.25,0.1875,0.0625,0.5,0,0.375,0.25,0.0625,0.5,0,0.5,0.3125,0.0625,0.5,0,0.625,0.375,0.0625,0.5,0,0.75,0.4375,0.0625,0.5,0,0.875,0.5,0.0625,0,0,1,0.5625,0.0625,0,0.125,1,0.625,0.0625,0,0.25,1,0.6875,0.0625,0,0.375,1,0.75,0.0625,0,0.5,1,0.8125,0.0625,0,0.625,1,0.875,0.0625,0,0.75,1,0.9375,0.0625,0,0.875,1,1,0,0,1,1
        Type=GESTURE
        
        [Data_2_7]
        Comment=Press, move up, release.\nConflicts with Opera-style 'Up #2', which is disabled by default.
        Enabled=true
        Name=New Tab
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_7Actions]
        ActionsCount=1
        
        [Data_2_7Actions0]
        DestinationWindow=2
        Input=Ctrl+Shift+N
        Type=KEYBOARD_INPUT
        
        [Data_2_7Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_7Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_7Triggers0]
        GesturePointData=0,0.125,-0.5,0.5,1,0.125,0.125,-0.5,0.5,0.875,0.25,0.125,-0.5,0.5,0.75,0.375,0.125,-0.5,0.5,0.625,0.5,0.125,-0.5,0.5,0.5,0.625,0.125,-0.5,0.5,0.375,0.75,0.125,-0.5,0.5,0.25,0.875,0.125,-0.5,0.5,0.125,1,0,0,0.5,0
        Type=GESTURE
        
        [Data_2_8]
        Comment=Press, move down, release.
        Enabled=true
        Name=New Window
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_8Actions]
        ActionsCount=1
        
        [Data_2_8Actions0]
        DestinationWindow=2
        Input=Ctrl+N\n
        Type=KEYBOARD_INPUT
        
        [Data_2_8Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_8Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_8Triggers0]
        GesturePointData=0,0.125,0.5,0.5,0,0.125,0.125,0.5,0.5,0.125,0.25,0.125,0.5,0.5,0.25,0.375,0.125,0.5,0.5,0.375,0.5,0.125,0.5,0.5,0.5,0.625,0.125,0.5,0.5,0.625,0.75,0.125,0.5,0.5,0.75,0.875,0.125,0.5,0.5,0.875,1,0,0,0.5,1
        Type=GESTURE
        
        [Data_2_9]
        Comment=Press, move up, move down, release.
        Enabled=true
        Name=Reload
        Type=SIMPLE_ACTION_DATA
        
        [Data_2_9Actions]
        ActionsCount=1
        
        [Data_2_9Actions0]
        DestinationWindow=2
        Input=F5
        Type=KEYBOARD_INPUT
        
        [Data_2_9Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_2_9Triggers]
        Comment=Gesture_triggers
        TriggersCount=1
        
        [Data_2_9Triggers0]
        GesturePointData=0,0.0625,-0.5,0.5,1,0.0625,0.0625,-0.5,0.5,0.875,0.125,0.0625,-0.5,0.5,0.75,0.1875,0.0625,-0.5,0.5,0.625,0.25,0.0625,-0.5,0.5,0.5,0.3125,0.0625,-0.5,0.5,0.375,0.375,0.0625,-0.5,0.5,0.25,0.4375,0.0625,-0.5,0.5,0.125,0.5,0.0625,0.5,0.5,0,0.5625,0.0625,0.5,0.5,0.125,0.625,0.0625,0.5,0.5,0.25,0.6875,0.0625,0.5,0.5,0.375,0.75,0.0625,0.5,0.5,0.5,0.8125,0.0625,0.5,0.5,0.625,0.875,0.0625,0.5,0.5,0.75,0.9375,0.0625,0.5,0.5,0.875,1,0,0,0.5,1
        Type=GESTURE
        
        [Data_3]
        Comment=This group contains various examples demonstrating most of the features of KHotkeys. (Note that this group and all its actions are disabled by default.)
        DataCount=8
        Enabled=false
        ImportId=kde32b1
        Name=Examples
        SystemGroup=0
        Type=ACTION_DATA_GROUP
        
        [Data_3Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_1]
        Comment=After pressing Ctrl+Alt+I, the KSIRC window will be activated, if it exists. Simple.
        Enabled=false
        Name=Activate KSIRC Window
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_1Actions]
        ActionsCount=1
        
        [Data_3_1Actions0]
        Type=ACTIVATE_WINDOW
        
        [Data_3_1Actions0Window]
        Comment=KSIRC window
        WindowsCount=1
        
        [Data_3_1Actions0Window0]
        Class=ksirc
        ClassType=1
        Comment=KSIRC
        Role=
        RoleType=0
        Title=
        TitleType=0
        Type=SIMPLE
        WindowTypes=33
        
        [Data_3_1Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_1Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_3_1Triggers0]
        Key=Ctrl+Alt+I
        Type=SHORTCUT
        Uuid={334918cf-56c6-4bee-8287-d0f4a6c2ec8b}
        
        [Data_3_2]
        Comment=After pressing Alt+Ctrl+H the input of 'Hello' will be simulated, as if you typed it.  This is especially useful if you have call to frequently type a word (for instance, 'unsigned').  Every keypress in the input is separated by a colon ':'. Note that the keypresses literally mean keypresses, so you have to write what you would press on the keyboard. In the table below, the left column shows the input and the right column shows what to type.\n\n"enter" (i.e. new line)                Enter or Return\na (i.e. small a)                          A\nA (i.e. capital a)                       Shift+A\n: (colon)                                  Shift+;\n' '  (space)                              Space
        Enabled=false
        Name=Type 'Hello'
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_2Actions]
        ActionsCount=1
        
        [Data_3_2Actions0]
        DestinationWindow=2
        Input=Shift+H:E:L:L:O\n
        Type=KEYBOARD_INPUT
        
        [Data_3_2Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_2Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_3_2Triggers0]
        Key=Ctrl+Alt+H
        Type=SHORTCUT
        Uuid={57b706f8-4f3a-4241-938b-eda21f1aab9f}
        
        [Data_3_3]
        Comment=This action runs Konsole, after pressing Ctrl+Alt+T.
        Enabled=false
        Name=Run Konsole
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_3Actions]
        ActionsCount=1
        
        [Data_3_3Actions0]
        CommandURL=konsole
        Type=COMMAND_URL
        
        [Data_3_3Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_3Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_3_3Triggers0]
        Key=Ctrl+Alt+T
        Type=SHORTCUT
        Uuid={18587b4b-a967-49ac-b875-3b0a5a325161}
        
        [Data_3_4]
        Comment=Read the comment on the "Type 'Hello'" action first.\n\nQt Designer uses Ctrl+F4 for closing windows.  In KDE, however, Ctrl+F4 is the shortcut for going to virtual desktop 4, so this shortcut does not work in Qt Designer.  Further, Qt Designer does not use KDE's standard Ctrl+W for closing the window.\n\nThis problem can be solved by remapping Ctrl+W to Ctrl+F4 when the active window is Qt Designer. When Qt Designer is active, every time Ctrl+W is pressed, Ctrl+F4 will be sent to Qt Designer instead. In other applications, the effect of Ctrl+W is unchanged.\n\nWe now need to specify three things: A new shortcut trigger on 'Ctrl+W', a new keyboard input action sending Ctrl+F4, and a new condition that the active window is Qt Designer.\nQt Designer seems to always have title 'Qt Designer by Trolltech', so the condition will check for the active window having that title.
        Enabled=false
        Name=Remap Ctrl+W to Ctrl+F4 in Qt Designer
        Type=GENERIC_ACTION_DATA
        
        [Data_3_4Actions]
        ActionsCount=1
        
        [Data_3_4Actions0]
        DestinationWindow=2
        Input=Ctrl+F4
        Type=KEYBOARD_INPUT
        
        [Data_3_4Conditions]
        Comment=
        ConditionsCount=1
        
        [Data_3_4Conditions0]
        Type=ACTIVE_WINDOW
        
        [Data_3_4Conditions0Window]
        Comment=Qt Designer
        WindowsCount=1
        
        [Data_3_4Conditions0Window0]
        Class=
        ClassType=0
        Comment=
        Role=
        RoleType=0
        Title=Qt Designer by Trolltech
        TitleType=2
        Type=SIMPLE
        WindowTypes=33
        
        [Data_3_4Triggers]
        Comment=
        TriggersCount=1
        
        [Data_3_4Triggers0]
        Key=Ctrl+W
        Type=SHORTCUT
        Uuid={47b63e95-1e2b-4db3-8f9c-e04e88a69be1}
        
        [Data_3_5]
        Comment=By pressing Alt+Ctrl+W a D-Bus call will be performed that will show the minicli. You can use any kind of D-Bus call, just like using the command line 'qdbus' tool.
        Enabled=false
        Name=Perform D-Bus call 'qdbus org.kde.krunner /App display'
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_5Actions]
        ActionsCount=1
        
        [Data_3_5Actions0]
        Arguments=
        Call=popupExecuteCommand
        RemoteApp=org.kde.krunner
        RemoteObj=/App
        Type=DBUS
        
        [Data_3_5Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_5Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_3_5Triggers0]
        Key=Ctrl+Alt+W
        Type=SHORTCUT
        Uuid={6dfd4ab1-b66f-42e2-bcf4-6805519eafd6}
        
        [Data_3_6]
        Comment=Read the comment on the "Type 'Hello'" action first.\n\nJust like the "Type 'Hello'" action, this one simulates keyboard input, specifically, after pressing Ctrl+Alt+B, it sends B to XMMS (B in XMMS jumps to the next song). The 'Send to specific window' tickbox is ticked and a window with its class containing 'XMMS_Player' is specified; this will make the input always be sent to this window. This way, you can control XMMS even if, for instance, it is on a different virtual desktop.\n\n(Run 'xprop' and click on the XMMS window and search for WM_CLASS to see 'XMMS_Player').
        Enabled=false
        Name=Next in XMMS
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_6Actions]
        ActionsCount=1
        
        [Data_3_6Actions0]
        DestinationWindow=1
        Input=B
        Type=KEYBOARD_INPUT
        
        [Data_3_6Actions0DestinationWindow]
        Comment=XMMS window
        WindowsCount=1
        
        [Data_3_6Actions0DestinationWindow0]
        Class=XMMS_Player
        ClassType=1
        Comment=XMMS Player window
        Role=
        RoleType=0
        Title=
        TitleType=0
        Type=SIMPLE
        WindowTypes=33
        
        [Data_3_6Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_6Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_3_6Triggers0]
        Key=Ctrl+Alt+B
        Type=SHORTCUT
        Uuid={409dae74-1c33-4b80-896f-cb6fb23aa6bb}
        
        [Data_3_7]
        Comment=Konqueror in KDE3.1 has tabs, and now you can also have gestures.\n\nJust press the middle mouse button and start drawing one of the gestures, and after you are finished, release the mouse button. If you only need to paste the selection, it still works, just click the middle mouse button. (You can change the mouse button to use in the global settings).\n\nRight now, there are the following gestures available:\nmove right and back left - Forward (Alt+Right)\nmove left and back right - Back (Alt+Left)\nmove up and back down  - Up (Alt+Up)\ncircle anticlockwise - Reload (F5)\n\nThe gesture shapes can be entered by performing them in the configuration dialogue. You can also look at your numeric pad to help you: gestures are recognised like a 3x3 grid of fields, numbered 1 to 9.\n\nNote that you must perform exactly the gesture to trigger the action. Because of this, it is possible to enter more gestures for the action. You should try to avoid complicated gestures where you change the direction of mouse movement more than once.  For instance, 45654 or 74123 are simple to perform, but 1236987 may be already quite difficult.\n\nThe conditions for all gestures are defined in this group. All these gestures are active only if the active window is Konqueror (class contains 'konqueror').
        DataCount=4
        Enabled=false
        Name=Konqi Gestures
        SystemGroup=0
        Type=ACTION_DATA_GROUP
        
        [Data_3_7Conditions]
        Comment=Konqueror window
        ConditionsCount=1
        
        [Data_3_7Conditions0]
        Type=ACTIVE_WINDOW
        
        [Data_3_7Conditions0Window]
        Comment=Konqueror
        WindowsCount=1
        
        [Data_3_7Conditions0Window0]
        Class=konqueror
        ClassType=1
        Comment=Konqueror
        Role=
        RoleType=0
        Title=
        TitleType=0
        Type=SIMPLE
        WindowTypes=33
        
        [Data_3_7_1]
        Comment=
        Enabled=false
        Name=Back
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_7_1Actions]
        ActionsCount=1
        
        [Data_3_7_1Actions0]
        DestinationWindow=2
        Input=Alt+Left
        Type=KEYBOARD_INPUT
        
        [Data_3_7_1Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_7_1Triggers]
        Comment=Gesture_triggers
        TriggersCount=3
        
        [Data_3_7_1Triggers0]
        GesturePointData=0,0.0625,1,1,0.5,0.0625,0.0625,1,0.875,0.5,0.125,0.0625,1,0.75,0.5,0.1875,0.0625,1,0.625,0.5,0.25,0.0625,1,0.5,0.5,0.3125,0.0625,1,0.375,0.5,0.375,0.0625,1,0.25,0.5,0.4375,0.0625,1,0.125,0.5,0.5,0.0625,0,0,0.5,0.5625,0.0625,0,0.125,0.5,0.625,0.0625,0,0.25,0.5,0.6875,0.0625,0,0.375,0.5,0.75,0.0625,0,0.5,0.5,0.8125,0.0625,0,0.625,0.5,0.875,0.0625,0,0.75,0.5,0.9375,0.0625,0,0.875,0.5,1,0,0,1,0.5
        Type=GESTURE
        
        [Data_3_7_1Triggers1]
        GesturePointData=0,0.0833333,1,0.5,0.5,0.0833333,0.0833333,1,0.375,0.5,0.166667,0.0833333,1,0.25,0.5,0.25,0.0833333,1,0.125,0.5,0.333333,0.0833333,0,0,0.5,0.416667,0.0833333,0,0.125,0.5,0.5,0.0833333,0,0.25,0.5,0.583333,0.0833333,0,0.375,0.5,0.666667,0.0833333,0,0.5,0.5,0.75,0.0833333,0,0.625,0.5,0.833333,0.0833333,0,0.75,0.5,0.916667,0.0833333,0,0.875,0.5,1,0,0,1,0.5
        Type=GESTURE
        
        [Data_3_7_1Triggers2]
        GesturePointData=0,0.0833333,1,1,0.5,0.0833333,0.0833333,1,0.875,0.5,0.166667,0.0833333,1,0.75,0.5,0.25,0.0833333,1,0.625,0.5,0.333333,0.0833333,1,0.5,0.5,0.416667,0.0833333,1,0.375,0.5,0.5,0.0833333,1,0.25,0.5,0.583333,0.0833333,1,0.125,0.5,0.666667,0.0833333,0,0,0.5,0.75,0.0833333,0,0.125,0.5,0.833333,0.0833333,0,0.25,0.5,0.916667,0.0833333,0,0.375,0.5,1,0,0,0.5,0.5
        Type=GESTURE
        
        [Data_3_7_2]
        Comment=
        Enabled=false
        Name=Forward
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_7_2Actions]
        ActionsCount=1
        
        [Data_3_7_2Actions0]
        DestinationWindow=2
        Input=Alt+Right
        Type=KEYBOARD_INPUT
        
        [Data_3_7_2Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_7_2Triggers]
        Comment=Gesture_triggers
        TriggersCount=3
        
        [Data_3_7_2Triggers0]
        GesturePointData=0,0.0625,0,0,0.5,0.0625,0.0625,0,0.125,0.5,0.125,0.0625,0,0.25,0.5,0.1875,0.0625,0,0.375,0.5,0.25,0.0625,0,0.5,0.5,0.3125,0.0625,0,0.625,0.5,0.375,0.0625,0,0.75,0.5,0.4375,0.0625,0,0.875,0.5,0.5,0.0625,1,1,0.5,0.5625,0.0625,1,0.875,0.5,0.625,0.0625,1,0.75,0.5,0.6875,0.0625,1,0.625,0.5,0.75,0.0625,1,0.5,0.5,0.8125,0.0625,1,0.375,0.5,0.875,0.0625,1,0.25,0.5,0.9375,0.0625,1,0.125,0.5,1,0,0,0,0.5
        Type=GESTURE
        
        [Data_3_7_2Triggers1]
        GesturePointData=0,0.0833333,0,0.5,0.5,0.0833333,0.0833333,0,0.625,0.5,0.166667,0.0833333,0,0.75,0.5,0.25,0.0833333,0,0.875,0.5,0.333333,0.0833333,1,1,0.5,0.416667,0.0833333,1,0.875,0.5,0.5,0.0833333,1,0.75,0.5,0.583333,0.0833333,1,0.625,0.5,0.666667,0.0833333,1,0.5,0.5,0.75,0.0833333,1,0.375,0.5,0.833333,0.0833333,1,0.25,0.5,0.916667,0.0833333,1,0.125,0.5,1,0,0,0,0.5
        Type=GESTURE
        
        [Data_3_7_2Triggers2]
        GesturePointData=0,0.0833333,0,0,0.5,0.0833333,0.0833333,0,0.125,0.5,0.166667,0.0833333,0,0.25,0.5,0.25,0.0833333,0,0.375,0.5,0.333333,0.0833333,0,0.5,0.5,0.416667,0.0833333,0,0.625,0.5,0.5,0.0833333,0,0.75,0.5,0.583333,0.0833333,0,0.875,0.5,0.666667,0.0833333,1,1,0.5,0.75,0.0833333,1,0.875,0.5,0.833333,0.0833333,1,0.75,0.5,0.916667,0.0833333,1,0.625,0.5,1,0,0,0.5,0.5
        Type=GESTURE
        
        [Data_3_7_3]
        Comment=
        Enabled=false
        Name=Up
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_7_3Actions]
        ActionsCount=1
        
        [Data_3_7_3Actions0]
        DestinationWindow=2
        Input=Alt+Up
        Type=KEYBOARD_INPUT
        
        [Data_3_7_3Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_7_3Triggers]
        Comment=Gesture_triggers
        TriggersCount=3
        
        [Data_3_7_3Triggers0]
        GesturePointData=0,0.0625,-0.5,0.5,1,0.0625,0.0625,-0.5,0.5,0.875,0.125,0.0625,-0.5,0.5,0.75,0.1875,0.0625,-0.5,0.5,0.625,0.25,0.0625,-0.5,0.5,0.5,0.3125,0.0625,-0.5,0.5,0.375,0.375,0.0625,-0.5,0.5,0.25,0.4375,0.0625,-0.5,0.5,0.125,0.5,0.0625,0.5,0.5,0,0.5625,0.0625,0.5,0.5,0.125,0.625,0.0625,0.5,0.5,0.25,0.6875,0.0625,0.5,0.5,0.375,0.75,0.0625,0.5,0.5,0.5,0.8125,0.0625,0.5,0.5,0.625,0.875,0.0625,0.5,0.5,0.75,0.9375,0.0625,0.5,0.5,0.875,1,0,0,0.5,1
        Type=GESTURE
        
        [Data_3_7_3Triggers1]
        GesturePointData=0,0.0833333,-0.5,0.5,1,0.0833333,0.0833333,-0.5,0.5,0.875,0.166667,0.0833333,-0.5,0.5,0.75,0.25,0.0833333,-0.5,0.5,0.625,0.333333,0.0833333,-0.5,0.5,0.5,0.416667,0.0833333,-0.5,0.5,0.375,0.5,0.0833333,-0.5,0.5,0.25,0.583333,0.0833333,-0.5,0.5,0.125,0.666667,0.0833333,0.5,0.5,0,0.75,0.0833333,0.5,0.5,0.125,0.833333,0.0833333,0.5,0.5,0.25,0.916667,0.0833333,0.5,0.5,0.375,1,0,0,0.5,0.5
        Type=GESTURE
        
        [Data_3_7_3Triggers2]
        GesturePointData=0,0.0833333,-0.5,0.5,0.5,0.0833333,0.0833333,-0.5,0.5,0.375,0.166667,0.0833333,-0.5,0.5,0.25,0.25,0.0833333,-0.5,0.5,0.125,0.333333,0.0833333,0.5,0.5,0,0.416667,0.0833333,0.5,0.5,0.125,0.5,0.0833333,0.5,0.5,0.25,0.583333,0.0833333,0.5,0.5,0.375,0.666667,0.0833333,0.5,0.5,0.5,0.75,0.0833333,0.5,0.5,0.625,0.833333,0.0833333,0.5,0.5,0.75,0.916667,0.0833333,0.5,0.5,0.875,1,0,0,0.5,1
        Type=GESTURE
        
        [Data_3_7_4]
        Comment=
        Enabled=false
        Name=Reload
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_7_4Actions]
        ActionsCount=1
        
        [Data_3_7_4Actions0]
        DestinationWindow=2
        Input=F5
        Type=KEYBOARD_INPUT
        
        [Data_3_7_4Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_7_4Triggers]
        Comment=Gesture_triggers
        TriggersCount=3
        
        [Data_3_7_4Triggers0]
        GesturePointData=0,0.03125,0,0,1,0.03125,0.03125,0,0.125,1,0.0625,0.03125,0,0.25,1,0.09375,0.03125,0,0.375,1,0.125,0.03125,0,0.5,1,0.15625,0.03125,0,0.625,1,0.1875,0.03125,0,0.75,1,0.21875,0.03125,0,0.875,1,0.25,0.03125,-0.5,1,1,0.28125,0.03125,-0.5,1,0.875,0.3125,0.03125,-0.5,1,0.75,0.34375,0.03125,-0.5,1,0.625,0.375,0.03125,-0.5,1,0.5,0.40625,0.03125,-0.5,1,0.375,0.4375,0.03125,-0.5,1,0.25,0.46875,0.03125,-0.5,1,0.125,0.5,0.03125,1,1,0,0.53125,0.03125,1,0.875,0,0.5625,0.03125,1,0.75,0,0.59375,0.03125,1,0.625,0,0.625,0.03125,1,0.5,0,0.65625,0.03125,1,0.375,0,0.6875,0.03125,1,0.25,0,0.71875,0.03125,1,0.125,0,0.75,0.03125,0.5,0,0,0.78125,0.03125,0.5,0,0.125,0.8125,0.03125,0.5,0,0.25,0.84375,0.03125,0.5,0,0.375,0.875,0.03125,0.5,0,0.5,0.90625,0.03125,0.5,0,0.625,0.9375,0.03125,0.5,0,0.75,0.96875,0.03125,0.5,0,0.875,1,0,0,0,1
        Type=GESTURE
        
        [Data_3_7_4Triggers1]
        GesturePointData=0,0.0277778,0,0,1,0.0277778,0.0277778,0,0.125,1,0.0555556,0.0277778,0,0.25,1,0.0833333,0.0277778,0,0.375,1,0.111111,0.0277778,0,0.5,1,0.138889,0.0277778,0,0.625,1,0.166667,0.0277778,0,0.75,1,0.194444,0.0277778,0,0.875,1,0.222222,0.0277778,-0.5,1,1,0.25,0.0277778,-0.5,1,0.875,0.277778,0.0277778,-0.5,1,0.75,0.305556,0.0277778,-0.5,1,0.625,0.333333,0.0277778,-0.5,1,0.5,0.361111,0.0277778,-0.5,1,0.375,0.388889,0.0277778,-0.5,1,0.25,0.416667,0.0277778,-0.5,1,0.125,0.444444,0.0277778,1,1,0,0.472222,0.0277778,1,0.875,0,0.5,0.0277778,1,0.75,0,0.527778,0.0277778,1,0.625,0,0.555556,0.0277778,1,0.5,0,0.583333,0.0277778,1,0.375,0,0.611111,0.0277778,1,0.25,0,0.638889,0.0277778,1,0.125,0,0.666667,0.0277778,0.5,0,0,0.694444,0.0277778,0.5,0,0.125,0.722222,0.0277778,0.5,0,0.25,0.75,0.0277778,0.5,0,0.375,0.777778,0.0277778,0.5,0,0.5,0.805556,0.0277778,0.5,0,0.625,0.833333,0.0277778,0.5,0,0.75,0.861111,0.0277778,0.5,0,0.875,0.888889,0.0277778,0,0,1,0.916667,0.0277778,0,0.125,1,0.944444,0.0277778,0,0.25,1,0.972222,0.0277778,0,0.375,1,1,0,0,0.5,1
        Type=GESTURE
        
        [Data_3_7_4Triggers2]
        GesturePointData=0,0.0277778,0.5,0,0.5,0.0277778,0.0277778,0.5,0,0.625,0.0555556,0.0277778,0.5,0,0.75,0.0833333,0.0277778,0.5,0,0.875,0.111111,0.0277778,0,0,1,0.138889,0.0277778,0,0.125,1,0.166667,0.0277778,0,0.25,1,0.194444,0.0277778,0,0.375,1,0.222222,0.0277778,0,0.5,1,0.25,0.0277778,0,0.625,1,0.277778,0.0277778,0,0.75,1,0.305556,0.0277778,0,0.875,1,0.333333,0.0277778,-0.5,1,1,0.361111,0.0277778,-0.5,1,0.875,0.388889,0.0277778,-0.5,1,0.75,0.416667,0.0277778,-0.5,1,0.625,0.444444,0.0277778,-0.5,1,0.5,0.472222,0.0277778,-0.5,1,0.375,0.5,0.0277778,-0.5,1,0.25,0.527778,0.0277778,-0.5,1,0.125,0.555556,0.0277778,1,1,0,0.583333,0.0277778,1,0.875,0,0.611111,0.0277778,1,0.75,0,0.638889,0.0277778,1,0.625,0,0.666667,0.0277778,1,0.5,0,0.694444,0.0277778,1,0.375,0,0.722222,0.0277778,1,0.25,0,0.75,0.0277778,1,0.125,0,0.777778,0.0277778,0.5,0,0,0.805556,0.0277778,0.5,0,0.125,0.833333,0.0277778,0.5,0,0.25,0.861111,0.0277778,0.5,0,0.375,0.888889,0.0277778,0.5,0,0.5,0.916667,0.0277778,0.5,0,0.625,0.944444,0.0277778,0.5,0,0.75,0.972222,0.0277778,0.5,0,0.875,1,0,0,0,1
        Type=GESTURE
        
        [Data_3_8]
        Comment=After pressing Win+E (Tux+E) a WWW browser will be launched, and it will open http://www.kde.org . You may run all kind of commands you can run in minicli (Alt+F2).
        Enabled=false
        Name=Go to KDE Website
        Type=SIMPLE_ACTION_DATA
        
        [Data_3_8Actions]
        ActionsCount=1
        
        [Data_3_8Actions0]
        CommandURL=http://www.kde.org
        Type=COMMAND_URL
        
        [Data_3_8Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_3_8Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_3_8Triggers0]
        Key=Meta+E
        Type=SHORTCUT
        Uuid={2b0e7c92-ddb3-469e-a3fd-3e43a4e32279}
        
        [Data_4]
        Comment=Shortcuts for taking screenshots
        DataCount=4
        Enabled=true
        ImportId=spectacle
        Name=Screenshots
        SystemGroup=0
        Type=ACTION_DATA_GROUP
        
        [Data_4Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_4_1]
        Comment=Start the screenshot tool and show the GUI
        Enabled=true
        Name=Start Screenshot Tool
        Type=SIMPLE_ACTION_DATA
        
        [Data_4_1Actions]
        ActionsCount=1
        
        [Data_4_1Actions0]
        Arguments=
        Call=StartAgent
        RemoteApp=org.kde.Spectacle
        RemoteObj=/
        Type=DBUS
        
        [Data_4_1Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_4_1Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_4_1Triggers0]
        Key=
        Type=SHORTCUT
        Uuid={2d0c9236-40f1-40dc-bcf1-2bc6f6cf131d}
        
        [Data_4_2]
        Comment=Take a full screen (all monitors) screenshot and save it
        Enabled=true
        Name=Take Full Screen Screenshot
        Type=SIMPLE_ACTION_DATA
        
        [Data_4_2Actions]
        ActionsCount=1
        
        [Data_4_2Actions0]
        Arguments=false
        Call=FullScreen
        RemoteApp=org.kde.Spectacle
        RemoteObj=/
        Type=DBUS
        
        [Data_4_2Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_4_2Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_4_2Triggers0]
        Key=
        Type=SHORTCUT
        Uuid={6537c7b3-72c7-48ed-a546-3697a5dc27dd}
        
        [Data_4_3]
        Comment=Take a screenshot of the currently active window and save it
        Enabled=true
        Name=Take Active Window Screenshot
        Type=SIMPLE_ACTION_DATA
        
        [Data_4_3Actions]
        ActionsCount=1
        
        [Data_4_3Actions0]
        Arguments=true, false
        Call=ActiveWindow
        RemoteApp=org.kde.Spectacle
        RemoteObj=/
        Type=DBUS
        
        [Data_4_3Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_4_3Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_4_3Triggers0]
        Key=Meta+Print
        Type=SHORTCUT
        Uuid={68727c50-6cca-4e8c-b239-8064ee3fed2a}
        
        [Data_4_4]
        Comment=Take a screenshot of a rectangular region you specify and save it
        Enabled=true
        Name=Take Rectangular Region Screenshot
        Type=SIMPLE_ACTION_DATA
        
        [Data_4_4Actions]
        ActionsCount=1
        
        [Data_4_4Actions0]
        Arguments=true
        Call=RectangularRegion
        RemoteApp=org.kde.Spectacle
        RemoteObj=/
        Type=DBUS
        
        [Data_4_4Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_4_4Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_4_4Triggers0]
        Key=Meta+Shift+Print
        Type=SHORTCUT
        Uuid={f4910c81-c02e-4158-8f54-4b2f9c6704f3}
        
        [Data_5]
        Comment=Comment
        Enabled=true
        Name=terminal
        Type=SIMPLE_ACTION_DATA
        
        [Data_5Actions]
        ActionsCount=1
        
        [Data_5Actions0]
        CommandURL=terminal
        Type=COMMAND_URL
        
        [Data_5Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_5Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_5Triggers0]
        Key=Meta+Return
        Type=SHORTCUT
        Uuid={394f4792-d7d6-475e-8fca-93d1647a68f2}
        
        [Data_6]
        Comment=Comment
        Enabled=true
        Name=file-manager
        Type=SIMPLE_ACTION_DATA
        
        [Data_6Actions]
        ActionsCount=1
        
        [Data_6Actions0]
        CommandURL=terminal -e file-manager
        Type=COMMAND_URL
        
        [Data_6Conditions]
        Comment=
        ConditionsCount=0
        
        [Data_6Triggers]
        Comment=Simple_action
        TriggersCount=1
        
        [Data_6Triggers0]
        Key=
        Type=SHORTCUT
        Uuid={3e1487dc-22f8-4503-b248-e0a0dbeabe59}
        
        [Gestures]
        Disabled=true
        MouseButton=2
        Timeout=300
        
        [GesturesExclude]
        Comment=
        WindowsCount=0
        
        [Main]
        AlreadyImported=defaults,konqueror_gestures_kde321,kde32b1,spectacle,konsole
        Disabled=false
        Version=2
        
        [Voice]
        Shortcut=

/home/ahrs/.config/kglobalshortcutsrc:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - sections:
        plasmashell:
          "activate widget 105": Alt+F1,none,Activate Application Launcher Widget
        khotkeys:
          "{394f4792-d7d6-475e-8fca-93d1647a68f2}": Meta+Return,none,terminal
          "{3e1487dc-22f8-4503-b248-e0a0dbeabe59}": Meta+E,none,file-manager
        kwin:
          "Switch to Next Desktop": Meta+],none,Switch to Next Desktop
          "Switch to Previous Desktop": Meta+[,none,Switch to Previous Desktop
          "Window Quick Tile Bottom": Meta+Shift+J,Meta+Down,Quick Tile Window to the Bottom
          "Window Quick Tile Bottom Left": Meta+Shift+N,none,Quick Tile Window to the Bottom Left
          "Window Quick Tile Bottom Right": Meta+Shift+M,none,Quick Tile Window to the Bottom Right
          "Window Quick Tile Left": Meta+Shift+H,Meta+Left,Quick Tile Window to the Left
          "Window Quick Tile Right": Meta+Shift+L,Meta+Right,Quick Tile Window to the Right
          "Window Quick Tile Top": Meta+Shift+K,Meta+Up,Quick Tile Window to the Top
          "Window Quick Tile Top Left": Meta+Shift+Y,none,Quick Tile Window to the Top Left
          "Window Quick Tile Top Right": Meta+Shift+I,none,Quick Tile Window to the Top Right
          "Window Maximize": Meta+M,Meta+PgUp,Maximize Window
          "Window Minimize": Meta+N,Meta+PgDown,Minimize Window
          "Window Fullscreen": Meta+F,none,Make Window Fullscreen
          "Window No Border": Meta+B,none,Hide Window Border
        krunner:
          _k_friendly_name: Run Command
          run command: Alt+Space\tAlt+F2\t,Alt+Space,Run Command
          run command on clipboard contents: Alt+Shift+F2,Alt+Shift+F2,Run Command on clipboard contents
{% if grains['os_family'] == 'Gentoo' %}
{% set kickoff_icon = 'start-here-gentoo' %}
{% elif grains['os'] == 'Arch' %}
{% set kickoff_icon = 'start-here-archlinux' %}
{% elif grains['os'] == 'Antergos' %}
{% set kickoff_icon = 'start-here-antergos' %}
{% elif grains['os'] == 'Manjaro' %}
{% set kickoff_icon = 'start-here-manjaro' %}
{% elif grains['os'] == 'Ubuntu' %}
{% set kickoff_icon = 'start-here-ubuntu' %}
{% elif grains['os_family'] == 'Debian' %}
{% set kickoff_icon = 'start-here-debian' %}
{% elif grains['os_family']|lower == 'suse' %}
{% set kickoff_icon = 'start-here-opensuse' %}
{% elif grains['os']|lower == 'fedora' %}
{% set kickoff_icon = 'start-here-fedora' %}
{% elif grains['os_family'] == 'RedHat' %}
{% set kickoff_icon = 'redhat' %}
{% else %}
{% set kickoff_icon = 'org.kde.plasma.kickoff' %}
{% endif %}

/home/ahrs/.config/kactivitymanagerdrc:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - sections:
        activities:
          "b6014c4d-33d6-44f3-aad8-9dc9d826891c": Default

/home/ahrs/.config/breezerc:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - sections:
        Windeco:
          DrawTitleBarSeparator: "false"

/home/ahrs/.config/plasma-org.kde.plasma.desktop-appletsrc:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs
  ini.options_present:
    - sections:
        ActionPlugins][0:
          MidButton;NoModifier: org.kde.paste
          RightButton;NoModifier: org.kde.contextmenu
          wheel:Vertical;NoModifier: org.kde.switchdesktop
        ActionPlugins][1:
          RightButton;NoModifier: org.kde.contextmenu
        Containments][103:
          activityId: b6014c4d-33d6-44f3-aad8-9dc9d826891c
          formfactor: '0'
          immutability: '1'
          lastScreen: '0'
          location: '0'
          plugin: org.kde.plasma.folder
          wallpaperplugin: org.kde.image
        Containments][103][Configuration:
          PreloadWeight: '0'
        Containments][103][General:
          ToolBoxButtonState: bottomleft
          iconSize: 3
          sortMode: -1
        Containments][104:
          activityId: ''
          formfactor: '2'
          immutability: '1'
          lastScreen: '0'
          location: '3'
          plugin: org.kde.panel
          wallpaperplugin: org.kde.image
        Containments][104][Applets][105:
          immutability: '1'
          plugin: org.kde.plasma.kickoff
        Containments][104][Applets][105][Configuration:
          PreloadWeight: '100'
        Containments][104][Applets][105][Configuration][ConfigDialog:
          DialogHeight: ''
          DialogWidth: ''
        Containments][104][Applets][105][Configuration][General:
          favoritesPortedToKAstats: 'true'
          icon: {{kickoff_icon}}
        Containments][104][Applets][105][Configuration][Shortcuts:
          global: Alt+F1
        Containments][104][Applets][105][Shortcuts:
          global: Alt+F1
        Containments][104][Applets][106:
          immutability: '1'
          plugin: org.kde.plasma.pager
        Containments][104][Applets][106][Configuration:
          PreloadWeight: '0'
        Containments][104][Applets][106][Configuration][ConfigDialog:
          DialogHeight: ''
          DialogWidth: ''
        Containments][104][Applets][106][Configuration][General:
          currentDesktopSelected: ShowDesktop
        Containments][104][Applets][108:
          immutability: '1'
          plugin: org.kde.plasma.systemtray
        Containments][104][Applets][108][Configuration:
          PreloadWeight: '0'
          SystrayContainmentId: '109'
        Containments][104][Applets][110:
          immutability: '1'
          plugin: org.kde.plasma.digitalclock
        Containments][104][Applets][110][Configuration:
          PreloadWeight: '0'
        Containments][104][Applets][120:
          immutability: '1'
          plugin: org.kde.plasma.panelspacer
        Containments][104][Applets][120][Configuration:
          PreloadWeight: '0'
        Containments][104][Applets][120][Configuration][General:
          length: '2448'
        Containments][104][Applets][121:
          immutability: '1'
          plugin: org.kde.plasma.appmenu
        Containments][104][Applets][121][Configuration:
          PreloadWeight: '0'
        Containments][104][Applets][121][Configuration][ConfigDialog:
          DialogHeight: ''
          DialogWidth: ''
        Containments][104][Applets][122:
          immutability: '1'
          plugin: org.kde.plasma.panelspacer
        Containments][104][Applets][122][Configuration:
          PreloadWeight: '0'
        Containments][104][Applets][122][Configuration][General:
          length: '39'
        Containments][104][ConfigDialog:
          DialogHeight: ''
          DialogWidth: ''
        Containments][104][Configuration:
          PreloadWeight: '0'
        Containments][104][General:
          AppletOrder: 105;121;120;106;108;110;122
        Containments][109:
          activityId: ''
          formfactor: '2'
          immutability: '1'
          lastScreen: '0'
          location: '3'
          plugin: org.kde.plasma.private.systemtray
          wallpaperplugin: org.kde.image
        Containments][109][Applets][111:
          immutability: '1'
          plugin: org.kde.plasma.volume
        Containments][109][Applets][111][Configuration:
          PreloadWeight: '0'
        Containments][109][Applets][112:
          immutability: '1'
          plugin: org.kde.plasma.clipboard
        Containments][109][Applets][112][Configuration:
          PreloadWeight: '0'
        Containments][109][Applets][113:
          immutability: '1'
          plugin: org.kde.plasma.devicenotifier
        Containments][109][Applets][113][Configuration:
          PreloadWeight: '0'
        Containments][109][Applets][114:
          immutability: '1'
          plugin: org.kde.plasma.keyboardindicator
        Containments][109][Applets][114][Configuration:
          PreloadWeight: '0'
        Containments][109][Applets][115:
          immutability: '1'
          plugin: org.kde.plasma.notifications
        Containments][109][Applets][115][Configuration:
          PreloadWeight: '0'
        Containments][109][Applets][116:
          immutability: '1'
          plugin: org.kde.discovernotifier
        Containments][109][Applets][116][Configuration:
          PreloadWeight: '0'
        Containments][109][Applets][117:
          immutability: '1'
          plugin: org.kde.plasma.vault
        Containments][109][Applets][117][Configuration:
          PreloadWeight: '0'
        Containments][109][Applets][118:
          immutability: '1'
          plugin: org.kde.plasma.battery
        Containments][109][Applets][118][Configuration:
          PreloadWeight: '0'
        Containments][109][Applets][119:
          immutability: '1'
          plugin: org.kde.plasma.networkmanagement
        Containments][109][Applets][119][Configuration:
          PreloadWeight: '0'
        Containments][109][Configuration:
          PreloadWeight: '0'
        Containments][109][General:
          extraItems: org.kde.plasma.battery,org.kde.plasma.clipboard,org.kde.plasma.devicenotifier,org.kde.plasma.mediacontroller,org.kde.plasma.notifications,org.kde.plasma.volume,org.kde.discovernotifier,org.kde.plasma.networkmanagement,org.kde.plasma.vault,org.kde.plasma.bluetooth,org.kde.plasma.keyboardindicator
          knownItems: org.kde.plasma.battery,org.kde.plasma.clipboard,org.kde.plasma.devicenotifier,org.kde.plasma.mediacontroller,org.kde.plasma.notifications,org.kde.plasma.volume,org.kde.discovernotifier,org.kde.plasma.networkmanagement,org.kde.plasma.vault,org.kde.plasma.bluetooth,org.kde.plasma.keyboardindicator
        Containments][123:
          activityId: 00000000-0000-0000-0000-000000000000
          formfactor: '0'
          immutability: '1'
          lastScreen: '0'
          location: '0'
          plugin: org.kde.plasma.folder
          wallpaperplugin: org.kde.image
        Containments][123][Configuration:
          PreloadWeight: '0'
        Containments][1][Configuration:
          PreloadWeight: '42'
        Containments][2][Applets][3][Configuration:
          PreloadWeight: '92'
        Containments][2][Applets][4][Configuration:
          PreloadWeight: '42'
        Containments][2][Applets][5][Configuration:
          PreloadWeight: '42'
        Containments][2][Applets][6][Configuration:
          PreloadWeight: '42'
        Containments][2][Applets][8][Configuration:
          PreloadWeight: '42'
        Containments][2][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][10][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][11][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][12][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][13][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][14][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][15][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][16][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][17][Configuration:
          PreloadWeight: '42'
        Containments][7][Applets][9][Configuration:
          PreloadWeight: '42'
        Containments][7][Configuration:
          PreloadWeight: '42'
        Containments][85][Configuration:
          PreloadWeight: '42'
        Containments][86][Applets][87][Configuration:
          PreloadWeight: '92'
        Containments][86][Applets][88][Configuration:
          PreloadWeight: '42'
        Containments][86][Applets][90][Configuration:
          PreloadWeight: '42'
        Containments][86][Applets][92][Configuration:
          PreloadWeight: '42'
        Containments][86][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][100][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][101][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][93][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][94][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][95][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][96][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][97][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][98][Configuration:
          PreloadWeight: '42'
        Containments][91][Applets][99][Configuration:
          PreloadWeight: '42'
        Containments][91][Configuration:
          PreloadWeight: '42'
        General:
          immutability: '2'
        ScreenMapping:
          itemsOnDisabledScreens: ''

{% set kactivitymanagerd_db = '/home/ahrs/.local/share/kactivitymanagerd/resources/database' %}

{{kactivitymanagerd_db}}:
  file.managed:
    - makedirs: True
    - user: ahrs
    - group: ahrs

kactivitymanagerd_SchemaInfo:
  sqlite3.table_present:
    - db: {{kactivitymanagerd_db}}
    - schema: CREATE TABLE IF NOT EXISTS SchemaInfo (key text PRIMARY KEY, value text);
    - require:
      - file: {{kactivitymanagerd_db}}

kactivitymanagerd_ResourceEvent:
  sqlite3.table_present:
    - db: {{kactivitymanagerd_db}}
    - schema: CREATE TABLE IF NOT EXISTS ResourceEvent (usedActivity TEXT, initiatingAgent TEXT, targettedResource TEXT, start INTEGER, end INTEGER );

kactivitymanagerd_ResourceScoreCache:
  sqlite3.table_present:
    - db: {{kactivitymanagerd_db}}
    - schema: CREATE TABLE IF NOT EXISTS ResourceScoreCache (usedActivity TEXT, initiatingAgent TEXT, targettedResource TEXT, scoreType INTEGER, cachedScore FLOAT, firstUpdate INTEGER, lastUpdate INTEGER, PRIMARY KEY(usedActivity, initiatingAgent, targettedResource));

kactivitymanagerd_drop_ResourceLink:
  module.run:
    - name: sqlite3.modify
    - db: {{kactivitymanagerd_db}}
    - sql: DROP TABLE IF EXISTS ResourceLink;

kactivitymanagerd_ResourceLink:
  sqlite3.table_present:
    - db: {{kactivitymanagerd_db}}
    - schema: CREATE TABLE ResourceLink (usedActivity TEXT, initiatingAgent TEXT, targettedResource TEXT, PRIMARY KEY(usedActivity, initiatingAgent, targettedResource));
    - require:
      - module: kactivitymanagerd_drop_ResourceLink

kactivitymanagerd_ResourceInfo:
  sqlite3.table_present:
    - db: {{kactivitymanagerd_db}}
    - schema: CREATE TABLE IF NOT EXISTS ResourceInfo (targettedResource TEXT, title TEXT, mimetype TEXT, autoTitle INTEGER, autoMimetype INTEGER, PRIMARY KEY(targettedResource));

{% for favourite in [
  'preferred://browser',
  'applications:file-manager.desktop',
  'applications:org.kde.discover.desktop',
  'applications:systemsettings.desktop',
  'applications:org.kde.kate.desktop',
] %}
org.kde.plasma.favourites.applications_{{favourite}}:
  sqlite3.table_present:
    - db: {{kactivitymanagerd_db}}
    - schema: INSERT INTO ResourceLink VALUES(':global','org.kde.plasma.favorites.applications','{{favourite}}');
    - require:
      - sqlite3: kactivitymanagerd_ResourceLink
{% endfor %}
