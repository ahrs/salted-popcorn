{% from "desktops/gnome/map.jinja" import gnome with context %}

{% if grains['os']|lower == 'ubuntu' and grains['osmajorrelease'] >= 18 and grains['oscodename']|lower != 'bionic' %}
include:
  - themes.yaru.yaru
{% endif %}

gnome:
  pkg.installed:
    - pkgs:
      {% for pkg in gnome %}
      - {{gnome[pkg]}}
      {% endfor %}
    - install_recommends: False


