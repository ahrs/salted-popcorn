{% set lxde = salt['grains.filter_by']({
  'default': {
    'lxde': 'lxde',
     'lxde-common': 'lxde-common',
     'lxsession': 'lxsession',
     'openbox': 'openbox'
  },
  'Gentoo': {
    'lxde': 'lxde-base/lxde-meta',
    'lxde-common':  'lxde-base/lxde-common',
    'lxsession':  'lxde-base/lxsession',
    'openbox':  'x11-wm/openbox'
  }
}, grain='os_family', merge = salt['pillar.get']('lxde:lookup'), base='default') %}
lxde:
  pkg.installed:
    - pkgs:
      {% for pkg in lxde %}
      - {{lxde[pkg]}}
      {% endfor %}
