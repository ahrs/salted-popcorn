{% from "desktops/budgie/map.jinja" import budgie with context %}

# https://wiki.gentoo.org/wiki/Budgie
{% if grains['os_family'] == 'Gentoo' %}
budgie_add_sabayon_overlay:
  cmd.run:
    - name: |
        set -e -x
        layman -q -d sabayon || true
        if [ ! -e /etc/layman/layman.cfg ]
        then
          mkdir -p /etc/layman
          echo "check_official : No" > /etc/layman/layman.cfg
        fi
        sed 's|check_official.*|check_official : No|g' /etc/layman/layman.cfg | layman --config=/dev/stdin -q -a sabayon
{% endif %}

budgie:
  pkg.installed:
    - pkgs:
      - {{budgie['budgie-desktop']}}
    - install_recommends: False
