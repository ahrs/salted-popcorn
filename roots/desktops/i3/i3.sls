{% from "desktops/i3/map.jinja" import i3wm with context %}

i3_remove_gaps:
  pkg.removed:
    - pkgs:
      - {{i3wm['i3-gaps']}}
i3:
  pkg.installed:
    - pkgs:
      - {{i3wm['i3-wm']}}
      - {{i3wm['i3status']}}
      - {{i3wm['i3lock']}}
