{% from "desktops/i3/map.jinja" import i3wm with context %}

i3_remove:
  pkg.removed:
    - pkgs:
      - {{i3wm['i3-wm']}}
i3-extras:
  pkg.installed:
    - pkgs:
      - {{i3wm['i3status']}}
      - {{i3wm['i3lock']}}
    - install_recommends: False
{% if grains['os_family'] != 'Debian' and i3wm['i3-gaps'] != None %}
i3-gaps:
  pkg.installed:
    - pkgs:
      - {{i3wm['i3-gaps']}}
{% else %}
include:
  - applications.utilities.stow.stow
i3-gaps:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp
        rm -rf i3-gaps
        sudo='sudo -u nobody --'
        $sudo git clone https://github.com/Airblader/i3 i3-gaps
        sed -i 's|^# deb-src|deb-src|g' /etc/apt/sources.list
        if ! grep -q 'deb-src' /etc/apt/sources.list
        then
          grep -E '^deb ' /etc/apt/sources.list | while read -r line
          do
          line="$(printf "%s" "$line" | sed 's|^deb |deb-src |g')"
          echo "$line" >> /etc/apt/sources.list
          done
        fi
        apt-get update -qq
        apt-get build-dep --yes i3-wm
        cd i3-gaps
        $sudo autoreconf --force --install
        $sudo rm -rf build/
        $sudo mkdir -p build && cd build/

        # Disabling sanitizers is important for release versions!
        # The prefix and sysconfdir are, obviously, dependent on the distribution.
        $sudo ../configure --prefix=/usr/local --sysconfdir=/usr/local/etc --docdir=/usr/local/share/doc/i3-wm --disable-sanitizers
        $sudo make -j$(expr $(nproc) + 1)
        [ -d /stow/i3-gaps.bak ] && rm -rf /stow/i3-gaps.bak
        [ -d /stow/i3-gaps ] && {
          cd /stow
          stow -t / -D i3-gaps
          mv i3-gaps i3-gaps.bak
          cd -
        }
        mkdir -p /stow/i3-gaps
        make install DESTDIR=/stow/i3-gaps
        cd /stow
        stow -t / i3-gaps
{% endif %}
