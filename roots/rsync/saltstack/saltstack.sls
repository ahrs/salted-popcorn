/var/www/saltstack:
  rsync.synchronized:
    - source: rsync://rsync.repo.saltstack.com/saltstack_pkgrepo_full/
    - delete: True
    - additional_opts:
      - -vaH
      - --links
      - --numeric-ids
      - --delete-after
      - --delay-updates
