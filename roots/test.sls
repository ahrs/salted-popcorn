{% from "detect-laptop.jinja" import detect_laptop with context %}

{% set acpi = 'acpi' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set acpi = 'sys-power/' + acpi %}
{% endif %}

{% set brightnessctl = 'brightnessctl' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set brightnessctl = 'x11-misc/' + brightnessctl %}
{% endif %}

acpi:
  pkg.installed:
    - pkgs:
      - {{acpi}}
    - onlyif: |
        {{detect_laptop()}}

brightnessctl:
  pkg.installed:
    - pkgs:
      - {{brightnessctl}}
    - onlyif: |
        {{detect_laptop()}}
