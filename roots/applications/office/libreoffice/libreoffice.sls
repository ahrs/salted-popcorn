{% set libreoffice = 'libreoffice' %}
{% if grains['os_family'] == "Gentoo" %}
{% set libreoffice = 'app-office/' + libreoffice %}
{% endif %}

{% if grains['os'] == "Ubuntu" %}
libreoffice_repo_key:
  module.run:
    - name: pkg.add_repo_key
    - keyserver: hkp://keyserver.ubuntu.com:80
    - keyid: 36E81C9267FD1383FCC4490983FBA1751378B444
/etc/apt/sources.list.d/libreoffice.list:
  file.managed:
    - contents: |
        deb http://ppa.launchpad.net/libreoffice/ppa/ubuntu {{grains['oscodename']}} main
        deb-src http://ppa.launchpad.net/libreoffice/ppa/ubuntu {{grains['oscodename']}} main
libreoffice_refresh_db:
  module.run:
    - name: pkg.refresh_db
{% endif %}

{{libreoffice}}:
  pkg.installed
