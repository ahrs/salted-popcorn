{% if grains['os_family'] == 'Arch' %}
{% set packer = 'packer-io' %}
{% else %}
{% set packer = 'packer' %}
{% endif %}
packer-io:
  pkg.installed:
    - pkgs:
      - {{packer}}
