{% set mono = 'mono' %}
{% if grains['os_family'] == 'Debian' %}
{% set mono = 'mono-devel' %}
{% elif grains['os_family'] == 'Gentoo' %}
{% set mono = 'dev-lang/mono' %}
{% endif %}
mono:
  pkg.installed:
    - pkgs:
      - {{mono}}
    - install_recommends: False
