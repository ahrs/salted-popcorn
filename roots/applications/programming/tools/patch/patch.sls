{% set patch = salt['grains.filter_by']({
  'default': {
    'patch':  'patch' 
  },
  'Gentoo': {
    'patch':  'sys-devel/patch'
  }
}, grain='os_family', merge = salt['pillar.get']('patch:lookup'), base='default') %}
{{patch['patch']}}:
  pkg.installed
