{% set ccache = 'ccache' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set ccache = 'dev-util/' + ccache %}
{% endif %}
ccache:
  pkg.installed:
    - pkgs:
      - {{ccache}}
