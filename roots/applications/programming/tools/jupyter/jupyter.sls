jupyter:
  pkg.installed:
    - pkgs:
      - jupyter
      - jupyter-notebook
      - ipython
