{% set cloc = salt['grains.filter_by']({
  'default': {
    'cloc':         'cloc',
  },
  'Gentoo': {
    'cloc': 'dev-util/cloc'
  }
}, grain='os_family', merge = salt['pillar.get']('cloc:lookup'), base='default') %}
{{cloc['cloc']}}:
  pkg.installed
