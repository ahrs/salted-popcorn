{% set pkg_config = salt['grains.filter_by']({
  'default': {
    'pkg-config': 'pkg-config',
  },
  'Alpine': {
    'pkg-config': 'pkgconf'
  },
  'Arch': {
    'pkg-config': 'pkgconf'
  },
  'Gentoo': {
    'pkg-config': 'dev-util/pkgconf'
  },
  'RedHat': {
    'pkg-config': 'pkgconfig'
  }
}, grain='os_family', merge = salt['grains.filter_by']({
  'Fedora': {
    'pkg-config': 'pkgconf-pkg-config'
  }
}, grain='os', merge=salt['pillar.get']('pkg_config:lookup')), base='default') %}

{% if grains['os_family'] == 'Gentoo' %}
# Gentoo uses pkg-config by default
# I want to use pkg-conf!
dev-util/pkg-config:
  pkg.removed
{{pkg_config['pkg-config']}}:
  portage_config.flags:
    - use:
      - pkg-config
      - abi_x86_32
  pkg.installed:
    - pkgs:
      - {{pkg_config['pkg-config']}}
{% else %}
{{pkg_config['pkg-config']}}:
  pkg.installed
{% endif %}
