{% if grains['os_family'] == 'Gentoo' %}
unkeyword_shellcheck:
  portage_config.flags:
    - name: dev-util/shellcheck
    - accept_keywords:
      - ~*
dev-util/shellcheck:
  pkg.installed
{% else %}
shellcheck:
  pkg.installed
{% endif %}
