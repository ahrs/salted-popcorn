{% set snap_install = pillar.get('kubectl_install_snap') or grains['os'] == 'Ubuntu' %}
{% if snap_install == True %}
kubectl:
  cmd.run:
    - name: |
        set -e
        snap install --classic --color=never --unicode=never kubectl
        snap refresh --color=never --unicode=never kubectl
{% else %}
kubectl_ensure_curl:
  pkg.installed:
    - pkgs:
      - curl
# https://kubernetes.io/docs/tasks/tools/install-kubectl
kubectl:
  cmd.run:
    - name: |
        set -e
        curl -s -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
        chmod -v +x kubectl
        mkdir -vp /opt/bin
        mv -v kubectl /opt/bin/kubectl
{% endif %}
