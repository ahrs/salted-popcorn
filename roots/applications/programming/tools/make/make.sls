{% set make = 'make' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set make = 'sys-devel/' + make %}
{% endif %}
{{make}}:
  pkg.installed
