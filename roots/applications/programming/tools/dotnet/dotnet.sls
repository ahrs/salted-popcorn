{% set dotnet_version = salt['pillar.get']('dotnet_version') %}
{% set dotnet_channel = salt['pillar.get']('dotnet_channel') %}
{% set dotnet_install_hash = salt['pillar.get']('dotnet_install_hash') %}
{% if not dotnet_install_hash %}
{% set dotnet_install_hash = '6f2d7fbca76e7746f22ecaf4e1cf70918ac68946d1b8ae12fbb7a685466b6c40' %}
{% endif %}
/tmp/dotnet-install.sh:
  file.managed:
    - source:  https://dot.net/v1/dotnet-install.sh
    - source_hash: {{dotnet_install_hash}}
dotnet:
  cmd.run:
    - name: |
        set -e
        args="--install-dir /opt/dotnet "
        dotnet_channel="{{dotnet_channel}}"
        dotnet_version="{{dotnet_version}}"
        if [ -n "$dotnet_version" ]
        then
          args="${args} --version $dotnet_version"
        fi
        if [ -n "$dotnet_channel" ] && [ -z "$dotnet_version" ]
        then
          args="${args} --channel $dotnet_channel"
        fi
        cd /tmp
        mkdir -p /opt/dotnet
        chown -R nobody:nobody /opt/dotnet
        chmod +x /tmp/dotnet-install.sh
        sudo -u nobody -- /usr/bin/env -i /tmp/dotnet-install.sh $args
        chown -R root:root /opt/dotnet
