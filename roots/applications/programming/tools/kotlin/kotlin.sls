{% set snap_install = pillar.get('kotlin_install_snap') %}
{% set manual_install = pillar.get('kotlin_manual_install') %}
{% if snap_install == True or grains['os'] == 'Ubuntu' and not manual_install %}
kotlin:
  cmd.run:
    - name: |
        set -e
        snap install --classic --color=never --unicode=never kotlin
        snap refresh --color=never --unicode=never kotlin
{% else %}
{% if manual_install %}
# To Do:
#   * add manual install here...
{% else %}
kotlin:
  pkg.installed:
    - pkgs:
      - kotlin
{% endif %}
{% endif %}
