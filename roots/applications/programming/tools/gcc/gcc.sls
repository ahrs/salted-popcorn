{% set gcc = 'gcc' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gcc = 'sys-devel/' + gcc %}
{% endif %}
{{gcc}}:
  pkg.installed
