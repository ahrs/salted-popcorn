{% set doxygen = 'doxygen' %}
{% if grains['os_family'] == "Gentoo" %}
{% set doxygen = 'app-doc/' + doxygen %}
use_{{doxygen}}:
  portage_config.flags:
    - name: {{doxygen}}
    - use:
      - -clang
      - -qt5
      - doc 
      - dot
      - latex
{% endif %}
{{doxygen}}:
  pkg.installed
