{% if grains['os_family'] == 'Gentoo' and salt['pillar.get']('bootstrap', default=True) == True %}
# Helps to avoid cyclic dependencies before qt5 is installed
cmake_useflag:
  portage_config.flags:
    - name: dev-util/cmake
    - use:
      - "-qt5"
# Avoid vim pulling in Xorg before it is installed
ninja_useflag:
  portage_config.flags:
    - name: dev-util/ninja
    - use:
      - "-vim-syntax"
{% endif %}
cmake:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}dev-util/{% endif %}cmake
