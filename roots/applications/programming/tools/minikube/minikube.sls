minikube_ensure_deps:
  pkg.installed:
    - pkgs:
      - curl
      - jq
{% if grains['os_family'] == 'Debian' %}
minikube:
  cmd.run:
    - name: |
        set -e
        cd /tmp
        minikube="$(curl -s https://api.github.com/repos/kubernetes/minikube/releases/latest | jq -r ".assets[] | select(.name | test(\".deb\")) | .browser_download_url" | head -1)"
        _minikube="$(basename "$minikube")"
        curl -s -L -o "$_minikube" "$minikube"
        apt-get update -qq
        apt install --yes --no-install-recommends "./$_minikube"
{% else %}
minikube:
  cmd.run:
    - name: |
        set -e
        cd /tmp
        minikube="$(curl -s https://api.github.com/repos/kubernetes/minikube/releases/latest | jq -r ".assets[] | select(.name | test(\"minikube-linux-amd64\")) | .browser_download_url" | head -1)"
        _minikube="$(basename "$minikube")"
        curl -s -L -o "$_minikube" "$minikube"
        curl -s -L -o "${_minikube}.sha256" "${minikube}.sha256"
        # This offers no security but is a good sanity check
        printf "%s\t%s\n" "$(cat ${_minikube}.sha256)" "$_minikube" | sha256sum --check
        mkdir -vp /opt/bin
        mv -v "$_minikube" $_/minikube
        chmod -v +x $_
{% endif %}
