{% from "applications/programming/tools/go/map.jinja" import go_packages with context %}

go:
  pkg.installed:
      - pkgs:
        {% for package in go_packages %}
        {% if go_packages[package] != None %}
        - {{go_packages[package]}}
        {% endif %}
        {% endfor %}
