{% if grains['os_family'] == 'Gentoo' %}
dev-vcs/git-lfs:
  portage_config.flags:
    - accept_keywords:
      - ~*
{% endif %}
git-lfs:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}dev-vcs/{% endif %}git-lfs
