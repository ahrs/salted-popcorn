git:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}dev-vcs/{% endif %}git
