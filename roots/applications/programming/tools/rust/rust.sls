{% set rustpkg = 'rust' %}
{% if grains['os_family'] == 'Gentoo' %}
{% from "common/Gentoo/map.jinja" import gentoo with context %}
{% if gentoo.ssl_lib|lower == 'libressl' %}
{% set rustpkg = 'dev-lang/' + rustpkg + '::libressl' %}
unkeyword_rustpkg:
    portage_config.flags:
        - name: dev-lang/rust
        - accept_keywords:
            - "~*"
{% else %}
{% set rustpkg = 'dev-lang/' + rustpkg %}
{% endif %}
{% endif %}
{% if grains['os_family'] == 'Debian' %}
{% set rustpkg = rustpkg + 'c' %}
{% endif %}
# pkg.installed seems to ignore the "fromrepo"
# argument on Gentoo. This may be a bug. A crude
# workaround is used instead
{% if rustpkg == 'dev-lang/rust::libressl' %}
dev-lang/rust:
    module.run:
        - name: pkg.install
        - m_name: dev-lang/rust
        - fromrepo: "libressl"
        - binhost: "try"
        - onlyif: test -z "$(salt-call --local --out=txt pkg.version dev-lang/rust 2> /dev/null)"
"=virtual/cargo-1.31.0":
    portage_config.flags:
        - accept_keywords:
            - "~*"
"=virtual/rust-1.31.0-r666":
    portage_config.flags:
        - accept_keywords:
            - "~*"
virtual/cargo:
    module.run:
        - name: pkg.install
        - m_name: virtual/cargo
        - fromrepo: "ahrs"
virtual/rust:
    module.run:
        - name: pkg.install
        - m_name: virtual/rust
        - fromrepo: "ahrs"
{% else %}
{{rustpkg}}:
    pkg.installed:
      - pkgs:
        - {{rustpkg}}
        {% if grains['os_family'] != 'Gentoo' %}
        - cargo
        {% endif %}
{% endif%}
