# To Do:
#    * Install atom extensions and themes?
{% set atom = salt['grains.filter_by']({
  'default': {
    'atom':         'atom',
    'apm':          'apm'  
  },
  'Gentoo': {
    'atom': 'app-editors/atom'
  }
}, grain='os_family', merge = salt['pillar.get']('atom:lookup'), base='default') %}
{% set snap_install = pillar.get('atom_install_snap') %}
{% if snap_install == True %}
atom:
  cmd.run:
    - name: |
        set -e
        snap install --classic --color=never --unicode=never atom
        snap refresh --color=never --unicode=never atom
{% else %}
{% if grains['os_family'] == 'Debian' %}
atom_ensure_curl:
  pkg.installed:
    - pkgs:
      - curl
atom:
  cmd.run:
    - name: |
        set -e
        dl_url="https://atom.io/download/deb"
        cd /tmp
        curl -L -o atom-amd64.deb "$dl_url" && apt install --yes --no-install-recommends "./atom-amd64.deb"
{% else %}
atom:
  pkg.installed:
    - pkgs:
      {% for package in atom %}
      {% if atom[package] != None %}
      - {{atom[package]}}
      {% endif %}
      {% endfor %}
    - install_recommends: False
{% endif %}
{% endif %}
