{% set docker_compose = salt['grains.filter_by']({
  'default': {
    'docker-compose':         'docker-compose'
  }
}, grain='os_family', merge = salt['pillar.get']('docker_compose:lookup'), base='default') %}
docker_compose:
  pkg.installed:
    - pkgs:
      - {{docker_compose['docker-compose']}}
