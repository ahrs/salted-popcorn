{% set docker = salt['grains.filter_by']({
  'default': {
    'docker':         'docker'
  },
  'Debian': {
    'docker':         'docker.io',
   },
  'Gentoo': {
    'docker':         'app-emulation/docker',
   }
}, grain='os_family', merge = salt['pillar.get']('docker:lookup'), base='default') %}
{% if grains['os_family'] == "Docker" %}
use_{{docker['docker']}}:
  portage_config.flags:
    - name: {{docker['docker']}}
    - use:
      - btrfs
{% endif %}
docker:
  pkg.installed:
    - pkgs:
      - {{docker['docker']}}
#enable_docker:
# service.enabled:
#   - name: docker
