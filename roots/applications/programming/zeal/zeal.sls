{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'app-doc/' %}
unkeyword_zeal:
  portage_config.flags:
    - name: {{gentoo_pkg}}zeal
    - accept_keywords:
      - ~*
{% endif %}

{% if grains['os'] == 'Ubuntu' %}
zeal_repo_key:
  module.run:
    - name: pkg.add_repo_key
    - keyserver: hkp://keyserver.ubuntu.com:80
    - keyid: 786285426B130F8C8C67CEF87F73D5BEFC1B6133
/etc/apt/sources.list.d/zeal.list:
  file.managed:
    - contents: |
        deb http://ppa.launchpad.net/zeal-developers/ppa/ubuntu {{grains['oscodename']}} main 
        deb-src http://ppa.launchpad.net/zeal-developers/ppa/ubuntu {{grains['oscodename']}} main
zeal_refresh_db:
  module.run:
    - name: pkg.refresh_db
{% endif %}

zeal:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}zeal
