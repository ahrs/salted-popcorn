neovim-qt:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}app-editors/{% endif %}neovim-qt
