{% set pkg_category = '' %}
{% if grains['os_family'] == 'Gentoo' %}
dasht_accept_keywords:
  portage_config.flags:
    - name: app-doc/dasht
    - accept_keywords:
      - "**"
{% set pkg_category = 'app-doc/' %}
{% endif %}
{{pkg_category}}dasht:
  pkg.installed
