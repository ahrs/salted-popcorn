{% if grains['os_family'] == "Gentoo" %}
use_app-emulation/vagrant:
  portage_config.flags:
    - name: app-emulation/vagrant
    - use:
      - -virtualbox
{% endif %}
vagrant:
  pkg.installed:
    - pkgs:
      {% if grains['os_family'] == 'Gentoo' %}
      - app-emulation/vagrant
      {% else %}
      - vagrant
      - vagrant-libvirt
      {% endif %}
      {% if grains['os_family'] == 'Debian' %}
      - vagrant-cachier
      - vagrant-digitalocean
      - vagrant-lxc
      - vagrant-mutate
      - vagrant-sshfs
      {% endif %}
