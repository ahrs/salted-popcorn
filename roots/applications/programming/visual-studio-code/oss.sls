{% if grains['os_family'] == 'Debian' %}

{% else %}
visual-studio-code-oss:
  pkg.installed:
    - pkgs:
      {% if grains['os_family'] == 'Gentoo' %}
      - app-editors/vscode
      {% else %}
      - visual-studio-code-oss
      {% endif %}
{% endif %}
