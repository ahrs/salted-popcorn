desmume:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}games-emulation/{% endif %}desmume
