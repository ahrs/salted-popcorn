{% if grains['os_family'] == 'Void' %}
include:
  - common.Void.void-repo-nonfree
{% endif %}

{% if grains['os_family'] == 'Debian' %}
steam_ensure_wget:
  pkg.installed:
    - pkgs:
      - wget
steam:
  cmd.run:
    - name: |
        set -e
        cd /tmp
        wget -q https://steamcdn-a.akamaihd.net/client/installer/steam.deb
        apt-get update -qq
        apt install --yes --no-install-recommends ./steam.deb
{% elif grains['os_family'] == "Gentoo" %}
include:
  - common.Gentoo.local_overlay
steam-overlay:
  module.run:
    - name: eselect.exec_action
    - module: repository
    - action: enable
    - action_parameter: steam-overlay
    - require:
      - sls: common.Gentoo.local_overlay
  cmd.run:
    - name: emerge --sync steam-overlay
{% elif grains['os_family'] == 'Arch' %}
steam_enable_multilib_repo:
  cmd.run:
    - name: |
        set -e -x
        sed '/#\[multilib\]$/{
          N
          s/#\[multilib\]\n#Include/\[multilib\]\nInclude/
        }' -i /etc/pacman.conf
steam_refresh_db:
  module.run:
    - name: pkg.refresh_db
{% endif %}
{% if grains['os_family'] != 'Debian' %}
steam:
  pkg.installed:
    - pkgs:
      {% if grains['os_family'] == "Arch" %}
      - steam-native-runtime
      {% endif %}
      {% if grains['os_family'] == "Gentoo" %}
      - games-util/steam-launcher
      {% else %}
      - steam
      {% endif %}
    {% if grains['os_family'] == 'Void' %}
    - require:
      - sls: common.Void.void-repo-nonfree
    {% endif %}
{% endif %}
