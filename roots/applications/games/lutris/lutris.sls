{% if grains['os_family'] == 'Debian' %}
lutris_ensure_wget:
  pkg.installed:
    - pkgs:
      - wget
      - lsb-release
{% if grains['os'] == 'Debian' %}
lutris_repo:
  cmd.run:
    - name: |
        set -e
        echo "deb http://download.opensuse.org/repositories/home:/strycore/Debian_9.0/ ./" | tee /etc/apt/sources.list.d/lutris.list
        wget -q http://download.opensuse.org/repositories/home:/strycore/Debian_9.0/Release.key -O- | apt-key add -
{% else %}
lutris_repo:
  cmd.run:
    - name: |
        set -e
        ver=$(lsb_release -sr)
        if [ $ver != "18.04" -a $ver != "16.04" ]
        then
          ver=18.04
        fi
        echo "deb http://download.opensuse.org/repositories/home:/strycore/xUbuntu_$ver/ ./" | tee /etc/apt/sources.list.d/lutris.list
        wget -q http://download.opensuse.org/repositories/home:/strycore/xUbuntu_$ver/Release.key -O- | apt-key add -
{% endif %}
lutris:
  cmd.run:
    - name: |
        apt-get update -qq
        apt-get install --yes --no-install-recommends lutris
{% else %}
lutris:
  pkg.installed:
    - pkgs:
      - lutris
{% endif %}
