{% set wine = 'wine' %}
{% if grains['os_family'] == 'Void' %}
{% set wine = wine + '-32bit' %}
include:
  - common.Void.void-repo-multilib
{% endif %}
{% if grains['os_family'] == 'Gentoo' %}
{% set wine = 'app-emulation/' + wine %}
{% endif %}
wine:
  pkg.installed:
    - pkgs:
      - {{wine}}
    {% if grains['os_family'] == 'Void' %}
    - require:
      - sls: common.Void.void-repo-multilib
    {% endif %}
