{% if grains['os_family'] == 'Gentoo' %}
dolphin-emu:
  pkg.installed:
    - pkgs:
      - games-emulation/dolphin
{% else %}
dolphin-emu:
  pkg.installed:
    - pkgs:
      - dolphin-emu
{% endif %}
