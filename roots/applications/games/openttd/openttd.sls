{% if grains['os_family'] == 'Gentoo' %}
games-simulation/openttd:
  pkg.installed
{% else %}
openttd:
  pkg.installed:
    - pkgs:
      - openttd
      {% if grains['os_family'] == 'Void' %}
      - openttd-data
      {% else %}
      - openttd-opengfx
      - openttd-opensfx
      {% endif %}
{% endif %}
