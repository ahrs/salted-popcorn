# To Do:
  # * Check machine architecture to get 32-bit package when appropriate
  # * Add support for other supported distros e.g Fedora, OpenSuse
# https://www.teamviewer.com/en/download/linux/
{% if grains['os_family'] == 'Debian' %}
teamviewer_ensure_curl:
  pkg.installed:
    - pkgs:
      - curl
teamviewer:
  cmd.run:
    - name: |
        set -e
        dl_url="https://download.teamviewer.com/download/linux/teamviewer_amd64.deb"
        cd /tmp
        curl -L -o "$(basename "$dl_url")" "$dl_url" && apt install --yes --no-install-recommends "./$(basename "$dl_url")"
{% else %}
teamviewer:
  pkg.installed:
    - pkgs:
      - teamviewer
{% endif %}
