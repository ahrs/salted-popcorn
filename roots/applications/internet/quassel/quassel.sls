{% if grains['os_family'] == 'Gentoo' %}
quassel_useflags:
  portage_config.flags:
    - name: net-irc/quassel
    - use:
      - X
      - server
quassel:
  pkg.installed:
    - pkgs:
      - net-irc/quassel
{% else %}
quassel:
  pkg.installed:
    - pkgs:
      - quassel
{% endif %}
