{% if grains['os_family'] == 'Gentoo' %}
quassel-core_useflags:
  portage_config.flags:
    - name: net-irc/quassel
    - use:
      - -X
      - server
      - -monolithic
      - -kde
      - -dbus
quassel-core:
  pkg.installed:
    - pkgs:
      - net-irc/quassel
{% else %}
quassel-core:
  pkg.installed:
    - pkgs:
      - quassel-core
{% endif %}
