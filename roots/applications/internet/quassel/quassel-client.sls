{% if grains['os_family'] == 'Gentoo' %}
quassel-client_useflags:
  portage_config.flags:
    - name: net-irc/quassel
    - use:
      - -server
quassel-client:
  pkg.installed:
    - pkgs:
      - net-irc/quassel
{% else %}
quassel-client:
  pkg.installed:
    - pkgs:
      - quassel-client
{% endif %}
