{% if grains['os'] == 'Ubuntu' %}
opera:
  cmd.run:
    - name: |
        snap install --color=never --unicode=never opera
        snap refresh --color=never --unicode=never opera
{% else %}
opera:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}www-client/{% endif %}opera
{% endif %}
