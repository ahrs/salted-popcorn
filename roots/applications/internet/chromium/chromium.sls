{% from "applications/internet/chromium/map.jinja" import chromium with context %}

{% set desktop_type = salt['pillar.get']('desktop_type', default=None) %}
include:
  - dummy
  {% if desktop_type == 'kde' %}
  - applications.utilities.kdialog.kdialog
  {% endif %}

{% if grains['os_family'] == 'Gentoo' %}
{% from "common/Gentoo/map.jinja" import gentoo with context %}

{% if gentoo.ssl_lib|lower == 'libressl' %}
# Set the atom below one major higher than the latest
# version we want to install from the libressl overlay
chromium_net-libs/nodejs_mask_gentoo_tree_nodejs:
  portage_config.flags:
    - name: ">=net-libs/nodejs-10.16.0"
    - mask: True
{% for pkg in [
  '<net-dns/c-ares-1.16.0',
  '<dev-libs/libuv-1.25.0',
  '<net-libs/nghttp2-1.37.0'
] %}
nodejs_unkeyword_{{pkg}}:
  portage_config.flags:
    - name: {{pkg}}
    - accept_keywords:
      - "~*"
{% endfor %}
{% endif %}
chromium_net-libs/nodejs:
    portage_config.flags:
      - name: net-libs/nodejs
      - use:
        {% if gentoo.ssl_lib|lower == 'libressl' %}
        - bundled-ssl
        {% endif %}
        - icu
        - inspector
        - ssl
        - npm
    module.run:
        - name: pkg.install
        - m_name: net-libs/nodejs
        {% if gentoo.ssl_lib|lower == 'libressl' %}
        - fromrepo: "libressl"
        {% endif %}
        - binhost: "try"
        - onlyif: test -z "$(salt-call --local --out=txt pkg.version net-libs/nodejs 2> /dev/null)"

chromium_dev-libs/libxml2:
  portage_config.flags:
    - name: dev-libs/libxml2
    - use:
      - icu

chromium_media-libs/harfbuzz:
  portage_config.flags:
    - name: media-libs/harfbuzz
    - use:
      - icu

chromium_sys-libs/zlib:
  portage_config.flags:
    - name: sys-libs/zlib
    - use:
      - minizip
{% endif %}

chromium:
  pkg.installed:
    - pkgs:
      # chromium is chromium on Debian
      # and chromium-browser on Ubuntu
      {% if grains['os'] == 'Debian' %}
      - chromium
      {% else %}
      - {{chromium}}
      {% endif %}

{% for extension in salt['pillar.get']('chromium:extensions', default=[]) %}
/usr/share/chromium/extensions/{{pillar['chromium']['extensions'][extension]}}.json:
  file.managed:
    - makedirs: True
    - contents: |
        {
            "external_update_url": "https://clients2.google.com/service/update2/crx"
        }
{% endfor %}
