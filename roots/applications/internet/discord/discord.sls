{% if grains['os_family'] == 'Debian' %}
discord:
  pkg.installed:
    - sources:
      - discord: "https://discordapp.com/api/download/stable?platform=linux&format=deb"
{% elif grains['os_family'] == 'Gentoo' %}
discord:
  pkg.installed:
    - pkgs:
      - net-im/discord-bin
{% elif grains['os_family'] == 'Arch' %}
{% from "common/Arch/aur_install.jinja" import yay with context %}
include:
  - applications.utilities.yay.yay
discord:
{{ yay(['discord']) }}
{% else %}
discord_deps:
  pkg.installed:
    - pkgs:
      - libatomic
      - libcxx
clean_discord:
  file.directory:
    - name: /tmp/discord
    - force: True
    - clean: True
discord-latest:
  file.managed:
    - name: /tmp/discord/discord-latest.tar.gz
    - source:
      - "https://discordapp.com/api/download/stable?platform=linux&format=tar.gz"
    - skip_verify:
      - True
    - require:
      - file: clean_discord
discord:
  cmd.run:
    - name: |
        set -x
        cd /tmp/discord || exit 1
        [ -d /opt/Discord ] && {
          rm -rf /opt/Discord.bak
          mv /opt/Discord /opt/Discord.bak || exit 1
        }
        tar zxf discord-latest.tar.gz -C /opt || {
          rm -rf /opt/Discord
          mv /opt/Discord.bak /opt/Discord
          exit 1
        }
        cp -f /opt/Discord/discord.png /usr/share/pixmaps
        cp -f /opt/Discord/discord.desktop /usr/share/applications
        sed -i 's|^Exec=.*|Exec=/opt/Discord/Discord|g' /usr/share/applications/discord.desktop
        rm -rf /opt/Discord.bak
        ln -fs /opt/Discord/Discord /usr/bin/discord
        chmod +x /opt/Discord/Discord
    - require:
      - file: discord-latest
{% endif %}
