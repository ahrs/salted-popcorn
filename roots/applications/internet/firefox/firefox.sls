{% set firefox = salt['grains.filter_by']({
  'default': {
    'firefox':  'firefox',
  },
  'Gentoo': {
    'firefox':  'www-client/firefox'
  }
}, grain='os_family', merge = salt['pillar.get']('firefox:lookup'), base='default') %}

firefox:
  pkg.installed:
    - pkgs:
      - {{firefox['firefox']}}
#firefox-nightly:
#  pkg.installed:
#    - pkgs:
#      - firefox-nightly
#firefox-developer:
#  pkg.installed:
#    - pkgs:
#      - firefox-developer
