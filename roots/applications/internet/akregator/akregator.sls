{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'kde-apps/' %}
akregator_qtwebchannel:
  portage_config.flags:
    - name: dev-qt/qtwebchannel
    - use:
      - qml
akregator_qtsql:
  portage_config.flags:
    - name: dev-qt/qtsql
    - use:
      - mysql
akregator_qtwebengine:
  portage_config.flags:
    - name: dev-qt/qtwebengine
    - use:
      - widgets
akregator_libxml2:
  portage_config.flags:
    - name: dev-libs/libxml2
    - use:
      - icu
akregator_zlib:
  portage_config.flags:
    - name: sys-libs/zlib
    - use:
      - minizip
{% endif %}
akregator:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}akregator
    - install_recommends: False

