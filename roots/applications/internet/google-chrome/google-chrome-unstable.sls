{% from "./google-chrome_common.jinja" import install_google_chrome with context %}

{% set google_chrome = 'google-chrome-unstable' %}
{% if grains['os_family'] == 'Arch' %}
{% set google_chrome = 'google-chrome-dev' %}
{% endif %}

{{install_google_chrome(google_chrome=google_chrome, google_chrome_prefix='/opt/google/chrome-unstable', google_chrome_deburl='https://dl.google.com/linux/direct/google-chrome-unstable_current_amd64.deb')}}
