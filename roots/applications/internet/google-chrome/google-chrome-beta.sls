{% from "./google-chrome_common.jinja" import install_google_chrome with context %}
{{install_google_chrome(google_chrome='google-chrome-beta', google_chrome_prefix='/opt/google/chrome-beta', google_chrome_deburl='https://dl.google.com/linux/direct/google-chrome-beta_current_amd64.deb')}}
