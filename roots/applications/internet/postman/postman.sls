{% if grains['os_family'] == 'Arch' %}
{% from "common/Arch/aur_install.jinja" import yay with context %}
include:
  - applications.utilities.yay.yay
postman:
{{ yay(['postman-bin']) }}
{% elif grains['os_family'] == 'Debian' and grains['os'] != 'Debian' %}
include:
  - applications.utilities.snapd.snapd
postman:
  cmd.run:
    - name: |
        snap install --color=never --unicode=never postman
        snap refresh --color=never --unicode=never postman
    - require:
      - sls: applications.utilities.snapd.snapd
{% else %}
clean_postman:
  file.directory:
    - name: /tmp/postman
    - force: True
    - clean: True
postman-latest:
  file.managed:
    - name: /tmp/postman/postman-latest.tar.gz
    - source:
      - "https://dl.pstmn.io/download/latest/linux64"
    - skip_verify:
      - True
    - require:
      - file: clean_postman
postman:
  cmd.run:
    - name: |
        set -x
        cd /tmp/postman || exit 1
        [ -d /opt/Postman ] && {
          rm -rf /opt/Postman.bak
          mv /opt/Postman /opt/Postman.bak || exit 1
        }
        tar zxf postman-latest.tar.gz -C /opt || {
          rm -rf /opt/Postman
          mv /opt/Postman.bak /opt/Postman
          exit 1
        }
        cp -f /opt/Postman/app/resources/app/assets/icon.png /usr/share/pixmaps/postman.png
        cat <<EOF > /usr/share/applications/postman.desktop
        [Desktop Entry]
        Name=Postman
        StartupWMClass=postman
        Exec=/opt/Postman/Postman %U
        Icon=postman
        Type=Application
        Categories=Network;Development;Monitor;
        MimeType=x-scheme-handler/postman
        EOF
        rm -rf /opt/Postman.bak
        ln -fs /opt/Postman/Postman /usr/bin/postman
        chmod +x /opt/Postman/Postman
    - require:
      - file: postman-latest
{% endif %}
