{% if grains['os_family'] == 'Debian' or grains['os_family'] == 'RedHat' %}
brave_browser_ensure_curl:
  pkg.installed:
    - pkgs:
      - curl
brave_browser_ensure_jq:
  pkg.installed:
    - pkgs:
      - jq
brave_browser:
  cmd.run:
    - name: |
        set -e
        cd /tmp
        if ! pkg="$(curl -s https://api.github.com/repos/brave/brave-browser/releases/latest | jq -r ".assets[] | select(.name | test(\"{% if grains['os_family'] == 'Debian' %}_amd64.deb{% else %}.x86_64.rpm{% endif %}\")) | .browser_download_url")"
        then
          exit 1
        fi
        curl -L -o "$(basename "$pkg")" "$pkg" && {% if grains['os_family'] == 'Debian' %}apt install --yes --no-install-recommends{% else %}dnf install -y {% endif %} "./$(basename "$pkg")"
{% else %}
brave_browser:
  pkg.installed:
    - pkgs:
      - brave-browser
{% endif %}
