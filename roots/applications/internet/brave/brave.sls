{% if grains['os_family'] == 'Debian' %}
brave_ensure_curl:
  pkg.installed:
    - pkgs:
      - curl
brave_ensure_jq:
  pkg.installed:
    - pkgs:
      - jq
brave:
  cmd.run:
    - name: |
        set -e
        cd /tmp
        if ! deb="$(curl -s https://api.github.com/repos/brave/browser-laptop/releases/latest | jq -r ".assets[] | select(.name | test(\"_amd64.deb\")) | .browser_download_url")"
        then
          exit 1
        fi
        curl -L -o "$(basename "$deb")" "$deb" && apt install --yes --no-install-recommends "./$(basename "$deb")"
{% else %}
brave:
  pkg.installed:
    - pkgs:
      - brave
{% endif %}
