{% set tor_browser = salt['grains.filter_by']({
  'default': {
    'tor-browser': 'tor-browser-en'
  },
  'Debian': {
    'tor-browser': 'torbrowser-launcher'
  }
}, grain='os_family', merge = salt['pillar.get']('tor_browser:lookup'),  base='default') %}
tor-browser:
  pkg.installed:
    - pkgs:
      {% for package in tor_browser %}
      {% if tor_browser[package] != None %}
      - {{tor_browser[package]}}
      {% endif %}
      {% endfor %}
