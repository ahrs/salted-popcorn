kdenlive:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}kde-apps/{% endif %}kdenlive
    - install_recommends: False
