{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'media-video/' %}
unkeyword_gnome-mpv:
  portage_config.flags:
    - name: media-video/gnome-mpv
    - accept_keywords:
      - ~*
gnome-mpv_mpv-useflags:
  portage_config.flags:
    - name: media-video/mpv
    - use:
      - libmpv
{% endif %}
include:
  - applications.video.mpv.mpv
gnome-mpv:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}gnome-mpv
