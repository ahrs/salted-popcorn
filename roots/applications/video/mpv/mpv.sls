include:
  {% if grains['os_family'] == 'RedHat' %}
  - common
  {% endif %}
  - applications.programming.tools.git.git
  - applications.programming.tools.gcc.gcc
  - applications.programming.tools.make.make
  - applications.programming.tools.pkg-config.pkg-config
{% if grains['os_family'] == 'Gentoo' %}
media-video/mpv:
  portage_config.flags:
    - use:
      - archive
      - cplugins
      - cuda
      - doc
      - drm
      - gbm
      - javascript
      - libmpv
      - openal
      - rubberband
      - -samba
      - -sdl
      - tools
      - v4l
      - vaapi
      - vdpau
      - vulkan
{% endif %}
mpv_ensure_deps:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}app-admin/{% endif %}sudo
      {% if grains['os_family'] == 'Debian' %}
      - build-essential
      - libmpv-dev
      - libglib2.0-dev
      {% endif %}
      {% if grains['os_family'] == 'Gentoo' %}
      - dev-libs/glib
      {% endif %}
      {% if grains['os_family'] == 'Void' %}
      - libglib-devel
      - mpv-devel
      {% endif %}
      {% if grains['os_family'] == 'Alpine' %}
      - glib-dev
      - musl-dev
      {% endif %}
      {% if grains['os_family'] == 'RedHat' %}
      - glib-devel
      - glib2-devel
      #- libmpv-devel
      - mpv-libs-devel
      {% endif %}
    - install_recommends: False
mpv:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}media-video/{% endif %}mpv
    {% if grains['os_family'] == 'RedHat' and grains['os'] == 'Fedora' %}
    - onlyif: 'dnf repolist --enabled  | grep -E "^\*?rpmfusion"'
    {% endif %}
# MPRIS plugin for mpv: https://github.com/hoyon/mpv-mpris
mpv-mpris_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/mpv-mpris
mpv-mpris_clone_mirror:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/mpv-mpris
    - user: nobody
    - url: http://192.168.0.11:10080/ahrs/mpv-mpris.git
    - opts: --depth 1
    - failhard: False
mpv-mpris_clone_upstream:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/mpv-mpris
    - user: nobody
    - url: https://github.com/hoyon/mpv-mpris.git
    - opts: --depth 1
    - onfail:
      - module: mpv-mpris_clone_mirror
mpv-mpris_build:
  cmd.run:
    - name: |
        set -e
        cd /tmp/mpv-mpris
        /usr/bin/env -i sudo -u nobody -- make
    - require:
      - sls: applications.programming.tools.git.git
      - sls: applications.programming.tools.gcc.gcc
      - sls: applications.programming.tools.make.make
      - sls: applications.programming.tools.pkg-config.pkg-config

{% for user in pillar['users'] %}
{% if user == 'root' or user == 'nobody' %}
{% continue %}
{% endif %}
mpv-mpris_{{user}}:
  cmd.run:
    - name: |
        set -e
        cd /tmp/mpv-mpris
        mkdir -p  ~{{user}}/.config/mpv/scripts
        chown -R {{user}}:{{user}} ~{{user}}/.config || true
        chown -R {{user}}:{{user}} ~{{user}}/.config/mpv || true
        chown -R {{user}}:{{user}} ~{{user}}/.config/mpv/scripts || true
        /usr/bin/env -i sudo -u nobody -- make
        /usr/bin/env -i sudo -u {{user}} -- install -d -Dm755 ~{{user}}/.config/mpv/scripts
        /usr/bin/env -i sudo -u {{user}} -- install -Dm755 /tmp/mpv-mpris/mpris.so ~{{user}}/.config/mpv/scripts/mpris.so
{% endfor %}
