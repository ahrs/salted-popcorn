{% set cpuarch = salt['grains.get']('cpuarch', default='x86_64') %}
{% set obs = 'obs-studio' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set obs = 'media-video/' + obs %}
{% elif grains['os_family'] == 'Void' %}
{% if cpuarch == 'i386' %}
{% set obs = 'obs-32bit' %}
{% else %}
{% set obs = 'obs' %}
{% endif %}
{% endif %}
obs-studio:
  pkg.installed:
    - pkgs:
      - {{obs}}
    - install_recommends: False
