handbrake:
  pkg.installed:
    - pkgs:
      - handbrake
      - handbrake-cli
