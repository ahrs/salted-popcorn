{% if grains['os_family'] == "Gentoo" %}
include:
  - applications.programming.tools.git.git
  - common.Gentoo.local_overlay
# Use the live ebuild on Gentoo so we always
# have the latest version fresh from git master
use_youtube-dl:
  portage_config.flags:
    - name: net-misc/youtube-dl
    - use:
      - offensive
      - "PYTHON_TARGETS: -python2_7 -python3_4 -python3_5 -python3_6 python3_7"
    - accept_keywords:
      - "**"
{% endif %}

youtube-dl:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}net-misc/{% endif %}youtube-dl
    {% if grains['os_family'] == "Gentoo" %}
    - require:
      - sls: applications.programming.tools.git.git
    {% endif %}
