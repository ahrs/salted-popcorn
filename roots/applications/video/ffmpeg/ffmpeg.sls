{% from "gpus.jinja" import video_cards with context %}

{% set ffmpeg = 'ffmpeg' %}
{% if grains['os_family'] == "Gentoo" %}
{% set ffmpeg = 'media-video/' + ffmpeg %}
{% if 'nvidia' in video_cards %}
# kernel sources need to be prepared
include:
  - common.Gentoo.kernel
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=x11-libs/libXext-1.3.3-r1":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by x11-libs/libxcb-1.13.1::gentoo
# required by x11-libs/libX11-1.6.7::gentoo
# required by x11-libs/libvdpau-1.1.1::gentoo
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=x11-base/xcb-proto-1.13":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=x11-libs/libvdpau-1.1.1":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by x11-libs/libxcb-1.13.1::gentoo
# required by x11-libs/libX11-1.6.7::gentoo
# required by x11-libs/libvdpau-1.1.1::gentoo
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=dev-libs/libpthread-stubs-0.4-r1":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by x11-libs/libX11-1.6.7::gentoo
# required by x11-libs/libvdpau-1.1.1::gentoo
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=x11-libs/libxcb-1.13.1":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by x11-libs/libxcb-1.13.1::gentoo
# required by x11-libs/libX11-1.6.7::gentoo
# required by x11-libs/libvdpau-1.1.1::gentoo
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=x11-libs/libXau-1.0.8-r1":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=sys-libs/zlib-1.2.11-r2":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=x11-libs/libX11-1.6.7":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by x11-libs/libxcb-1.13.1::gentoo
# required by x11-libs/libX11-1.6.7::gentoo
# required by x11-libs/libvdpau-1.1.1::gentoo
# required by x11-drivers/nvidia-drivers-396.54::gentoo[X]
# required by x11-base/xorg-drivers-1.20::gentoo[video_cards_nvidia]
# required by x11-base/xorg-server-1.20.3::gentoo[xorg]
# required by x11-drivers/xf86-input-libinput-0.28.1::gentoo
">=x11-libs/libXdmcp-1.1.2-r2":
  portage_config.flags:
    - use:
      - abi_x86_32
# required by media-video/ffmpeg-4.0.2::gentoo[video_cards_nvidia]
# required by media-plugins/alsa-plugins-1.1.6::gentoo[-libav,ffmpeg]
# required by media-sound/pulseaudio-11.1-r1::gentoo
# required by media-libs/libsdl2-2.0.8-r2::gentoo
"=media-libs/nv-codec-headers-8.1.24.2":
  portage_config.flags:
    - accept_keywords:
      - ~*
{% endif %}
# Note: Even for libressl systems the openssl
# useflag is correct in combination with the libressl flag (set globally).
# Why both the libressl and openssl flag need to be set is a mystery
use_ffmpeg:
  portage_config.flags:
    - name: <media-video/ffmpeg-4.1.0
    - use:
      - openssl
      - vpx
      - -samba
      - -ssh
      - vdpau
      - vaapi
    - accept_keywords:
      - ~*
    - unmask: True
{% endif %}
{{ffmpeg}}:
  pkg.installed:
    - pkgs:
      - {{ffmpeg}}
    {% if  grains['os_family'] == "Gentoo" %}
    - require:
      - sls: common.Gentoo.kernel
    {% endif %}
