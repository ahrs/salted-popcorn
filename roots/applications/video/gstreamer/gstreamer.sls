{% set gstreamer = salt['grains.filter_by']({
  'default': {
    'gstreamer':         'gstreamer'
  },
  'Debian': {
    'gstreamer':         'libgstreamer1.0-0'
   }
}, grain='os_family', merge = salt['pillar.get']('gstreamer:lookup'), base='default') %}
gstreamer:
  pkg.installed:
    - pkgs:
      - {{gstreamer['gstreamer']}}
