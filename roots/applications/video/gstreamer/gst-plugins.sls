include:
  - applications.video.gstreamer.gstreamer
{% set gstplugins = salt['grains.filter_by']({
  'default': {
    'gst-plugins-bad': 'gst-plugins-bad',
    'gst-plugins-base': 'gst-plugins-base',
    'gst-plugins-base-libs': 'gst-plugins-base-libs',
    'gst-plugins-good': 'gst-plugins-good',
    'gst-plugins-ugly': 'gst-plugins-ugly'
  },
  'Debian': {
    'gst-plugins-bad': 'gstreamer1.0-plugins-bad',
    'gst-plugins-base': 'gstreamer1.0-plugins-base',
    'gst-plugins-base-libs': None,
    'gst-plugins-good': 'gstreamer1.0-plugins-good',
    'gst-plugins-ugly': 'gstreamer1.0-plugins-ugly'
   }
}, grain='os_family', merge = salt['pillar.get']('gstplugins:lookup'), base='default') %}
gst-plugins:
  pkg.installed:
    - pkgs:
      {% for package in gstplugins %}
      {% if gstplugins[package] != None %}
      - {{gstplugins[package]}}
      {% endif %}
      {% endfor %}
    - install_recommends: False
