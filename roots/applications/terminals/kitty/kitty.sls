{% set kitty_manual_install = pillar.get('kitty_manual_install') %}

{% set os = grains['os'] %}

{% if os == 'Fedora' or os == 'SUSE' %}
{% set kitty_manual_install = os %}
{% endif %}

{% if kitty_manual_install %}
include:
  - applications.utilities.stow.stow
  - applications.programming.tools.gcc.gcc
kitty_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/kitty
kitty_clone:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/kitty
    - user: nobody
    - url:  https://github.com/kovidgoyal/kitty.git
    - opts: -b v0.13.3
kitty_deps:
  pkg.installed:
    - pkgs:
      - make
      - gcc
      - git
      - python3-{% if os == 'SUSE' %}S{% else %}s{% endif %}phinx
      - python3
      - python3-devel
      - python3-setuptools
      - harfbuzz-devel
      {% if os == 'SUSE' %}
      - libpng16-devel
      {% else %}
      - libpng-devel
      {% endif %}
      - fontconfig-devel
      {% if os == 'SUSE' %}
      - Mesa-libGL-devel
      {% else %}
      - libGL-devel
      {% endif %}
      - libxkbcommon-devel
      - libXrandr-devel
      - libXinerama-devel
      - libXcursor-devel
      - libxkbcommon-x11-devel
      {% if os == 'SUSE' %}
      - dbus-1-devel
      {% else %}
      - dbus-devel
      {% endif %}
      - libXi-devel
      - wayland-protocols-devel
    - check_cmd:
      # assume dependencies are installed
      - /bin/true
kitty:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp/kitty
        /usr/bin/env -i sudo -u nobody -- make
        /usr/bin/env -i sudo -u nobody -- /bin/sh -c '[ -d /usr/libexec/python3-sphinx ] && export PATH="$PATH:/usr/libexec/python3-sphinx";make docs'
        /usr/bin/env -i sudo -u nobody -- python3 setup.py linux-package
        rm -rf /stow/kitty.tmp
        install -dm0775 -o nobody -g nobody /stow/kitty.tmp
        install -dm0775 -o nobody -g nobody /stow/kitty.tmp/usr
        if [ -d /stow/kitty ]
        then
          cd /stow/kitty
          stow --verbose=3 -t /usr -D usr
          cd -
          mv /stow/kitty /stow/kitty.bak
        fi
        cp -r linux-package/* "/stow/kitty.tmp/usr" || {
          cd /stow
          if [ -d kitty.bak ]
          then
            rm -rf kitty
            rm -rf kitty.tmp
            mv kitty.bak kitty
            cd kitty
            stow --verbose=3 -t /usr usr
          fi
          exit 1
        }
        cd /stow
        mv kitty.tmp kitty
        chown -R root:root kitty
        cd kitty
        stow --verbose=3 -t /usr usr
        sed -i 's|Icon=.*|Icon=/stow/kitty/usr/share/icons/hicolor/256x256/apps/kitty.png|g' /stow/kitty/usr/share/applications/kitty.desktop
        rm -rf /stow/kitty.bak
{% else %}

{% if grains['os_family'] == 'Gentoo' %}
"/etc/portage/package.use/x11-terms":
  file.directory
"/etc/portage/package.use/x11-terms/kitty":
  file.managed:
    - contents: "x11-terms/kitty PYTHON_SINGLE_TARGET: -python3_6 python3_7 PYTHON_TARGETS: -python2_7 -python3_4 -python3_5 -python3_6 python3_7"
    - require:
      - file: "/etc/portage/package.use/x11-terms"
"/etc/portage/package.accept_keywords/x11-terms":
  file.directory
"/etc/portage/package.accept_keywords/x11-terms/kitty":
  file.managed:
    - contents: "x11-terms/kitty ~*"
    - require:
      - file: "/etc/portage/package.accept_keywords/x11-terms"
kitty_python_useflags:
  portage_config.flags:
    - name: dev-lang/python
    - use:
      - sqlite
  cmd.run:
    - name: |
        set -e -x
        if [ -z "$(find /var/db/pkg -maxdepth 2 -wholename "/var/db/pkg/dev-lang/python-*" -type d -exec cat {}/USE \; | grep -oE 'sqlite')" ]
        then
          emerge -v1 dev-lang/python:2.7 dev-lang/python
        fi


x11-terms/kitty:
  pkg.installed

{% else %}
kitty:
  pkg.installed
{% endif %}
{% endif %}
