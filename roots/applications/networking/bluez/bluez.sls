{% set bluez = salt['grains.filter_by']({
  'default': {
    'bluez':  'bluez',
    'bluez-tools':  'bluez-tools'
  },
  'Alpine': {
    'bluez-tools': 'bluez-deprecated'
  },
  'Arch': {
    'bluez-utils':  'bluez-utils'
  },
  'Gentoo': {
    'bluez':  'net-wireless/bluez',
    'bluez-tools':  'net-wireless/bluez-tools'
  },
  'Void': {
    'bluez-tools':  None
  }
}, grain='os_family', merge = salt['pillar.get']('bluez:lookup'), base='default') %}

{% for pkg in bluez %}
{% if bluez[pkg] != None %}
{{bluez[pkg]}}:
  pkg.installed
{% endif %}
{% endfor %}
