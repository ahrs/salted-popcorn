{% if grains['os_family'] == 'Arch' %}
wireshark-cli:
  pkg.installed:
    - pkgs:
      - wireshark-cli
{% endif %}
{% if grains['os_family'] == 'Gentoo' %}
net-analyzer/wireshark:
  pkg.installed
{% else %}
wireshark-gtk:
  pkg.installed:
    - pkgs:
      - wireshark-gtk
{% endif %}
#wireshark-qt:
#  pkg.installed:
#    - pkgs:
#      - wireshark-qt
