{% set samba = 'samba' %}
{% if grains['os_family'] == "Gentoo" %}
{% set samba = 'net-fs/' + samba %}
# required by sys-libs/ldb
use_sys-libs/tdb:
  portage_config.flags:
    - name: sys-libs/tdb
    - use:
      - python
use_{{samba}}:
  portage_config.flags:
    - name: {{samba}}
    - use:
      - client
{% endif %}
{{samba}}:
  pkg.installed
