# Pretty much every distro names NeTwOrk ManAgEr differently :(
{% set network_manager = salt['grains.filter_by']({
  'default': {
    'network-manager':         'network-manager',
  },
  'Alpine': {
    'network-manager':  'networkmanager'
  },
  'Arch': {
    'network-manager':  'networkmanager'
  },
  'RedHat': {
    'network-manager':  'NetworkManager',
    'nmtui':  'NetworkManager-tui'
  },
  'Suse': {
    'network-manager':  'NetworkManager'
  },
  'Gentoo': {
    'network-manager':  'net-misc/networkmanager'
  },
  'Void': {
    'network-manager':  'NetworkManager'
  }
}, grain='os_family', merge = salt['pillar.get']('network_manager:lookup'), base='default') %}

{% if grains['os_family'] == 'Void' %}
dbus:
  pkg.installed:
    - pkgs:
      - dbus
  cmd.run:
    - name: |
        ln -fs /etc/sv/dbus /var/service
{% endif %}

network-manager:
  pkg.installed:
    - pkgs:
      {% for pkg in network_manager %}
      {% if network_manager[pkg] != None %}
      - {{network_manager[pkg]}}
      {% endif %}
      {% endfor %}
  {% if salt['grains.get']('init', default='unknown') != 'unknown' %}
  {% if grains['os_family'] == 'Void' %}
  cmd.run:
    - name: |
        ln -fs /etc/sv/NetworkManager /var/service
  {% else %}
  {% if not 'virtual_subtype' in grains %}
  {% set state = 'service.running' %}
  {% else %}
  {% set state = 'service.enabled' %}
  {% endif %}
  {{state}}:
    - name: NetworkManager
    - enable: True
  {% endif %}
  {% endif %}
