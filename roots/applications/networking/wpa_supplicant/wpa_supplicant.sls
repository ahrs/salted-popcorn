{% set wpa_supplicant = salt['grains.filter_by']({
  'default': 'wpa_supplicant',
  'Gentoo': 'net-wireless/wpa_supplicant',
  'Debian': 'wpasupplicant'
}) %}

{{wpa_supplicant}}:
  pkg.installed

{% if grains['os_family'] == 'RedHat' %}
/etc/sysconfig/wpa_supplicant:
  file.managed:
    - contents: |
        # Use the flag "-i" before each of your interfaces, like so:
        #  INTERFACES="-ieth1 -iwlan0"
        INTERFACES="{% for interface in salt['grains.get']('ip_interfaces', default={'lo': ''}) %}{% if interface[0:2] == 'wl' %}-i {{interface}} {% endif %}{% endfor %}"
        
        # Use the flag "-D" before each driver, like so:
        #  DRIVERS="-Dwext"
        DRIVERS=""

        # Other arguments
        #   -s   Use syslog logging backend
        OTHER_ARGS="-s -u -c /etc/wpa_supplicant/wpa_supplicant.conf"
    - require:
      - pkg: {{wpa_supplicant}}
{% elif grains['os_family'] == 'Gentoo' or grains['os_family'] == 'Alpine' %}
/etc/conf.d/wpa_supplicant:
  file.managed:
    - contents: |
        # conf.d file for wpa_supplicant
        
        # uncomment this if wpa_supplicant starts up before your network interface
        # is ready and it causes issues
        # rc_want="dev-settle"
        
        # Please check man 8 wpa_supplicant for more information about the options
        # wpa_supplicant accepts.
        #
        wpa_supplicant_args="{% for interface in salt['grains.get']('ip_interfaces', default={'lo': ''}) %}{% if interface[0:2] == 'wl' %}-i {{interface}} {% endif %}{% endfor %} -s -u -c /etc/wpa_supplicant/wpa_supplicant.conf"
    - require:
      - pkg: {{wpa_supplicant}}
{% endif %}

# Use systemd-networkd on systemd systems
{% if salt['grains.get']('init', default='unknown') == 'systemd' %}
{% for interface in salt['grains.get']('hwaddr_interfaces', default={'lo': ''}) %}{% if interface[0:2] == 'wl' %}
/etc/systemd/network/{{interface}}.network:
  file.managed:
    - makedirs: True
  ini.options_present:
    - separator: '='
    - strict: True
    - sections:
        Match:
          MACAddress: {{grains['hwaddr_interfaces'][interface]}}
        Network:
          DHCP: "yes"
{% endif %}
{% endfor %}
{% endif %}
