{% if pillar['applications'] is defined %}

{% set desktop_type = salt['pillar.get']('desktop_type', default=None) %}
{% set subdesktop_type = None %}

{% if desktop_type != None %}
  {% if desktop_type|lower == 'gnome' %}
    {% set subdesktop_type = 'gtk' %}
  {% endif %}

  {% if desktop_type|lower == 'kde' or desktop_type|lower == 'lxqt' %}
    {% set subdesktop_type = 'qt' %}
  {% endif %}
{% endif %}

include:
{% for state in salt['pillar.get']('applications:common', default={}) %}
    - applications.{{state}}
{% endfor %}

{% if desktop_type != None %}
{% for state in salt['pillar.get']('applications:' + desktop_type, default={}) %}
    - applications.{{state}}
{% endfor %}
{% endif %}

{% if subdesktop_type != None %}
{% for state in salt['pillar.get']('applications:' + subdesktop_type, default={}) %}
    - applications.{{state}}
{% endfor %}
{% endif %}

{% endif %}
