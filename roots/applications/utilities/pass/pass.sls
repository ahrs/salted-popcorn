{% set pass_otp_manual_install = pillar.get('pass_otp_manual_install') %}

{% set pass_packages = salt['grains.filter_by']({
  'default': {
    'pass': 'pass',
    'pass-otp': 'pass-otp',
  },
  'Alpine': {
    'pass-otp': None
  },
  'Debian': {
    'pass-otp': 'pass-extension-otp',
    'pass-extension-tail': 'pass-extension-tail'
  },
  'Gentoo': {
    'pass':     'app-admin/pass',
    'pass-otp': 'app-admin/pass-otp'
  },
  'Suse': {
    'pass':   None,
    'pass-otp': None
  }
}, grain='os_family', merge = salt['pillar.get']('pass_packages:lookup'), base='default') %}

{% if grains['os_family'] == 'Alpine' %}
{% set pass_otp_manual_install = True %}
{% endif %}

{% if pass_otp_manual_install == True %}
include:
  - applications.utilities.stow.stow
{% endif %}

{% if grains['os_family'] == 'Gentoo' %}
{% for pkg in [
    'sys-auth/oath-toolkit',
    'app-admin/pass-otp'
] %}
unkeyword_{{pkg}}:
  portage_config.flags:
    - name: {{pkg}}
    - accept_keywords:
      - ~*
{% endfor %}
{% endif %}

pass:
  pkg.installed:
    - pkgs:
      {% for pkg in pass_packages %}
      {% if pass_packages[pkg] != None %}
      - {{pass_packages[pkg]}}
      {% endif %}
      {% endfor %}

{% if grains['os_family'] == 'Alpine' %}
pass-otp_deps:
  pkg.installed:
    - pkgs:
      - oath-toolkit-oathtool
{% endif %}

{% set pass_otp_tag = 'v1.2.0' %}
{% set verify_pass_otp_repo = '/bin/sh -c "set -e;set -x;cd /tmp/pass-otp;curl -s -L https://keyserver.ubuntu.com/pks/lookup\?op\=get\&search\=0x0DDE73383977AC09 | gpg --import;git verify-tag -v ' + pass_otp_tag + ' || true;' + 'git verify-tag -v ' + pass_otp_tag + ' 2>&1 | grep -E "^gpg: Good signature from \"Tad Fisher <tadfisher@gmail.com>\" \[unknown\]\$";"' %}

{% if pass_otp_manual_install == True %}
pass-otp_clean:
  cmd.run:
    - name: rm -rf /tmp/pass-otp
pass-otp_clone_mirror:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/pass-otp
    - user: nobody
    - url:  http://192.168.0.11:10080/ahrs/pass-otp.git
    - opts: -b {{pass_otp_tag}}
    - failhard: False
pass-otp_clone_upstream:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/pass-otp
    - user: nobody
    - url:  https://github.com/tadfisher/pass-otp.git
    - opts: -b {{pass_otp_tag}}
    - failhard: False
    - onfail:
      - module: pass-otp_clone_mirror
pass-otp:
  cmd.run:
    - name: |
        set -e -x
        rm -rf /stow/pass-otp.tmp
        install -dm0775 -o nobody -g nobody /stow/pass-otp.tmp
        if [ -d /stow/pass-otp ]
        then
          cd /stow/pass-otp
          stow --verbose=3 -t /usr/local -D usr
          cd -
          mv /stow/pass-otp /stow/pass-otp.bak
        fi
        cd /tmp/pass-otp
        /usr/bin/env -i sudo -u nobody -- make DESTDIR=/stow/pass-otp.tmp install || {
          cd /stow
          if [ -d pass-otp.bak ]
          then
            rm -rf pass-otp
            rm -rf pass-otp.tmp
            mv pass-otp.bak pass-otp
            cd pass-otp
            stow --verbose=3 -t /usr usr
          fi
          exit 1
        }
        cd /stow
        mv pass-otp.tmp pass-otp
        chown -R root:root pass-otp
        cd pass-otp
        stow --verbose=3 -t /usr usr
        rm -rf /stow/pass-otp.bak
{% endif %}
