{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'app-editors/' %}
gedit_dev-libs/libpeas:
  portage_config.flags:
    - name: dev-libs/libpeas
    - use:
      - gtk
{% endif %}
gedit:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}gedit
    - install_recommends: False
