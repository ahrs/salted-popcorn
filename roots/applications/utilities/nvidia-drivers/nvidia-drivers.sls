{% set nvidia_drivers = salt['grains.filter_by']({
  'default': {
    'nvidia':	'nvidia'  
  },
  'Gentoo': {
    'nvidia': 'x11-drivers/nvidia-drivers'
  }
}, grain='os_family', merge = salt['pillar.get']('nvidia_drivers:lookup'), base='default') %}
{% if grains['os_family'] == "Gentoo" %}
use_{{nvidia_drivers['nvidia']}}:
  portage_config.flags:
    - name: {{nvidia_drivers['nvidia']}}
    - use:
      - gtk3
      - tools
      - uvm
      - X
{% endif %}
{{nvidia_drivers['nvidia']}}:
  pkg.installed
