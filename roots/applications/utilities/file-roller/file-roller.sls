file-roller:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}app-arch/{% endif %}file-roller
    - install_recommends: False
