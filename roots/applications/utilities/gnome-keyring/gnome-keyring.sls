{% set gnome_keyring = 'gnome-keyring' %}
{% if grains['os_family'] == "Gentoo" %}
{% set gnome_keyring = 'gnome-base/' + gnome_keyring %}
use_app-crypt/gcr:
  portage_config.flags:
    - name: app-crypt/gcr
    - use:
      - gtk
{% endif %}
{{gnome_keyring}}:
  pkg.installed
