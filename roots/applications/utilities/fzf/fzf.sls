{% set fzf = 'fzf' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set fzf = 'app-shells/' + fzf %}
include:
  - common.Gentoo.local_overlay
{% endif %}

# To Do:
#   * This works on Ubuntu 18.10 other Debian/Ubuntu based distros might need backports 
{% if (grains['os'] == 'Debian' and grains['oscodename'] != 'sid') or (grains['os'] == 'Ubuntu' and grains['osmajorrelease'] < 19) %}
fzf:
  cmd.run:
    - name: |
        set -e -x

        sudo='/usr/bin/env -i sudo -u nobody --'
        cd /tmp
        rm -rf fzf
        $sudo mkdir fzf
        cd fzf
        apt-get update -qq
        apt-get install --yes --no-install-recommends sudo build-essential git dpkg-dev golang ca-certificates fakeroot bash-completion dh-golang dh-exec golang-golang-x-crypto-dev golang-github-mattn-go-isatty-dev golang-github-mattn-go-runewidth-dev golang-github-mattn-go-shellwords-dev
        $sudo git clone --depth 1 https://salsa.debian.org/debian/fzf.git fzf
        cd fzf
        $sudo dpkg-buildpackage -rfakeroot -uc -b
        apt install --yes --no-install-recommends ../fzf_*.deb
        apt-get remove --purge --autoremove --yes golang-golang-x-crypto-dev golang-github-mattn-go-isatty-dev golang-github-mattn-go-runewidth-dev golang-github-mattn-go-shellwords-dev
{% elif grains['os_family']|lower != 'suse' or grains['oscodename']|lower == 'tumbleweed' %}
fzf:
  pkg.installed:
    - pkgs:
      - {{fzf}}
    - install_recommends: False
    {% if grains['os_family'] == "Gentoo" %}
    - require:
      - sls: common.Gentoo.local_overlay
    {% endif %}
{% endif %}
