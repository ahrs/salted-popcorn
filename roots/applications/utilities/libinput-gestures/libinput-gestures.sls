{% if grains['os_family'] != 'Debian' %}
libinput-gestures:
  pkg.installed:
    - pkgs:
      - libinput-gestures
{% else %}
libinput-gestures_install_deps:
  pkg.installed:
    - pkgs:
      - libinput-tools
      - xdotool
      - wmctrl
libinput-gestures_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/libinput-gestures
libinput-gestures_clone:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/libinput-gestures
    - user: nobody
    - url: https://github.com/bulletmark/libinput-gestures.git
    - opts: --depth 1
libinput-gestures:
  cmd.run:
    - name: |
        set -e
        cd /tmp/libinput-gestures
        make uninstall || true
        make install
{% endif %}
