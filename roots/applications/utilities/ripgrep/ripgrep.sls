{% set ripgrep = 'ripgrep' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set ripgrep = 'sys-apps/' + ripgrep %}
include:
  - applications.programming.tools.rust.rust
{% endif %}

{% if grains['os_family'] != 'Gentoo' %}
ripgrep_ensure_deps:
  pkg.installed:
    - pkgs:
      - curl
      - jq
{% endif %}

{% if (grains['os'] == 'Debian' and grains['oscodename'] != 'sid') or (grains['os'] == 'Ubuntu' and grains['osmajorrelease'] < 19) %}
ripgrep:
  cmd.run:
    - name: |
        set -e -x
        ripgrep="$(curl -s https://api.github.com/repos/BurntSushi/ripgrep/releases/latest | jq -r ".assets[] | select(.name | test(\".deb\")) | .browser_download_url" | head -1)"
        cd /tmp
        [ -z "$ripgrep" ] && ripgrep="https://github.com/BurntSushi/ripgrep/releases/download/0.10.0/ripgrep_0.10.0_amd64.deb"
        _ripgrep="$(basename "$ripgrep")"
        curl -s -L -o "$_ripgrep" "$ripgrep"
        apt install --yes --no-install-recommends ./"$_ripgrep"
{% elif grains['os_family'] == 'Alpine' %}
ripgrep:
  cmd.run:
    - name: |
        set -e -x
        ripgrep="$(curl -s https://api.github.com/repos/BurntSushi/ripgrep/releases/latest | jq -r ".assets[] | select(.name | test(\"{{salt['grains.get']('cpuarch', default='x86_64')}}-unknown-linux-musl.tar.gz\")) | .browser_download_url" | head -1)"
        cd /tmp
        [ -z "$ripgrep" ] && ripgrep="https://github.com/BurntSushi/ripgrep/releases/download/0.10.0/ripgrep-0.10.0-x86_64-unknown-linux-musl.tar.gz"
        _ripgrep="$(basename "$ripgrep")"
        rm -rf *ripgrep*
        curl -s -L -o "$_ripgrep" "$ripgrep"
        tar xf ./"$_ripgrep"
        install -m755 ripgrep*/rg /usr/bin/rg
        install -dm755 /usr/share/doc/ripgrep
        install -m755 ripgrep*/doc/* /usr/share/doc/ripgrep/
        if [ -f /usr/share/doc/ripgrep/rg.1 ]
        then
          install -dm755 /usr/share/man/man1
          mv /usr/share/doc/ripgrep/rg.1 /usr/share/man/man1
        fi
        for d in 'bash-completion/completions' 'fish/completion' 'zsh/vendor-completion'
        do
          install -dm755 /usr/share/"$d"
        done
        install -m644 ripgrep*/complete/_rg /usr/share/zsh/vendor-completion/_rg
        install -m644 ripgrep*/complete/rg.fish /usr/share/fish/completion/rg.fish
        install -m644 ripgrep*/complete/rg.bash /usr/share/bash-completion/completions/rg
        rm -rf ripgrep*
{% else %}
{% if grains['os_family'] == 'Gentoo' %}
unkeyword_ripgrep:
  portage_config.flags:
    - name: {{ripgrep}}
    - accept_keywords:
      - ~*
{% endif %}
{{ripgrep}}:
  pkg.installed
{% endif %}
