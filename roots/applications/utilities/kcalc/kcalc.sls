kcalc:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}kde-apps/{% endif %}kcalc
    - install_recommends: False
