{% set sc_im_manual_install = pillar.get('sc_im_manual_install') %}

{% set os = grains['os'] %}

{% if os == 'Fedora' or grains['os_family'] == 'Debian' %}
{% set sc_im_manual_install = os %}
{% endif %}

{% if sc_im_manual_install %}
include:
  - applications.utilities.stow.stow
sc-im_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/sc-im
sc-im_clone_mirror:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/sc-im
    - user: nobody
    - url:  http://192.168.0.11:10080/ahrs/sc-im.git
    - opts: --depth 1
sc-im_clone_upstream:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/sc-im
    - user: nobody
    - url:  https://github.com/andmarti1424/sc-im.git
    - opts: --depth 1
    - onfail:
      - module: sc-im_clone_mirror
sc-im_deps:
  pkg.installed:
    - pkgs:
      {% if grains['os_family'] == 'Debian' %}
      - build-essential
      {% endif %}
      - byacc
      - gnuplot
      - ncurses-dev{% if grains['os_family'] == 'RedHat' %}el{% endif %}
    - check_cmd:
      # assume dependencies are installed
      - /bin/true
sc-im:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp/sc-im/src
        /usr/bin/env -i sudo -u nobody -- make prefix="/usr"
        rm -rf /stow/sc-im.tmp
        install -dm0775 -o nobody -g nobody /stow/sc-im.tmp
        if [ -d /stow/sc-im ]
        then
          cd /stow/sc-im
          stow --verbose=3 -t /usr -D usr
          cd -
          mv /stow/sc-im /stow/sc-im.bak
        fi
        /usr/bin/env -i sudo -u nobody -- make prefix="/usr" DESTDIR=/stow/sc-im.tmp install || {
          cd /stow
          if [ -d sc-im.bak ]
          then
            rm -rf sc-im
            rm -rf sc-im.tmp
            mv sc-im.bak sc-im
            cd sc-im
            stow --verbose=3 -t /usr usr
          fi
          exit 1
        }
        cd /stow
        mv sc-im.tmp sc-im
        chown -R root:root sc-im
        cd sc-im
        stow --verbose=3 -t /usr usr
        rm -rf /stow/sc-im.bak
{% else %}

{% set scim = salt['grains.filter_by']({
  'default': {
    'scim': 'sc-im'
  },
  'Gentoo': {
    'scim': 'app-office/sc-im'
  }
}, grain='os_family', merge = salt['pillar.get']('scim:lookup'), base='default') %}

{% if grains['os_family'] == "Gentoo" %}
include:
  - applications.programming.tools.git.git
  - common.Gentoo.local_overlay
{% endif %}

{% if grains['os_family'] == 'Debian' %}
sc-im_ensure_deps:
  pkg.installed:
    - pkgs:
      - curl
      - jq
      - unzip
{% if grains['oscodename']|lower == 'disco' %}
{% set codename = 'cosmic' %}
{% else %}
{% set codename = grains['oscodename']|lower %}
{% endif %}
sc-im:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp
        rm -rf sc-im
        mkdir sc-im
        cd sc-im
        download="$(curl -s -L --header 'Accept: application/json' https://gitlab.com/ahrs/sc-im_debianpackage/pipelines/ | jq -r '(.pipelines|first).details.artifacts[]|select(.name=="package_{{grains['os']|lower}}_{{codename}}").path')"
        curl -s -L -o artifacts.zip "https://gitlab.com$download"
        unzip artifacts.zip
        cd debs
        apt install --yes --no-install-recommends ./*.deb
{% elif grains['os_family'] == 'Arch' %}
sc-im:
  cmd.run:
    - name: |
        set -e -x
        if command -v sc-im > /dev/null 2>&1
        then
          exit 0
        fi
        pacman -S --noconfirm --needed --asdeps libzip binutils gcc
        cd /tmp
        sudo='sudo -u nobody -- '
        rm -rf sc-im
        $sudo git clone https://aur.archlinux.org/sc-im.git sc-im
        cd sc-im
        $sudo makepkg
        pacman -U --noconfirm sc-im-*.pkg.tar.xz
{% else %}
{% if grains['os_family'] == 'Gentoo' %}
unkeyword_sc-im:
  portage_config.flags:
    - name: {{scim['scim']}}
    - accept_keywords:
      - ~*
reload_db:
  module.run:
    - name: pkg.refresh_db
    - require:
      - sls: common.Gentoo.local_overlay
test_sc-im_install:
  cmd.run:
    - name: |
        set -e -x
        emerge -pv app-office/sc-im
    - require:
      - sls: common.Gentoo.local_overlay
{% endif %}
sc-im:
  pkg.installed:
    - pkgs:
      - {{scim['scim']}}
    {% if grains['os_family'] == "Gentoo" %}
    - require:
      - sls: common.Gentoo.local_overlay
    {% endif %}
{% endif %}

{% endif %}
