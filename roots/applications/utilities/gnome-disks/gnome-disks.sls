{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'sys-apps/' %}
gnome-disks_pinentry:
  portage_config.flags:
    - name: app-crypt/pinentry
    - use:
      - gnome-keyring
{% endif %}
gnome-disks:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}gnome-disk-utility
    - install_recommends: False

