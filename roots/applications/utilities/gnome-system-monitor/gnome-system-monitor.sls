gnome-system-monitor:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}gnome-extra/{% endif %}gnome-system-monitor
    - install_recommends: False
