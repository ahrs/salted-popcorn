{% if grains['os_family'] == 'Debian' %}
insync_add_repo_key:
  module.run:
    - name: pkg.add_repo_key
    - keyserver: hkp://keyserver.ubuntu.com:80
    - keyid: ACCAF35C
/etc/apt/sources.list.d/insync.list:
  file.managed:
    - contents: "deb http://apt.insynchq.com/{{grains['os'] | lower}} {{grains['oscodename']}} non-free contrib"
insync_refresh_repos:
  module.run:
    - name: pkg.refresh_db
{% endif %}
insync:
  pkg.installed
