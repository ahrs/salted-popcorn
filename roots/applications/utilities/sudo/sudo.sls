{% set sudo = 'sudo' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set sudo = 'app-admin/' + sudo %}
{% endif %}
{{sudo}}:
  pkg.installed
