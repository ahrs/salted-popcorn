{% set kdialog = 'kdialog' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set kdialog = 'kde-apps/' + kdialog %}
{% endif %}

{{kdialog}}:
  pkg.installed
