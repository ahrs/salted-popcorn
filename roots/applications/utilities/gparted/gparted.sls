{% set gparted = salt['grains.filter_by']({
  'default': {
    'gparted':  'gparted',
    'gpart':  'gpart'  
  },
  'Gentoo': {
    'gparted':  'sys-block/gparted',
    'gpart':  'sys-block/gpart'
  }
}, grain='os_family', merge = salt['pillar.get']('gparted:lookup'), base='default') %}

gparted:
  pkg.installed:
    - pkgs:
      - {{gparted['gparted']}}
      - {{gparted['gpart']}}
