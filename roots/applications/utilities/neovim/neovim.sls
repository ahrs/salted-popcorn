{% if grains['os_family'] == 'Gentoo' %}
{% set unstable_packages = [
    'dev-libs/msgpack',
    'app-editors/neovim',
    'dev-python/neovim-python-client',
    'dev-ruby/msgpack',
    'dev-ruby/neovim-ruby-client',
    'dev-python/neovim-remote'
] %}
{% for pkg in unstable_packages %}
unkeyword_{{pkg}}:
  portage_config.flags:
    - name: {{pkg}}
    - accept_keywords:
      - ~*
    {% if pkg == 'app-editors/neovim' %}
    - use:
      - python
      - ruby
      - remote
    {% endif %}
{% endfor %}
{% set luajit_packages = [
    'dev-lua/lpeg',
    'dev-lua/mpack'
] %}
{% for pkg in luajit_packages %}
{{pkg}}:
  portage_config.flags:
    - use:
      - luajit
{% endfor %}
app-editors/neovim:
  pkg.installed
dev-python/neovim-python-client:
  pkg.installed
dev-ruby/neovim-ruby-client:
  pkg.installed
dev-python/neovim-remote:
  pkg.installed
{% else %}
neovim:
  pkg.installed:
    - pkgs:
      - neovim
python-neovim:
  pkg.installed:
    - pkgs:
      {% if grains['os_family'] == 'Debian' or grains['os_family'] == 'RedHat' %}
      - python3-neovim
      {% else %}
      - python-neovim
      {% endif %}
{% if grains['os_family'] != 'RedHat' and grains['os_family'] != 'Arch' %}
lua-neovim:
  pkg.installed:
    - pkgs:
      - lua-nvim
ruby-neovim:
  pkg.installed
{% elif grains['os_family'] == 'Arch' %}
{% from "common/Arch/aur_install.jinja" import yay with context %}
include:
  - applications.utilities.yay.yay
ruby-neovim:
{{ yay(['ruby-neovim']) }}
{% endif %}
{% endif %}
