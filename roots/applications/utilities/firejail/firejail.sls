{% if grains['os_family'] == 'Gentoo' %}
unkeyword_firejail:
  portage_config.flags:
    - name: sys-apps/firejail
    - accept_keywords:
      - ~*
sys-apps/firejail:
  pkg.installed
{% else %}
firejail:
  pkg.installed:
    - pkgs:
      - firejail
{% endif %}
