{% set kdeconnect = salt['grains.filter_by']({
  'default': {
    'kdeconnect': 'kdeconnect',
  },
  'Gentoo': {
    'kdeconnect': 'app-misc/kdeconnect'
  },
  'RedHat': {
    'kdeconnect': 'kde-connect',
  }
}, grain='os_family', merge = salt['pillar.get']('kdeconnect:lookup'), base='default') %}



{{kdeconnect['kdeconnect']}}:
  pkg.installed:
    - install_recommends: False

