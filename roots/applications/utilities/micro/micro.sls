{% set snap_install = pillar.get('micro_install_snap') or grains['os'] == 'Ubuntu' %}
{% if snap_install == True %}
micro:
  cmd.run:
    - name: |
        set -e
        snap install --classic --color=never --unicode=never micro
        snap refresh --color=never --unicode=never micro
{% else %}
micro:
  pkg.installed
{% endif %}
