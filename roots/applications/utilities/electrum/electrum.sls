{% set snap_install = pillar.get('electrum_install_snap') %}
{% if snap_install == True or grains['os'] == 'Ubuntu' %}
electrum:
  cmd.run:
    - name: |
        set -e
        snap install --color=never --unicode=never electrum
        snap refresh --color=never --unicode=never electrum
{% else %}
{% if grains['os_family'] == 'Gentoo' %}net-misc/{% endif %}electrum:
  pkg.installed
{% endif %}
