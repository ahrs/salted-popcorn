kate:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}kde-apps/{% endif %}kate{% if grains['os_family'] == 'Void' %}5{% endif %}
    - install_recommends: False
