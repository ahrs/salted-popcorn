remmina:
  {% if grains['os'] == 'Ubuntu' %}
  cmd.run:
    - name: |
        snap install --color=never --unicode=never remmina
        snap refresh --color=never --unicode=never remmina
  {% else %}
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}net-misc/{% endif %}remmina
  {% endif %}
