ksysguard:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}kde-plasma/{% endif %}ksysguard
    - install_recommends: False
