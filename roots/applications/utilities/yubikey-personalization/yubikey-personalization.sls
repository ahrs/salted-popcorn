{% set yubikey = salt['grains.filter_by']({
  'default': {
    'yubikey-personalization':  'yubikey-personalization',
    'yubikey-personalization-gui':  'yubikey-personalization-gui'  
  },
  'Gentoo': {
    'yubikey-personalization':  'app-crypt/yubikey-manager',
    'yubikey-personalization-gui':  'sys-auth/yubikey-personalization-gui'
  }
}, grain='os_family', merge = salt['pillar.get']('yubikey:lookup'), base='default') %}

yubikey-personalization:
  pkg.installed:
    - pkgs:
      - {{yubikey['yubikey-personalization']}}
yubikey-personalization-gui:
  pkg.installed:
    - pkgs:
      - {{yubikey['yubikey-personalization-gui']}}
