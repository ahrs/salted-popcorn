dconf-editor:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}gnome-base/{% endif %}dconf-editor
    - install_recommends: False
