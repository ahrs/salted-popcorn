# To Do:
#   * pre-built releases from https://github.com/sharkdp/bat/releases
#   for when building from source is undesireable
{% set bat_manual_install = pillar.get('bat_manual_install') %}

{% set os = grains['os'] %}

{% if grains['os_family'] == 'Debian' or os == 'Alpine' or os == 'SUSE' %}
{% set bat_manual_install = True %}
{% endif %}

include:
  - dummy
{% if grains['os_family'] == 'Gentoo' %}
  - common.Gentoo.local_overlay
{% endif %}
{% if bat_manual_install %}
  - applications.utilities.stow.stow
  - applications.programming.tools.git.git
  - applications.programming.tools.rust.rust
bat_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/bat
bat_mirror:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/bat
    - user: nobody
    - url:  http://192.168.0.11:10080/ahrs/bat.git
    - opts: --depth 1
    - failhard: False
bat_clone_upstream:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/bat
    - user: nobody
    - url:  https://github.com/sharkdp/bat.git
    - onfail:
      - module: bat_mirror
bat:
  cmd.run:
    - name: |
        set -e -x
        _HOME="$(mktemp -d)"
        mkdir -p "$_HOME/.cache"
        chown -R nobody:nobody "$_HOME"
        cd /tmp/bat
        /usr/bin/env -i sudo -u nobody -- env LC_ALL=C.UTF-8 HOME="$_HOME" cargo build --release
        rm -rf /stow/bat.tmp
        install -dm0775 -o nobody -g nobody /stow/bat.tmp
        install -D -dm0775 -o nobody -g nobody /stow/bat.tmp/usr/local
        install -D -dm0775 -o nobody -g nobody /stow/bat.tmp/usr/local/share/man/man1
        if [ -d /stow/bat ]
        then
          cd /stow/bat/usr
          stow --verbose=3 -t /usr/local -D local
          cd -
          mv /stow/bat /stow/bat.bak
        fi
        /usr/bin/env -i sudo -u nobody -- env LC_ALL=C.UTF-8 HOME="$_HOME" cargo install --bins --examples --root /stow/bat.tmp/usr/local --path . || {
          cd /stow
          if [ -d bat.bak ]
          then
            rm -rf bat
            rm -rf bat.tmp
            mv bat.bak bat
            cd bat/usr
            stow --verbose=3 -t /usr/local local
          fi
          exit 1
        }
        install -m0644 doc/bat.1 /stow/bat.tmp/usr/local/share/man/man1/bat.1
        cd /stow
        mv bat.tmp bat
        chown -R root:root bat
        cd bat/usr
        rm -f local/.crates.toml
        stow --verbose=3 -t /usr/local local
        rm -rf /stow/bat.bak
{% elif grains['os_family'] == 'Gentoo' %}
unkeyword_bat:
  portage_config.flags:
    - name: app-misc/bat
    - accept_keywords:
      - ~*
app-misc/bat:
  pkg.installed:
    - require:
      - sls: common.Gentoo.local_overlay
{% else %}
bat:
  pkg.installed:
    - pkgs:
      - bat
{% endif %}
