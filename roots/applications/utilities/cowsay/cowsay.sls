cowsay:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}games-misc/{% endif %}cowsay
