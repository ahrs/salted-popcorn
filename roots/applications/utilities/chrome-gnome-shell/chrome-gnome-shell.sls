chrome-gnome-shell:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}gnome-extra/{% endif %}chrome-gnome-shell
    - install_recommends: False
