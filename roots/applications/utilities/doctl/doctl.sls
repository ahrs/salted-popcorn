{% set snap_install = pillar.get('doctl_install_snap') %}
{% if snap_install == True or grains['os'] == 'Ubuntu' %}
doctl:
  cmd.run:
    - name: |
        set -e
        snap install --color=never --unicode=never doctl
        snap refresh --color=never --unicode=never doctl
{% else %}
doctl:
  pkg.installed:
    - pkgs:
      - doctl-bin
{% endif %}
