{% if grains['os_family'] == "Gentoo" %}
include:
  - common.Gentoo.local_overlay
"/etc/portage/package.use/app-misc/ranger":
  file.absent
app-misc/ranger:
  portage_config.flags:
    - use:
      - kitty
      - "PYTHON_TARGETS: -* python3_7"
    - accept_keywords:
      - "**"
    - require:
      - file: "/etc/portage/package.use/app-misc/ranger"
"<dev-python/pillow-5.4.0":
  portage_config.flags:
    - accept_keywords:
      - ~*
# required by dev-python/pillow-5.3.0::gentoo
# required by app-misc/ranger-9999::ahrs[kitty]
# required by ranger (argument)
"<dev-python/olefile-0.47":
  portage_config.flags:
    - accept_keywords:
      - ~*
{% endif %}
ranger:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == "Gentoo" %}app-misc/{% endif %}ranger
    - install_recommends: False
    {% if grains['os_family'] == "Gentoo" %}
    - require:
      - sls: common.Gentoo.local_overlay
      - portage_config: app-misc/ranger
    {% endif %}
