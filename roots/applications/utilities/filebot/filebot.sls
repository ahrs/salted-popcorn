{% set snap_install = pillar.get('filebot_install_snap') %}
{% if snap_install == True or grains['os'] == 'Ubuntu' %}
filebot:
  cmd.run:
    - name: |
        set -e
        snap install --color=never --unicode=never filebot
        snap refresh --color=never --unicode=never filebot
{% else %}
{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'media-video/' %}
unkeyword_filebot:
  portage_config.flags:
    - name: {{gentoo_pkg}}filebot
    - accept_keywords:
      - ~*
{% endif %}
filebot:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}filebot
{% endif %}
