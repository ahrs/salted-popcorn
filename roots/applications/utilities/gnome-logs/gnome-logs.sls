gnome-logs:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}gnome-extra/{% endif %}gnome-logs
    - install_recommends: False
