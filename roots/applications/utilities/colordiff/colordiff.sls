colordiff:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}app-misc/{% endif %}colordiff
