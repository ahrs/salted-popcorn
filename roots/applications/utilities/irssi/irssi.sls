irssi:
  pkg.installed:
    - pkgs:
      - irssi
{% if grains['os_family'] == 'Arch' %}
irssi-scripts:
  pkg.installed:
    - pkgs:
      - irssi-scripts-git
{% endif %}
{% if grains['os_family'] == 'Debian' %}
irssi-scripts:
  pkg.installed
{% endif %}
