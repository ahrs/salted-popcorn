alacarte:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}x11-misc/{% endif %}alacarte
    - install_recommends: False
