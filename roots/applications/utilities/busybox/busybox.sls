{% set busybox = salt['grains.filter_by']({
  'default': {
    'busybox':  'busybox',
  },
  'Debian': {
    'busybox':  'busybox-static'
  },
  'Gentoo': {
    'busybox':  'sys-apps/busybox'
  }
}, grain='os_family', merge = salt['pillar.get']('busybox:lookup'), base='default') %}
{{busybox['busybox']}}:
  pkg.installed
