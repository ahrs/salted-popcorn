partitionmanager:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}sys-block/{% endif %}partitionmanager
