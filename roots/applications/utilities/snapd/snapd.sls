{% if grains['os_family'] == 'Debian' %}
snapd:
  pkg.installed
{% endif %}

{% if grains['os_family'] == 'Arch' %}
{% from "common/Arch/aur_install.jinja" import yay with context %}
include:
  - applications.utilities.yay.yay
snapd:
{{ yay(['snapd']) }}
{% endif %}
