{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'gnome-extra/' %}
gnome-calculator_x11-libs/gtksourceview:
  portage_config.flags:
    - name: x11-libs/gtksourceview
    - use:
      - vala
{% endif %}
gnome-calculator:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}gnome-calculator
    - install_recommends: False
