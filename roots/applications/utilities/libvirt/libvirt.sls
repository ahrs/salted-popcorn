{% set libvirt = salt['grains.filter_by']({
  'default': {
    'libvirt':  'libvirt'
  },
  'Debian': {
    'libvirt':  'libvirt0'
  },
  'Gentoo': {
    'libvirt': 'app-emulation/libvirt'
  }
}, grain='os_family', merge = salt['pillar.get']('libvirt:lookup'), base='default') %}
{% if grains['os_family'] == "Gentoo" %}
use_{{libvirt['libvirt']}}:
  portage_config.flags:
    - name: {{libvirt['libvirt']}}
    - use:
      - -virtualbox
{% endif %}
{{libvirt['libvirt']}}:
  pkg.installed
