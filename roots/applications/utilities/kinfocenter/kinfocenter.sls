{% set kinfocenter = 'kinfocenter' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set kinfocenter = 'kde-plasma/' + kinfocenter %}
{% endif %}

{{kinfocenter}}:
  pkg.installed
