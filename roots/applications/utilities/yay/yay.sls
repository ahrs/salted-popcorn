{% if grains['os_family'] == 'Arch' %}
include:
  - applications.programming.tools.go.go
yay:
  cmd.run:
    - name: |
        set -e -x
        
        if command -v yay > /dev/null 2>&1
        then
          exit 0
        fi

        pacman -S --needed --noconfirm --asdeps binutils gcc fakeroot
        cd /tmp
        sudo='sudo -u nobody -- '
        rm -rf yay
        $sudo git clone https://aur.archlinux.org/yay.git yay
        cd yay
        _HOME="$(mktemp -d)"
        mkdir -p "$_HOME/.cache"
        chown -R nobody:nobody "$_HOME"
        $sudo env HOME="$_HOME" XDG_CACHE_HOME="$_HOME/.cache" makepkg
        pacman -U --noconfirm yay-*.pkg.tar.xz
        rm -rf "$_HOME"
    - check_cmd:
      - test -x /usr/bin/yay
    - require:
      - sls: applications.programming.tools.go.go
{% endif %}
