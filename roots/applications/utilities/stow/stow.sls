{% set stow = 'stow' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set stow = 'app-admin/' + stow %}
unkeyword_stow:
  portage_config.flags:
    - name: {{stow}}
    - accept_keywords:
      - ~*
{% endif %}
{{stow}}:
  pkg.installed
