baobab:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}sys-apps/{% endif %}baobab
    - install_recommends: False
