pandoc:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}app-text/{% endif %}pandoc
