{% from "applications/utilities/hunspell/map.jinja" import hunspell_packages with context %}

{% for pkg in hunspell_packages %}
{% if hunspell_packages[pkg] != None %}
{{hunspell_packages[pkg]}}:
  pkg.installed
{% endif %}
{% endfor %}
