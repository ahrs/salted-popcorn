{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'gnome-base/' %}
nautilus_dev-lang/python-use_sqlite:
  portage_config.flags:
    - name: dev-lang/python
    - use:
      - sqlite
{% endif %}
nautilus:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}nautilus
    - install_recommends: False
