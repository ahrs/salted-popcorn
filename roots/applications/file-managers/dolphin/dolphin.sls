{% set dolphin = 'dolphin' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set dolphin = 'kde-apps/' + dolphin %}
dolphin_sys-libs/zlib:
  portage_config.flags:
    - name: sys-libs/zlib
    - use:
      - minizip
{% endif %}
dolphin:
  pkg.installed:
    - pkgs:
      - {{dolphin}}
    - install_recommends: False
