{% set cpuarch = salt['grains.get']('cpuarch', default='x86_64') %}
{% set thunar = 'thunar' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set thunar = 'xfce-base/' + thunar %}
{% elif grains['os_family'] == 'Void' %}
{% if cpuarch == 'i386' %}
{% set thunar = 'Thunar-32bit' %}
{% else %}
{% set thunar = 'Thunar' %}
{% endif %}
{% endif %}
thunar:
  pkg.installed:
    - pkgs:
      - {{thunar}}
    - install_recommends: False
