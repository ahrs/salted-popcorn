{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'x11-misc/' %}
pcmanfm_libfm_use_gtk:
  portage_config.flags:
    - name: x11-libs/libfm
    - use:
      - gtk
{% endif %}
pcmanfm:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}pcmanfm
    - install_recommends: False
