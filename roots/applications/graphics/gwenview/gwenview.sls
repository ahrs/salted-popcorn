{% set gwenview = 'gwenview' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gwenview = 'kde-apps/' + gwenview %}
gwenview_sys-libs/zlib:
  portage_config.flags:
    - name: sys-libs/zlib
    - use:
      - minizip
{% endif %}
gwenview:
  pkg.installed:
    - pkgs:
      - {{gwenview}}
    - install_recommends: False

