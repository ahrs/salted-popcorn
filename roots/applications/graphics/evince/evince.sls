evince:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}app-text/{% endif %}evince
    - install_recommends: False

