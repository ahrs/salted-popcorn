{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'kde-apps/' %}
okular_media-gfx/exiv2:
  portage_config.flags:
    - name: media-gfx/exiv2
    - use:
      - xmp
okular_sys-libs/zlib:
  portage_config.flags:
    - name: sys-libs/zlib
    - use:
      - minizip
{% endif %}
okular:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}okular
