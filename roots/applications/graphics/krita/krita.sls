krita:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}media-gfx/{% endif %}krita
