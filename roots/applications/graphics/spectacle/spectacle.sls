{% set spectacle = salt['grains.filter_by']({
  'default': {
    'spectacle':         'spectacle'
  },
  'Debian': {
    'spectacle':  'kde-spectacle'
  },
  'Gentoo': {
    'spectacle':  'kde-apps/spectacle'
  }
}, grain='os_family', merge = salt['pillar.get']('spectacle:lookup'), base='default') %}

spectacle:
  pkg.installed:
    - pkgs:
      - {{spectacle['spectacle']}}
