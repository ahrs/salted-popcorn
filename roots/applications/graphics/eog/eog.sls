{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'media-gfx/' %}
eog_dev-libs/libpeas:
  portage_config.flags:
    - name: dev-libs/libpeas
    - use:
      - gtk
{% endif %}
eog:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}eog
    - install_recommends: False
