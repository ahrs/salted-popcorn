gnome-screenshot:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}media-gfx/{% endif %}gnome-screenshot
    - install_recommends: False

