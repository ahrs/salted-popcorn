ardour:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}media-sound/{% endif %}ardour
    - install_recommends: False
