pavucontrol:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}media-sound/{% endif %}pavucontrol
    - install_recommends: False
