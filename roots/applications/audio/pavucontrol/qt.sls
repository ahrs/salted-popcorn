{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set gentoo_pkg = 'media-sound/' %}
{% for pkg in [
  'media-sound/pavucontrol-qt',
  'dev-util/lxqt-build-tools'
] %}
unkeyword_{{pkg}}:
  portage_config.flags:
    - name: {{pkg}}
    - accept_keywords:
      - ~*
{% endfor %}
{% endif %}
pavucontrol-qt:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}pavucontrol-qt
    - install_recommends: False
