{% set gentoo_pkg = '' %}
include:
  - applications.audio.pulseaudio.pulseaudio
{% if grains['os_family'] == 'Gentoo' %}
  - applications.video.ffmpeg.ffmpeg
{% set gentoo_pkg = 'media-sound/' %}
perl_useflags:
  portage_config.flags:
    - name: dev-lang/perl
    - use:
      - ithreads
  cmd.run:
    - name: |
        set -e -x
        if [ -z "$(find /var/db/pkg -maxdepth 2 -wholename "/var/db/pkg/dev-lang/perl-*" -type d -exec cat {}/USE \; | grep -oE 'ithreads')" ]
        then
          emerge -v1 dev-lang/perl
          perl-cleaner --modules -- --usepkg=n --getbinpkg=n --with-bdeps=y
          perl-cleaner --force --libperl -- --usepkg=n --getbinpkg=n --with-bdeps=y
        fi
cantata_useflags:
  portage_config.flags:
    - name: {{gentoo_pkg}}cantata
    - use:
      - cdda
      - cddb
      - http-server
      - mtp
      - musicbrainz
      - replaygain
      - taglib
      - udisks
      - zeroconf
{% endif %}
cantata:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}cantata
    - install_recommends: False
    - require:
      - sls: applications.audio.pulseaudio.pulseaudio
    {% if grains['os_family'] == "Gentoo" %}
      - sls: applications.video.ffmpeg.ffmpeg
    {% endif %}

