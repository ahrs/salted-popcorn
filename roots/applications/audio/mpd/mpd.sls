{% from "firewall/map.jinja" import firewall with context %}

{% set gentoo_pkg = '' %}
{% if grains['os_family'] == 'Gentoo' %}
include:
  - applications.video.ffmpeg.ffmpeg
{% set gentoo_pkg = 'media-sound/' %}
mpd_useflags:
  portage_config.flags:
    - name: {{gentoo_pkg}}mpd
    - use:
      - expat
      - ffmpeg
      - opus
      - pipe
      - recorder
      - samba
      - soundcloud
      - sqlite
      - webdav
      - zip
{% endif %}
mpd:
  pkg.installed:
    - pkgs:
      - {{gentoo_pkg}}mpd
    - install_recommends: False
    {% if grains['os_family'] == "Gentoo" %}
    - require:
      - sls: applications.video.ffmpeg.ffmpeg
    {% endif %}
  {% if firewall.firewall == 'firewalld' %}
  firewalld.service:
    - name: mpd
    - ports:
      - 6600/tcp
  {% endif %}
/etc/mpd.conf:
  file.managed:
    - source:
      - salt://applications/audio/mpd/mpd.conf
{% if firewall.firewall == 'firewalld' %}
{% if firewall['zone'] != None %}
mpd_firewalld_{{firewall['zone']}}:
  firewalld.present:
    - name: {{firewall['zone']}}
    - services:
      - mpd
    - prune_services: False
{% endif %}
{% elif firewall.firewall == 'ufw' %}
mpd_ufw:
  cmd.run:
    - name: ufw allow proto tcp from any to any port 6600
{% endif %}
