{% set playerctl_version = '2.0.1' %}
{% set playerctl_manual_install = pillar.get('playerctl_manual_install') %}

{% set os = grains['os'] %}

{% if os == 'Fedora' or os == 'Alpine' %}
{% set playerctl_manual_install = True %}
{% endif %}

{% if playerctl_manual_install %}
include:
  - applications.utilities.stow.stow
playerctl_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/playerctl
playerctl_clone:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/playerctl
    - user: nobody
    - url:  https://github.com/acrisci/playerctl.git
    - opts: -b v{{playerctl_version}}
playerctl_deps:
  pkg.installed:
    - pkgs:
      - meson
    - check_cmd:
      # assume dependencies are installed
      - /bin/true
playerctl:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp/playerctl
        /usr/bin/env -i sudo -u nobody -- env PREFIX=/usr/local meson --prefix=/usr/local --libdir=/usr/local/lib mesonbuild -Dintrospection=false -Dgtk-doc=false
        rm -rf /stow/playerctl.tmp
        install -dm0775 -o nobody -g nobody /stow/playerctl.tmp
        if [ -d /stow/playerctl ]
        then
          cd /stow/playerctl
          stow --verbose=3 -t /usr -D usr
          cd -
          mv /stow/playerctl /stow/playerctl.bak
        fi
        /usr/bin/env -i sudo -u nobody -- env HOME="$(pwd)" DESTDIR=$(pwd)/install ninja -C mesonbuild install
        cp -r install/usr /stow/playerctl.tmp || {
          cd /stow
          if [ -d playerctl.bak ]
          then
            rm -rf playerctl
            rm -rf playerctl.tmp
            mv playerctl.bak playerctl
            cd playerctl
            stow --verbose=3 -t /usr usr
          fi
          exit 1
        }
        cd /stow
        mv playerctl.tmp playerctl
        chown -R root:root playerctl
        cd playerctl
        stow --verbose=3 -t /usr usr
        rm -rf /stow/playerctl.bak
{% else %}
playerctl:
  pkg.installed:
    {% if grains['os_family'] == 'RedHat' and grains['cpuarch'] == 'x86_64' %}
    - sources:
      - playerctl: https://github.com/acrisci/playerctl/releases/download/v{{playerctl_version}}/playerctl-{{playerctl_version}}_x86_64.rpm
    {% elif grains['os_family'] == 'Debian' and grains['cpuarch'] == 'x86_64' %}
    - sources:
      - playerctl: https://github.com/acrisci/playerctl/releases/download/v{{playerctl_version}}/playerctl-{{playerctl_version}}_amd64.deb
    {% else %}
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}media-sound/{% endif %}playerctl
    {% endif %}
{% endif %}
