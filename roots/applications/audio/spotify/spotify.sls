include:
  - applications.audio.pulseaudio.pulseaudio
{% if grains['os_family'] == 'Void' %}
  - common.Void.void-repo-nonfree
{% endif %}
{% if grains['os_family'] == 'Debian' and grains['os'] != 'Debian' %}
  - applications.utilities.snapd.snapd
{% endif %}

{% if grains['os_family'] != 'Debian' %}
spotify:
  pkg.installed:
    - pkgs:
      - {% if grains['os_family'] == 'Gentoo' %}media-sound/{% endif %}spotify
    - require:
      - sls: applications.audio.pulseaudio.pulseaudio
    {% if grains['os_family'] == 'Void' %}
      - sls: common.Void.void-repo-nonfree
    {% endif %}
{% else %}
# https://www.spotify.com/uk/download/linux/
{% if grains['os'] == 'Debian' %}
spotify_addrepo:
  cmd.run:
    - name: |
        apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
        echo deb http://repository.spotify.com stable non-free | tee /etc/apt/sources.list.d/spotify.list
spotify_refreshrepo:
  module.run:
    - name: pkg.refresh_db
spotify:
  pkg.installed:
    - pkgs:
      - spotify-client
    - require:
      - sls: applications.audio.pulseaudio.pulseaudio
{% else %}
spotify:
  cmd.run:
    - name: |
        snap install --color=never --unicode=never spotify
        snap refresh --color=never --unicode=never spotify
    - require:
      - sls: applications.audio.pulseaudio.pulseaudio
      - sls: applications.utilities.snapd.snapd
{% endif %}
{% endif %}
