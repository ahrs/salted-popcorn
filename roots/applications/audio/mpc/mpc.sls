{% set mpc = salt['grains.filter_by']({
  'default': {
    'mpc':  'mpc',
  },
  'Gentoo': {
    'mpc':  'media-sound/mpc'
  },
  'Suse': {
    'mpc':  'mpclient'
  }
}, grain='os_family', merge = salt['pillar.get']('mpc:lookup'), base='default') %}

mpc:
  pkg.installed:
    - pkgs:
      - {{mpc['mpc']}}
