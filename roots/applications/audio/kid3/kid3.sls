{% if grains['os_family'] == 'Gentoo' %}
include:
  - applications.audio.kid3.kid3-qt
{% else %}
kid3:
  pkg.installed:
    - pkgs:
      - kid3
{% endif %}
