kid3-qt:
  pkg.installed:
    - pkgs:
      {% if grains['os_family'] == 'Gentoo' %}
      - media-sound/kid3
      {% else %}
      - kid3-qt
      {% endif %}
