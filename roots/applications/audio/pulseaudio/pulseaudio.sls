{% from "firewall/map.jinja" import firewall with context %}

include:
  - applications.networking.bluez.bluez

{% set pulseaudio_packages = salt['grains.filter_by']({
  'default': {
      'bluez-utils': 'bluez-utils',
      'libpulse': 'libpulse',
      'pulseaudio-alsa': 'pulseaudio-alsa',
      'pulseaudio-bluetooth': 'pulseaudio-bluetooth',
      'pulseaudio': 'pulseaudio'
  },
  'Debian': {
      'bluez-utils': 'bluez-tools',
      'libpulse': None,
      'pulseaudio-alsa': None,
      'pulseaudio-bluetooth': 'pulseaudio-module-bluetooth'
  },
  'Alpine': {
      'alsa-plugins': 'alsa-plugins',
      'alsa-plugins-pulse': 'alsa-plugins-pulse',
      'bluez-utils': 'bluez',
      'libpulse': 'pulseaudio-libs',
      'pulseaudio-alsa': 'pulseaudio-alsa',
      'pulseaudio-bluetooth': 'pulseaudio-bluez',
      'pulseaudio-utils': 'pulseaudio-utils'
  },
  'RedHat': {
      'libpulse': 'pulseaudio-libs',
      'bluez-utils': 'bluez',
      'pulseaudio-bluetooth': 'pulseaudio-module-bluetooth',
      'pulseaudio-alsa':  'alsa-plugins-pulseaudio',
      'eulseaudio-module-x11': 'pulseaudio-module-x11',
      'pulseaudio-utils': 'pulseaudio-utils'
  },
  'Gentoo': {
      'pulseaudio':     'media-sound/pulseaudio',
  },
  'Void': {
      'libpulse': 'libpulseaudio',
      'bluez-utils': 'bluez',
      'pulseaudio-bluetooth': 'pulseaudio',
      'pulseaudio-alsa':  'alsa-plugins-pulseaudio'
  }
}, grain='os_family', merge = salt['pillar.get']('pulseaudio_packages:lookup'), base='default') %}
pulseaudio:
  pkg.installed:
    - pkgs:
      {% for pkg in pulseaudio_packages %}
      {% if pulseaudio_packages[pkg] != None %}
      - {{pulseaudio_packages[pkg]}}
      {% endif %}
      {% endfor %}

# Allow all connections from localhost and 192.168.0.11 without a password
/etc/pulse/default.pa:
  file.append:
    - text: |
        load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1;192.168.0.11;
    - require:
      - pkg: pulseaudio

{% if firewall.firewall == 'firewalld' %}
{% if firewall['zone'] != None %}
pulseaudio_firewalld_{{firewall['zone']}}:
  firewalld.present:
    - name: {{firewall['zone']}}
    - services:
      - pulseaudio
    - prune_services: False
    - onlyif:
      - test -e /run/dbus/system_bus_socket
{% endif %}
{% elif firewall.firewall == 'ufw' %}
pulseaudio_ufw:
  cmd.run:
    - name: ufw allow proto tcp from any to any port 4713
{% endif %}
