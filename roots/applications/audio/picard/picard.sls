{% set picard = 'picard' %}
{% if grains['os_family'] == 'Gentoo' %}
{% set picard = 'media-sound/' + picard %}
{% endif %}

{% if grains['os'] == 'Ubuntu' %}
{% set codename = grains['oscodename']|lower %}
picard_repo_key:
  module.run:
    - name: pkg.add_repo_key
    - keyserver: hkp://keyserver.ubuntu.com:80
    - keyid: 0CC3AFF5CEDF0F40
/etc/apt/sources.list.d/picard.list:
  file.managed:
    - contents: |
        deb http://ppa.launchpad.net/musicbrainz-developers/stable/ubuntu {{codename}} main 
        deb-src http://ppa.launchpad.net/musicbrainz-developers/stable/ubuntu {{codename}} main
picard_refresh_db:
  module.run:
    - name: pkg.refresh_db
{% endif %}

{{picard}}:
  pkg.installed
