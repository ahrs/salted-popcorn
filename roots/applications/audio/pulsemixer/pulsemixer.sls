{% set pulsemixer_manual_install = pillar.get('pulsemixer_manual_install') %}

{% set os = grains['os'] %}

{% if os == 'Fedora' or os == 'Alpine' or os == 'SUSE' %}
{% set pulsemixer_manual_install = True %}
{% endif %}

include:
  - applications.audio.pulseaudio.pulseaudio
{% if grains['os_family'] == 'Gentoo' %}
  - common.Gentoo.local_overlay
{% endif %}
{% if pulsemixer_manual_install %}
  - applications.utilities.stow.stow
pulsemixer_clean:
  cmd.run:
    - name: |
        set -e
        rm -rf /tmp/pulsemixer
pulsemixer_mirror:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/pulsemixer
    - user: nobody
    - url:  http://192.168.0.11:10080/ahrs/pulsemixer.git
    - opts: --depth 1
    - failhard: False
pulsemixer_clone_upstream:
  module.run:
    - name: git.clone
    - cwd: /tmp
    - m_name: /tmp/pulsemixer
    - user: nobody
    - url:  https://github.com/GeorgeFilipkin/pulsemixer.git
    - onfail:
      - module: pulsemixer_mirror
pulsemixer_deps:
  pkg.installed:
    - pkgs:
      - python3-setuptools
    - check_cmd:
      # assume dependencies are installed
      - /bin/true
pulsemixer:
  cmd.run:
    - name: |
        set -e -x
        cd /tmp/pulsemixer
        /usr/bin/env -i sudo -u nobody -- env LC_ALL=C.UTF-8 python3 setup.py build
        rm -rf /stow/pulsemixer.tmp
        install -dm0775 -o nobody -g nobody /stow/pulsemixer.tmp
        if [ -d /stow/pulsemixer ]
        then
          cd /stow/pulsemixer
          stow --verbose=3 -t /usr/local -D usr
          cd -
          mv /stow/pulsemixer /stow/pulsemixer.bak
        fi
        /usr/bin/env -i sudo -u nobody -- env LC_ALL=C.UTF-8 python3 setup.py install --root /stow/pulsemixer.tmp --prefix /usr || {
          cd /stow
          if [ -d pulsemixer.bak ]
          then
            rm -rf pulsemixer
            rm -rf pulsemixer.tmp
            mv pulsemixer.bak pulsemixer
            cd pulsemixer
            stow --verbose=3 -t /usr/local usr
          fi
          exit 1
        }
        cd /stow
        mv pulsemixer.tmp pulsemixer
        chown -R root:root pulsemixer
        cd pulsemixer
        stow --verbose=3 -t /usr/local usr
        rm -rf /stow/pulsemixer.bak
{% elif grains['os_family'] == 'Gentoo' %}
alsa-plugins_use_pulseaudio:
  portage_config.flags:
    - name: media-plugins/alsa-plugins
    - use:
      - pulseaudio
unkeyword_pulsemixer:
  portage_config.flags:
    - name: media-sound/pulsemixer
    - accept_keywords:
      - ~*
media-sound/pulsemixer:
  pkg.installed:
    - require:
      - sls: common.Gentoo.local_overlay
      - sls: applications.audio.pulseaudio.pulseaudio
{% else %}
pulsemixer:
  pkg.installed:
    - pkgs:
      - pulsemixer
    - require:
      - sls: applications.audio.pulseaudio.pulseaudio
{% endif %}
