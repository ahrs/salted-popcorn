{% set ncmpcpp = salt['grains.filter_by']({
  'default': {
    'ncmpcpp': 'ncmpcpp',
  },
  'Gentoo': {
    'ncmpcpp': 'media-sound/ncmpcpp'
  }
}, grain='os_family', merge = salt['pillar.get']('ncmpcpp:lookup'), base='default') %}

{% if grains['os_family'] == 'Gentoo' %}
ncmpcpp_boost:
  portage_config.flags:
    - name: dev-libs/boost
    - use:
      - icu
{% endif %}

ncmpcpp:
  pkg.installed:
    - pkgs:
      - {{ncmpcpp['ncmpcpp']}}

