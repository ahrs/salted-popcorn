/etc/xdg:
  file.directory
/etc/xdg/mimeapps.list:
    file.absent
default_applications:
    file.append:
        - name: /etc/xdg/mimeapps.list
        - text: |
            [Default Applications]
        
{% for mimetype in pillar['default_applications'] %}
{{ mimetype }}:
    file.append:
        - name: /etc/xdg/mimeapps.list
        - text: 
            - {{ mimetype }}={% for desktop in pillar['default_applications'][mimetype]%}{{ desktop }};{% endfor %}
{% endfor %}
