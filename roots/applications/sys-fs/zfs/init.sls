{% if grains['os_family'] == 'Debian' %}
{% if grains['os'] == 'Ubuntu' %}
zfsutils-linux:
  pkg.installed
{% else %}
zfs:
  pkg.installed:
    - pkgs:
      - zfs-dkms
      - zfsutils-linux
{% endif %}
{% elif grains['os_family'] == 'RedHat' %}
{% if grains['os'] == 'Fedora' %}
# the repo signing key is listed on the zfsonlinux wiki page here:
# https://github.com/zfsonlinux/zfs/wiki/Fedora
{% set zfsonlinux_signing_key = 'C93AFFFD9F3F7B03C310CEB6A9D5A1C0F14AB620' %}
zfsonlinux:
  cmd.run:
    - name: |
        set -e -x
        dnf install -y http://download.zfsonlinux.org/fedora/zfs-release$(rpm -E %dist).noarch.rpm
        gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-zfsonlinux
    - onlyif:
      - "! test -f /etc/yum.repos.d/zfs.repo"
    - check_cmd:
      - "test -f /etc/yum.repos.d/zfs.repo"
      - test "$(gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-zfsonlinux | grep -E '^\s+Key fingerprint = ' | sed -e 's|Key fingerprint =||g' -e 's/\s//g')" = "{{zfsonlinux_signing_key}}"
zfs:
  pkg.installed:
    - pkgs:
      - kernel-devel
      - zfs
    - onlyif:
      - "dnf repolist --enabled zfs | grep -q -E '^zfs '"
      - test "$(gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-zfsonlinux | grep -E '^\s+Key fingerprint = ' | sed -e 's|Key fingerprint =||g' -e 's/\s//g')" = "{{zfsonlinux_signing_key}}"
{% endif %}
{% endif %}
