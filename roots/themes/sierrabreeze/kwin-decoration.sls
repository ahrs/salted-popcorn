{% if grains['os_family'] == 'Arch' %}
{% from "common/Arch/aur_install.jinja" import yay with context %}
include:
  - applications.utilities.yay.yay
sierrabreeze-kwin-decoration:
{{ yay(['sierrabreeze-kwin-decoration-git']) }}
{% endif %}

{% if grains['os_family'] == 'Gentoo' %}
include:
  - common.Gentoo.local_overlay
x11-themes/SierraBreeze:
  pkg.installed:
    - pkgs:
      - x11-themes/SierraBreeze
    - require:
      - sls: common.Gentoo.local_overlay
{% endif %}
