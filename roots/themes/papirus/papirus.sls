{% set papirus_icon_theme = salt['grains.filter_by']({
  'default': {
    'papirus-icon-theme': 'papirus-icon-theme',
  },
  'Gentoo': {
    'papirus-icon-theme': 'x11-themes/papirus-icon-theme'
  }
}, grain='os_family', merge = salt['pillar.get']('papirus_icon_theme:lookup'), base='default') %}

papirus-icon-theme:
  pkg.installed:
    - pkgs:
      - {{papirus_icon_theme['papirus-icon-theme']}}
