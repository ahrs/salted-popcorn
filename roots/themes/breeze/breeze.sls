{% set breeze = salt['grains.filter_by']({
  'default': {
    'breeze':  'breeze',
    'breeze-gtk': 'breeze-gtk',
    'breeze-icons': 'breeze-icons'
  },
  'Arch': {
    'breeze':  'breeze'
  },
  'Debian': {
    'breeze-gtk': 'breeze-gtk-theme',
    'breeze-icons': 'breeze-icon-theme'
  },
  'Gentoo': {
    'breeze':  'kde-plasma/breeze',
    'breeze-icons':  'kde-frameworks/breeze-icons',
    'breeze-gtk':  'kde-plasma/breeze-gtk'
  },
  'RedHat': {
    'breeze': 'plasma-breeze',
    'breeze-icons': 'breeze-icon-theme'
  }
}, grain='os_family', merge = salt['pillar.get']('breeze:lookup'), base='default') %}

include:
  - themes.cursors.breeze-cursor-theme.breeze-cursor-theme

breeze:
  pkg.installed:
    - pkgs:
      {% for pkg in breeze %}
      - {{breeze[pkg]}}
      {% endfor %}


