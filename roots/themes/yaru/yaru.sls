yaru:
  pkg.installed:
    - pkgs:
      - yaru-theme-gnome-shell
      - yaru-theme-gtk
      - yaru-theme-icon
      - yaru-theme-sound
