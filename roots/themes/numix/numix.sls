{% set numix_themes = salt['grains.filter_by']({
  'default': {
    'numix-icon-theme': 'numix-icon-theme',
    'numix-gtk-theme':  'numix-gtk-theme'
  },
  'Arch': {
    'numix-icon-theme': None
  },
  'Gentoo': {
    'numix-icon-theme': 'x11-themes/numix-icon-theme',
    'numix-gtk-theme':  'x11-themes/numix-gtk-theme'
  }
}, grain='os_family', merge = salt['pillar.get']('numix_themes:lookup'), base='default') %}

numix:
  pkg.installed:
    - pkgs:
      {% for pkg in numix_themes %}
      {% if numix_themes[pkg] != None %}
      - {{numix_themes[pkg]}}
      {% endif %}
      {% endfor %}
