{% set breeze_cursor_theme = salt['grains.filter_by']({
  'default': {
    'breeze-cursor-theme':  'breeze-cursor-theme'
  },
  'Arch': {
    'breeze-cursor-theme':  'breeze'
  },
  'Gentoo': {
    'breeze-cursor-theme':  'kde-plasma/breeze'
  },
  'Void': {
    'breeze-cursor-theme':  'breeze-cursors'
  }
}, grain='os_family', merge = salt['pillar.get']('breeze_cursor_theme:lookup'), base='default') %}

breeze-cursor-theme:
  pkg.installed:
    - pkgs:
      - {{breeze_cursor_theme['breeze-cursor-theme'] }}
