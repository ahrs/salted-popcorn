# Salted Popcorn 🍿

[Saltstack](https://s.saltstack.com/community/) states for bootstrapping a desktop linux system

---

This (somewhat messy) repo contains states to bootstrap a new linux distro. It configures the systems locale and timezone, creates a user capable of doing desktopy things (an empty password is used so I can login without a password and then change it afterwards) and installs various packages. It also pulls in my dotfiles.

## Highlights

<!-- To Do: -->
<!--    * Link to the source code for each item below -->

* Configures the system locale
* Configures the systems timezone
* Creates a user
* Installs "useful" packages
* On Arch, installs `yay` and sets up a dedicated `aur` user that's capable of building and installing packages (this has security implications I'm aware of)
* On Gentoo, the profile is configured via pillar data, libressl support is optionally provided (This should be done before installing other packages since migrating is kind of a pain once you have lots of packages dependent on openssl installed) and an attempt to manage useflags, masks and keywords using individual states is made. This is kind of a mess right now and will no doubt be improved once I start using this for real instead of just testing it in an lxc container.

## Testing

The provided Makefile is capable of bootstrapping an lxc container in the following environments:


| Target     | Description |
| ------     | ----------- |
| lxc_alpine | Alpine Linux lxc container provided via the [download template](https://us.images.linuxcontainers.org/). A lot of stuff is broken on non-edge releases either due to Alpine (missing/broken packages?) or issues upstream with salt. |
| lxc_arch   | Arch Linux lxc container provided via the [download template](https://us.images.linuxcontainers.org/) |
| lxc_centos | CentOS lxc container provided via the [download template](https://us.images.linuxcontainers.org/). Some stuff is broken due to both missing and outdated packages. It should still be possible to get a basic X11 environment setup though |
| lxc_debian | Debian lxc container provided via the [download template](https://us.images.linuxcontainers.org/) |
| lxc_fedora | Fedora lxc container provided via the [download template](https://us.images.linuxcontainers.org/). |
| lxc_gentoo | Gentoo lxc container provided via the [download template](https://us.images.linuxcontainers.org/). A shellscript is used to carefully provision the container with salt |
| lxc_opensuse | Opensuse Leap lxc container provided via the [download template](https://us.images.linuxcontainers.org/) |
| lxc_ubuntu | Ubuntu lxc container provided via the [download template](https://us.images.linuxcontainers.org/) |
| lxc_void   | Void Linux container using the voidlinux template from [lxc-templates](https://github.com/lxc/lxc-templates). This requires [xbps](https://github.com/void-linux/xbps) to be installed on your host system (xbps can be built on non-Void systems so this shouldn't be an issue) |

The advantage of using an lxc container over another container technology (like say Docker) is that it can somewhat reliably emulate the entire distro including its init system. In the Alpine container openrc is run as pid 1, in Void's runit is used and in Fedora's container systemd is used too.

**Note**: Systemd can sometimes be picky about running in an lxc container when the host system itself is not using systemd. This can be trivially solved by mounting a systemd cgroup hierarchy as follows:

```
# mkdir -p /sys/fs/cgroup/systemd
# mount -t cgroup -o none,name=systemd systemd /sys/fs/cgroup/systemd
```

## FAQ

**Q**: Why not a shellscript?

**A**: If anything's worth doing, it's worth overdoing. Just kidding, I took this as an opportunity to learn more about salt. Lots of stuff I'm doing is "ugly" and could be greatly improved.
